# README #

How to use the data

### What is this repository for? ###

The repository contains the do-files and the stata file used for the analysis of our paper on China.
I have created it so I can document the workflow and changes to the do-files and at the same time make them available to you.
All you have to do is download the file you want to look at.
The Stata file needed is "data.dta" and the repective file used for our analysis is called
"First analysis.do". The do-file containing the variable creation is called
"variablecreation.do".