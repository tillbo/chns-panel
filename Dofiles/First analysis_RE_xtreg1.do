	set more off
	capture log close
	clear matrix
	global homefolder "/gpfs/med/gsd12ytu/thesis/China"
	global homefolder ""/home/till/Phd/China Data/Data_2014""
	//global homefolder ""/home/till/Phd/China Data/Data_2014_2""
	cd $homefolder





	log using Logs/re_binary_mi_results_xtreg.log, replace
	***************************************
	/*+++++Binary diabetes indicator+++++*/
	***************************************

	use "Data/data_imputed30", clear

	drop if dmyet_bl == 1
	drop if maxseq == 1
	drop if retired == 1
	drop if age > 64


	global x "dmyet age_sq i.wave han rural married secondary university insurance index ib45.province hhincpc_cpi"


	//eststo mi_works_re_ia: mi est, post cmdok esampvaryok : xtreg works $x c.c__dmyet#i.female c.m__dmyet#i.female female, re ro  

	/*BY GENDER*/


	forvalues i =1/1{
	display `i'
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{
	eststo mi_`var'_re`i': mi est, post cmdok esampvaryok : xtreg `var' $x if female == `i', re ro  
	}
	}


	estwrite mi_* using "savedestimates/diabetes_dummy_mi_re_xtreg", id() replace
	estread mi_* using "savedestimates/diabetes_dummy_mi_re_xtreg"


/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar  ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")

	forvalues i=0/1{
	esttab mi_works_re`i' mi_smoke_re`i'  mi_alc_re`i' mi_bmi_re`i' mi_waist_re`i' mi_d3kcal_re`i' mi_MET_PA_re`i' mi_hypert_bio_re`i' using "Tables/re_estimates_mi_xtreg`i'.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced}
	       }
	

	
	      
*Word
  global table_reduced b(%9.3f) se(%9.3f)  nolz noobs  ///
	 collabels(none) keep(dmyet) label nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_re`i' mi_smoke_re`i'  mi_alc_re`i' mi_bmi_re`i' mi_waist_re`i' mi_d3kcal_re`i'  mi_MET_PA_re`i' mi_hypert_bio_re`i' using "Tables/re_estimates_mi_xtreg`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} 
}





log close

****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************


global x "yearsdiagall age_sq i.wave han rural married secondary university insurance index ib45.province hhincpc_cpi"

label var yearsdiagall "Years since diagnosis"


log using Logs/re_duration_mi_results_xtreg.log, replace

/*BY GENDER*/

forvalues i =0/1{
display `i'
foreach var of varlist works smoke alc bmi waist d3kcal obese MET_PA hypert_bio{
eststo mi_`var'_re_dur`i': mi est, post cmdok esampvaryok : xtreg `var' $x if female == `i', re ro  
}
}


estwrite mi_*_dur* using "savedestimates/diabetes_duration_mi_re_xtreg", id() replace
estread mi_*_dur*  using "savedestimates/diabetes_duration_mi_re_xtreg"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar   ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(yearsdiagall) label nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")



forvalues i=0/1{
esttab mi_works_re_dur`i' mi_smoke_re_dur`i'  mi_alc_re_dur`i' mi_bmi_re_dur`i' mi_waist_re_dur`i' mi_d3kcal_re_dur`i' mi_MET_PA_re_dur`i' mi_hypert_bio_re_dur`i'  using "Tables/re_estimates_mi_xtreg_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} fragment
       }
   

           
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell eform(1 1 1 0 0 0) ///
	 collabels(none) keep(yearsdiagall) label  nobaselevels /// 
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")



forvalues i=0/1{
esttab mi_works_re_dur`i' mi_smoke_re_dur`i'  mi_alc_re_dur`i' mi_bmi_re_dur`i' mi_waist_re_dur`i' mi_d3kcal_re_dur`i' mi_MET_PA_re_dur`i' mi_hypert_bio_re_dur`i' using "Tables/re_estimates_mi_xtreg_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} 
}

capture log close



***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************

log using Logs/re_durationgroups_mi_xtreg.log, append

global x "i.yearsdiagall_g_alt age_sq i.wave i.han ib(freq).province rural married secondary university insurance index hhincpc_cpi"



/*BY GENDER*/

	* normally use loops, but here need to estimate them seperatley as for female smoke regression gives error because ommits different groups between datasets, i.e. ommits > 20 years duration in some due to lack of data. Thefore exclude this group in female estimation, but not in male

forvalues i =0/1{
display `i'
foreach var of varlist works smoke alc bmi waist d3kcal obese MET_PA hypert_bio{
eststo mi_`var'_re_g`i': mi est, post cmdok esampvaryok : xtreg `var' $x if female == `i', re ro  
}
}




label var bmi "BMI" 
label var waist "Waist circumference (cm)" 
label var works "Employment status"
label var alc "Any alcohol consumption"
label var smoke "smokeing status"
label var obese "Obese"
label var overweight "Overweight"
label var d3kcal "Kcal"

estwrite mi_*_g* using "savedestimates/diabetes_durationgroups_mi_re_xtreg", id() replace
estread mi_*_g* using "savedestimates/diabetes_durationgroups_mi_re_xtreg"

*PLOTTING ALL DURATION GROUP RESULTS*
coefplot mi_works_re_g0 mi_works_re_g1 , bylabel("Employed")   || mi_smoke_re_g0 mi_smoke_re_g1, bylabel("Smoking")|| ///
 mi_alc_re_g0 mi_alc_re_g1, bylabel("Alcohol") || ///
(mi_bmi_re_g0, label(men) lpattern(solid)) (mi_bmi_re_g1, label(women) lpattern(shortdash)) , bylabel("BMI") || ///
mi_waist_re_g0 mi_waist_re_g1,  bylabel("Waist (cm)") || ///
mi_d3kcal_re_g0 mi_d3kcal_re_g1, bylabel("Kcal") || ///
mi_MET_PA_re_g0 mi_MET_PA_re_g1,  bylabel("PA") || ///
mi_hypert_bio_re_g0 mi_hypert_bio_re_g1,  bylabel("Hypertension") || , ///
keep(*.yearsdiagall_g_alt ) yline(0) recast(line)  byopts(yrescale cols(4))  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(msm, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_re.eps, replace 







/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(*yearsdiagall_g_alt) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_re_g`i' mi_smoke_re_g`i'  mi_alc_re_g`i' mi_bmi_re_g`i' mi_waist_re_g`i' mi_d3kcal_re_g`i' mi_MET_PA_re_g`i' mi_hypert_bio_re_g`i' using "Tables/re_estimates_mi_xtreg_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced}
       }
       

*Word
  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )  nolz nostar noobs ///
	 collabels(none) keep(*yearsdiagall_g_alt) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_re_g`i' mi_smoke_re_g`i'  mi_alc_re_g`i' mi_bmi_re_g`i' mi_waist_re_g`i' mi_d3kcal_re_g`i' mi_MET_PA_re_g`i' mi_hypert_bio_re_g`i' using "Tables/re_estimates_mi_xtreg_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} 
}

capture log close



