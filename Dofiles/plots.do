*Plots of FE and MSM models for corrections of thesis



set more off
capture log close
clear matrix


*global homefolder ""/home/till/Phd/China Data/Data_2014_2""
global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using "Logs/plots.log", replace

use "Data/data_imputed30", clear


estread mi_* using "savedestimates/diabetes_mi_msm_l", id()

estread mi_*_g* using "savedestimates/diabetes_durationgroups_mi_fe_xtreg"

estread mi_*_g* using "savedestimates/diabetes_durationgroups_mi_re_xtreg"


*PLOTTING ALL DURATION GROUP RESULTS -- Tables used for thesis*

coefplot mi_works_l_dur_g_0 mi_works_l_dur_g_1, bylabel("Employed (MSM)") ///
||  mi_works_fe_g0 mi_works_fe_g1 , bylabel("Employed (FE)") ||  (mi_works_re_g0,label("men") lpattern(solid)) (mi_works_re_g1, label("women") lpattern(shortdash)) , bylabel("Employed (RE)") ///
|| mi_smoke_l_dur_g_0 mi_smoke_l_dur_g_1, bylabel("Smoking (MSM)") || mi_smoke_fe_g0 mi_smoke_fe_g1, bylabel("Smoking (FE)") ///
 || mi_smoke_re_g0 mi_smoke_re_g1, bylabel("Smoking (RE)") ///
|| mi_alc_l_dur_g_0 mi_alc_l_dur_g_1, bylabel("Alcohol (MSM)") || mi_alc_fe_g0 mi_alc_fe_g1, bylabel("Alcohol (FE)") ///
 || mi_alc_re_g0 mi_alc_re_g1, bylabel("Alcohol (RE)")  || ///
 hypert_bio_l_dur_g0_tr1 hypert_bio_l_dur_g1_tr1, bylabel("Hypertension (MSM)") || ///
 mi_hypert_bio_fe_g0 mi_hypert_bio_fe_g1 , bylabel("Hypertension (FE)") ||  mi_hypert_bio_re_g0 mi_hypert_bio_re_g1 , bylabel("Hypertension (RE)") ||, ///
keep(*.yearsdiagall_g_alt) yline(0) recast(line) byopts(row(4) yrescale )  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(binary, replace) ylabel(,  angle(horizontal) nogrid)   xlabel(,  angle(45)) scheme(s1mono) ytitle("Marginal effect (Pr.)")  xtitle("Years after diagnosis")  xsize(7) 
graph export binary.eps, replace 


coefplot mi_bmi_l_dur_g_0 mi_bmi_l_dur_g_1, bylabel("BMI (MSM)") ///
||  mi_bmi_fe_g0 mi_bmi_fe_g1 , bylabel("BMI (FE)") ||  mi_bmi_re_g0 mi_bmi_re_g1 , bylabel("BMI (RE)") ///
|| mi_waist_l_dur_g_0 mi_waist_l_dur_g_1, bylabel("Waist in cm (MSM)") || mi_waist_fe_g0 mi_waist_fe_g1, bylabel("Waist in cm (FE)") ///
 || mi_waist_re_g0 mi_waist_re_g1, bylabel("Waist in cm (RE)") ///
|| mi_d3kcal_l_dur_g_0 mi_d3kcal_l_dur_g_1, bylabel("Kcal (MSM)") ///
 || mi_d3kcal_fe_g0 mi_d3kcal_fe_g1, bylabel("Kcal (FE)") || ///
 (mi_d3kcal_re_g0, label("men") lpattern(solid)) (mi_d3kcal_re_g1, label("women") lpattern(shortdash)), bylabel("Kcal (RE)") || ///
  mi_MET_PA_l_dur_g_0 mi_MET_PA_l_dur_g_1, bylabel("PA (MSM)") || ///
  mi_MET_PA_fe_g0 mi_MET_PA_fe_g1 , bylabel("PA (FE)") ||  mi_MET_PA_re_g0 mi_MET_PA_re_g1, bylabel("PA (RE)") ||, ///
keep(*.yearsdiagall_g_alt) yline(0) recast(line) byopts(row(4) yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(cont, replace) scale(.7)  ylabel(,angle(horizontal) nogrid) xlabel(,  angle(45)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(5)
graph export continuous.eps, replace 



/*

coefplot mi_alc_l_dur_g_me_0, bylabel("Smoking (MSM)") ///
||  (mi_smoking_fe_g0, label("men") lpattern(solid)) (mi_smoking_fe_g1, label("women") lpattern(shortdash)), bylabel("Smoking (FE)") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line) byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(smoking, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_smoking.eps, replace 


coefplot mi_alc_l_dur_g_me_0, bylabel("Alcohol (MSM)") ///
||  (mi_alc_fe_g0, label("men") lpattern(solid)) (mi_alc_fe_g1, label("women") lpattern(shortdash)), bylabel("Alcohol (FE)") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line) byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(alc, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_alc.eps, replace 


coefplot (mi_bmi_l_dur_g_0,label("men") lpattern(solid)) (mi_bmi_l_dur_g_1, label("women") lpattern(shortdash)), bylabel("BMI (MSM)") ///
||  mi_bmi_fe_g0 mi_bmi_fe_g1 , bylabel("BMI (FE)") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line) byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(bmi, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_bmi.eps, replace 

coefplot (mi_waist_l_dur_g_0,label("men") lpattern(solid)) (mi_waist_l_dur_g_1, label("women") lpattern(shortdash)), bylabel("Waist (MSM)") ///
||  mi_waist_fe_g0 mi_waist_fe_g1 , bylabel("Waist (FE)") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line) byopts(yrescale)   ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(waist, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_waist.eps, replace 


coefplot (mi_d3kcal_l_dur_g_0,label("men") lpattern(solid)) (mi_d3kcal_l_dur_g_1, label("women") lpattern(shortdash)), bylabel("Kcal (MSM)") ///
||  mi_kcal_fe_g0 mi_kcal_fe_g1 , bylabel("Kcal (FE)") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line) byopts(yrescale)  ylabel(#8)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(kcal, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_kcal.eps, replace 


graph combine works smoking alc, xcommon cols(1) name(binary_graphs, replace)
graph export mi_binary.eps, replace 

graph combine bmi waist kcal, xcommon altshrink cols(1) name(continuous_graphs, replace)
graph export mi_continuous.eps, replace 
