set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data.dta", clear
***********************************
/*FE RESULTS FOR NON-IMPUTED DATA*/
***********************************

drop if wave < 1997

drop seq
bysort id (wave): egen seq=seq()  // creates number of waves that people have went through
bysort id (wave): egen maxseq=max(seq)  // indicates the maximum number of waves an individual attended. I need to drop all those with only one wave because for those the marginal structural model will not work as they have no prior information

drop if maxseq==1
bysort id (wave): gen dmyet_bl= dmyet[1]

drop if dmyet_bl == 1
 //USE LPM

 xtset id wave


log using Logs/re_binary_results_xtreg.log, replace
***************************************
/*+++++Binary diabetes indicator+++++*/
***************************************

global x "dmyet age_sq i.wave han rural married secondary university insurance index ib(freq).province hhincpc_cpi"


/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo works_re`i':  xtreg works $x if female == `i', re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo smoking_re`i':  xtreg stillsmoking $x works if female == `i', re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo alc_re`i':  xtreg alc $x works if female == `i', re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
}

forvalues i =0/1{
eststo bmi_re`i':  xtreg bmi $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo waist_re`i':  xtreg waist $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo kcal_re`i':  xtreg d3kcal $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)

}
estwrite * using "savedestimates/diabetes_dummy_re_xtreg", id() replace
estread * using "savedestimates/diabetes_dummy_re_xtreg"


/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f) stats(j jp N, fmt( %9.3f %9.3f  %9.0f ) labels("Hausman test" "p-value" "N"))  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

	forvalues i=0/1{
	esttab works_re`i' smoking_re`i'  alc_re`i' bmi_re`i' waist_re`i' kcal_re`i' using "Tables/noMI/re_estimates_xtreg`i'.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
	       }
	      
	      
*Word
  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	  collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

forvalues i=0/1{
esttab works_re`i' smoking_re`i'  alc_re`i' bmi_re`i' waist_re`i' kcal_re`i' using "Tables/noMI/re_estimates_xtreg`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


estwrite * using "savedestimates/diabetes_dummy_re_xtreg", id() replace
estread * using "savedestimates/diabetes_dummy_re_xtreg"


capture log close

****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************

global x "yearsdiagall age_sq i.wave han ib(freq).province rural married secondary university insurance index hhincpc_cpi"

label var yearsdiagall "Years since diagnosis"


log using Logs/re_duration_results_xtreg.log, replace


/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo works_re_dur`i':  xtreg works $x  if female == `i', re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo smoking_re_dur`i':  xtreg stillsmoking $x works if female == `i', re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo alc_re_dur`i':  xtreg alc $x works if female == `i', re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
}

forvalues i =0/1{
eststo bmi_re_dur`i':  xtreg bmi $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo waist_re_dur`i':  xtreg waist $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo kcal_re_dur`i':  xtreg d3kcal $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)

}

estwrite * using "savedestimates/diabetes_duration_re_dur_xtreg", id() replace
estread * using "savedestimates/diabetes_duration_re_dur_xtreg"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  stats(j jp N, fmt( %9.3f %9.3f  %9.0f ) labels("Hausman test" "p-value" "N")) ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")



forvalues i=0/1{
esttab works_re_dur`i' smoking_re_dur`i'  alc_re_dur`i' bmi_re_dur`i' waist_re_dur`i' kcal_re_dur`i' using "Tables/noMI/re_estimates_xtreg_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
           
*Word
  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  stats(j jp N, fmt( %9.3f %9.3f  %9.0f ) labels("Hausman test" "p-value" "N")) ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")



forvalues i=0/1{
esttab works_re_dur`i' smoking_re_dur`i'  alc_re_dur`i' bmi_re_dur`i' waist_re_dur`i' kcal_re_dur`i' using "Tables/noMI/re_estimates_xtreg_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close






***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************

drop yearsdiagall_*

replace yearsdiagall = 0 if dmyet == 0

gen yearsdiagall_r = round(yearsdiagall,1)
recode yearsdiagall_r (0/1 = 1) ///
			(2/3 = 2) ///
			(4/5 = 3) ///
			(6/7 = 4) ///
			(8/9 = 5) ///
			(10/11 = 6) ///
			(12/13 = 7) ///
			(14/15 = 8) ///
			(16/20 = 9) ///
			(21/100 = 10) (-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g)
			
replace yearsdiagall_g = 0 if dmyet == 0			
label define yearsdiagall_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-10" 7 "11-12" 8 "13-14" 9 "15-19" 10 "20+", replace
label value yearsdiagall_g yearsdiagall_g
label var yearsdiagall_g "Years since diagnosis in groups"
dummies yearsdiagall_g 


log using Logs/re_durationgroups_xtreg.log, append

global x "i.yearsdiagall_g age_sq i.wave i.han ib(freq).province rural married secondary university insurance index hhincpc_cpi"



/*BY GENDER*/

	* normally use loops, but here need to estimate them seperatley as for female smoking regression gives error because ommits different groups between datasets, i.e. ommits > 20 years duration in some due to lack of data. Thefore exclude this group in female estimation, but not in male
eststo works_re_g0:  xtreg works $x if female == 0, re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo smoking_re_g0:  xtreg stillsmoking $x  works if female == 0, re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo alc_re_g0:  xtreg alc $x i.province works if female == 0, re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)

eststo works_re_g1:  xtreg works $x if female == 1, re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo smoking_re_g1:  xtreg stillsmoking $x  works if female == 1, re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo alc_re_g1:  xtreg alc $x i.province works if female == 1, re ro 
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)


forvalues i =0/1{
eststo bmi_re_g`i':  xtreg bmi $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo waist_re_g`i':  xtreg waist $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
eststo kcal_re_g`i':  xtreg d3kcal $x works if female == `i', re ro
xtoverid, ro cluster(id)
estadd r(j)  
estadd r(jp)
}

estwrite * using "savedestimates/diabetes_durationgroups_re_xtreg", id() replace
estread * using "savedestimates/diabetes_durationgroups_re_xtreg"

*PLOTTING ALL DURATION GROUP RESULTS*
coefplot works_re_g0 works_re_g1 , bylabel("Employed")   || smoking_re_g0 smoking_re_g1, bylabel("Smoking")|| ///
 alc_re_g0 alc_re_g1, bylabel("Alcohol") || ///
(bmi_re_g0, label(men) lpattern(solid)) (bmi_re_g1, label(women) lpattern(shortdash)) , bylabel("BMI") || ///
waist_re_g0 waist_re_g1,  bylabel("Waist (cm)") || ///
kcal_re_g0 kcal_re_g1, bylabel("Kcal") ||, ///
keep(1.yearsdiagall_g 2.yearsdiagall_g 3.yearsdiagall_g 4.yearsdiagall_g 5.yearsdiagall_g 6.yearsdiagall_g 7.yearsdiagall_g 8.yearsdiagall_g 9.yearsdiagall_g ) yline(0) recast(line)  byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(continuous_xtreg_re, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono)

graph export re.eps, replace 

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) stats(j jp N, fmt( %9.3f %9.3f  %9.0f ) labels("Hausman test" "p-value" "N")) ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX

forvalues i=0/1{
esttab works_re_g`i' smoking_re_g`i'  alc_re_g`i' bmi_re_g`i' waist_re_g`i' kcal_re_g`i' using "Tables/noMI/re_estimates_xtreg_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
*Word
  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) stats(j jp N, fmt( %9.3f %9.3f  %9.0f ) labels("Hausman test" "p-value" "N")) ///
	 collabels(none) keep(*.yearsdiagall_g) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


forvalues i=0/1{
esttab works_re_g`i' smoking_re_g`i'  alc_re_g`i' bmi_re_g`i' waist_re_g`i' kcal_re_g`i' using "Tables/noMI/re_estimates_xtreg_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}





capture log close
