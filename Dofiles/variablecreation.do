
set more off
capture log close
clear matrix
/*
// preparational do files
global homefolder ""/home/till/Dropbox/PhD/Data/China Data""

        do "/home/till/Dropbox/PhD/Data/China Data/merging.do"	//master china files
	do "/home/till/Dropbox/PhD/Data/China Data/variablecreation.do" //creating new variables
	
	*/
	
// renaming and creating variables


global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder

use "Data/merged_data.dta", clear

rename IDind id

*gen retired variable because women retire at age 50 to 55 and men at age 60.
gen retired = 0 
replace retired = 1 if B2A == 5
gen student =B2A == 4 if B2 < .


// generate time variable with regular time gaps to later create lagged values

recode wave (1989=1) (1991=2) (1993=3) (1997=4) (2000=5) (2004=6) (2006=7) (2009=8) (2011=9), gen(idt)
drop if wave < 1997
drop if age >=65 | age <18

// set panel variables
xtset id idt

 // generate new variables
 
 /*demographics*/
 
 /* Province in variable T1
 21 Liaoning 23 Heilongjiang 32 Jiangsu 37 Shandong
 42 Hubei 43 Hunan 45 Guangxi 52 Guizhou 41 Henan */
 
label define province 11 "Beijing" 21 "Liaoning" 23 "Heilongjiang" 31 "Shanghai" 32 "Jiangsu" ///
   37 "Shandong" 42 "Hubei" 43 "Hunan" 45 "Guangxi" 52 "Guizhou" 41 "Henan" 55 "Chongqing"

label values T1 province

clonevar province = T1  // create variable called province based on T1
 
// create WAVE dummies out of WAVE variable
dummies wave

// create province dummies
dummies province 

// nationality
gen han = 1 if nationality != 1
replace han = 0 if nationality == 1
label define han 1 "Han" 0 "Other"
label value han han
label var han "Non-Han ethnicity"
 
// urban or rural
gen rural = 0 if T2 == 1 // urban  

replace  rural = 1 if T2 == 2 // rural

label define rural 0 "urban" 1 "rural"

label values rural rural

// age 
replace age = round(age,1)
replace age = . if age <0 // rounding age 
gen age_sq = age^2



by id: gen age_ini = age[1]
label var age_ini "Age when first entered survey"


// gender
gen male = 0

replace male = 1 if gender == 1

gen female = 0

replace female = 1 if gender == 2

// marital status
gen married = 0 if A8 <.

replace married = 1 if A8 == 2


/*education -- highest education level attained*/
gen primary = 0 if A12 != 1 & A12 <.

replace primary = 1 if A12 == 1

gen secondary = 0 if A12 != 2 & A12 != 3 & A12 != 4  & A12 <.

replace secondary = 1 if A12 == 2 | A12 == 3 | A12 == 4  

gen university = 0 if A12 <=4 | A12 > 6 & A12 <.

replace university = 1 if A12 > 4 & A12 <=6

gen school_curr = 0  // if currently in school

replace school_curr = 1 if A13 == 1

/*employment -- if presently working*/
gen works = B2 if B2 > -9 & B2 < 2  // there are miscodings
replace works =. if B2A == 4  // not working if in school

/* health treatment expenditures variable over last 4 weeks*/
gen healthexp = M30 if M30 > -1 & M30 < 99999 

/* medical insurance yes / no */
 
clonevar insurance = M1 if M1 <9

/* log income adjusted fro inflation to 2006-- from pre-constructed income variable from below mentioned income sources:
INDBUSNyyyy - business income
INDFARMyyyy - farming income
INDFISHyyyy - fishing income
INDGARDyyyy - gardening income
INDLVSTyyyy - livestock income
INDRETIREyyyy - retirement income
INDWAGEyyyy - non-retirement wages */
 
gen lnindinc_cpi = log(indinc_cpi)  // individual income

gen lnhhinc_cpi = log(hhinc_cpi)  // total household income
 
gen lnindwage = log(indwage)  // non-retirement wages


/*Diabetes variables*/

clonevar diabetes_sr = U24A if U24A <3  // self-reported diabetes diagnosis
label var diabetes_sr "Self-reported diabetes diagnosis"

clonevar diabetes_age = U24B if U24B >=0  // self-reported age at diagnosis
label var diabetes_age "Self-reported age at diabetes diagnosis"

clonevar treat_insulin = U24F if U24F <3
label var treat_insulin "Currently diabetes: taking insulin"
replace treat_insulin = 0 if diabetes_sr == 0

clonevar treat_oral = U24E if U24E <3 & treat_insulin == 0
label var treat_oral "Currently diabetes: taking oral medication"
replace treat_oral = 0 if diabetes_sr == 0

gen treat_med = 1 if treat_oral == 1 | treat_insulin == 1  // take medicine
replace treat_med = 0 if treat_oral == 0 & treat_insulin == 0 // take no medicine for diabetes treatment
label var treat_med "Currently diabetes: taking oral medication or insulin"


gen treat_nomed = 0 if treat_med == 1 | diabetes_sr == 0  // take medicine
replace treat_nomed = 1 if treat_oral == 0 & treat_insulin == 0 & diabetes_sr == 1 // take no medicine for diabetes treatment
label var treat_nomed "Currently diabetes: not taking medication"

// creat categorical variable of medical treatment instead of dummies
gen diab_med = 0 if diabetes_sr == 0
replace diab_med = 1 if treat_nomed == 1
replace diab_med = 2 if treat_med == 1

/*First occurence of diabetes diagnosis*/
by id, sort: egen firsttime = min(cond(diabetes_sr == 1, wave, .))
gen byte first = wave == firsttime
label define first 1 "newly diagnosed"
label value first first
label var first "first time diabetes reported"


/*Find those that have newly entered the survey and report diabetes or report diabetes in 1997
to then use the information of age at diagnosis to construct dummies for when was first diagnosed*/

gen dmyet= diabetes_sr
label var dmyet "Diabetes"
by id (wave), sort: replace dmyet = dmyet[_n-1] if diabetes_sr == 0 | diabetes_sr ==.
replace dmyet = 0 if dmyet == . & diabetes_sr == 0
by id (wave), sort: replace dmyet = dmyet[_n+1] if dmyet == . & dmyet[_n+1] == 0
 
bysort id: egen temp1 = mean(diabetes_age) // mean reported age of diagnosis througout waves

gen diabetes_age_mean = round(temp1)  
bysort id (wave): egen firstwave = min(wave)  // which wave is first wave
bysort id (wave): egen temp2 = min(wave) if dmyet == 1  // wave where diagnosis was first reported
bysort id (wave): egen temp3 = max(dmyet) // extend diabetes info to all waves
bysort id (wave): egen temp4 = max(wave) if dmyet == 0 & temp3 ==1 //  last wave before diabetes diagnosis was reported
bysort id (wave): gen temp_yeardx_mean = (temp2 + temp4[_n-1])/2  // creating mean between last wave befor diagnosis and first wave of reported diagnosis
bysort id (wave): replace temp_yeardx_mean = temp2 if _n==1 & dmyet == 1
bysort id (wave): gen yeardx = temp_yeardx_mean 
gen year_diagnosis_reported = wave - (age - diabetes_age_mean) if dmyet== 1 // year diagnosis was reported
bysort id (wave): replace yeardx = year_diagnosis_reported if (year_diagnosis_reported >= wave[_n-1] & year_diagnosis_reported <.  ) | (firstwave == temp2) & dmyet==1 // replace calculated year of dianosis with self-reported year of diagnosis only if first year in survey is equal to first year with reported diabetes or if the slef-reported year of diagnosis is larger or equal to the year of the last wave without self-reported diabetes
bysort id (wave): replace yeardx = yeardx[_n-1] if yeardx[_n-1] <. // replace calculated year of dianosis with self-reported year of diagnosis only if first year in survey is equal to first year with reported diabetes or if the self-reported year of diagnosis is larger or equal to the year of the last wave without self-reported diabetes



gen yeardx_alt = year_diagnosis_reported if dmyet== 1
replace yeardx_alt =  yeardx if year_diagnosis_reported ==. & dmyet ==1
gen yearssincedx = wave - yeardx if yeardx < 2013 // years since diagnosis
gen yearssincedx_alt = wave - yeardx_alt if yeardx_alt < 2013 // years since diagnosis

/*yearssincedx_alt has about 150 more observations than yearssincedx because yearssincedx 
does not provide years of diagnosis information for those that missed a wave exactly before they
 reported diabetes for the first time. I could use the self reported information for these cases,
 however, for many the self-reported info is not reliable. Or I could use the mean year between the
 first year of reported diabetes and the last non-missing wave without self-reported diabetes. However,
this can be quite a big time period so I could be off by three years or so of the true year of diagnosis.
Therefor I decided to leave them missing for now.*/ 

//gen yearssincedx_uncorr = wave - yeardx // without correction for those newly in wave
/*Treatment at first diagnosis*/

gen diag_oral = 0 if diabetes_sr == 1
replace diag_oral = 1 if first == 1 & treat_oral == 1
label var diag_oral "Newly diagnosed with oral medication"
gen diag_insulin = 0 if diabetes_sr == 1
replace diag_insulin = 1 if first == 1 & treat_insulin == 1
label var diag_insulin "Newly diagnosed with insulin"
gen diag_nomed = 0 if diabetes_sr == 1
replace diag_nomed = 1 if first == 1 & treat_oral == 0 & treat_insulin == 0
label var diag_nomed "Newly diagnosed without medication"
gen diag_med = 0 if diabetes_sr == 1
replace diag_med = 1 if first == 1 & (treat_oral == 1 | treat_insulin == 1)
label var diag_med "Newly diagnosed with either oral medication or insulin"

/*Extend first treatment to other waves by id.
 Doing this we assume that even if persons reported no diabetes after they reported a diagnosis before, that they still have diabetes.
 I.e., we replace */
bysort id: egen temp_med = max(diag_med)
bysort id: egen temp_nomed = max(diag_nomed)

/*Multiply above treatment by years since diagnosis to get years since diagnosis with specific treatment for every wave*/
gen yearsdiagmed = temp_med * yearssincedx  // years since first diagnosis with medication treatment
gen yearsdiagnomed = temp_nomed * yearssincedx  // years since first diagnosis without medication treatment
gen yearsdiagall = dmyet * yearssincedx  // years since first diagnosis with or without medication treatment
gen yearsdiagall_alt = dmyet * yearssincedx_alt  // years since first diagnosis with or without medication treatment
replace yearsdiagall = yearsdiagall + 1 if dmyet == 1 & yearsdiagall >= 0  // increase by 1 to count those without diabetes as zero
replace yearsdiagall_alt = yearsdiagall_alt + 1 if dmyet == 1 & yearsdiagall_alt >= 0 // increase by 1 to count those without diabetes as zero
replace yearsdiagall = 0 if dmyet == 0  // replace with zero to have those without diabetes counted as zero for linear specification
replace yearsdiagall_alt = 0 if dmyet == 0 // replace with zero to have those without diabetes counted as zero for linear specification
replace yearsdiagall =. if yearsdiagall <0 // to missing those with infeasible time since diagnosis
replace yearsdiagall_alt =. if yearsdiagall_alt <0 // to missing those with infeasible time since diagnosis

foreach var of varlist yearsdiagmed yearsdiagnomed yearsdiagall{  // recode the variables to get groups
recode `var' (2/3 = 2) (3.1/5 = 3) (6/7 = 4) (8/9 = 5) (10/15 = 6) (16/21 = 7) (22/100 = 8) (-1000/-1 = .), gen(`var'_g)
label define `var'_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-14" 7 "15-20" 8 "21+"
label value `var'_g `var'_g
label var `var'_g "Years since diagnosis in groups"
}
replace yearsdiagmed_g = 1 if temp_med == 1 & yearsdiagmed == 0 & diabetes_sr == 1   
replace yearsdiagnomed_g = 1 if temp_nomed == 1 & yearsdiagnomed == 0 & diabetes_sr == 1 
replace yearsdiagall_g = 1 if yearsdiagall == 0 & dmyet == 1 


*indicator to identify cases where reported year of diagnosis was replaced with mean year between last non-diabetes wave and first diabetes wave
/*only tab the indicator by wave to get info on how many obervations were replaced*/ 
gen yeardx_indicator = 1 if  yeardx != year_diagnosis_reported & yearsdiagall >0 & wave == temp2 // replaced
replace yeardx_indicator = 0 if  yeardx == year_diagnosis_reported & yearsdiagall >0 & yearsdiagall <. & wave == temp2 // not replaced



/*Other diseases*/

clonevar hypertension =  U22 if U22 <9

clonevar infarct = U24J if U24J <9

clonevar stroke = U24L if U24L <9

clonevar cancer = U24W if U24W <9

/*self-rated health*/

clonevar health_status = U48A if U48A <9

label define health_status 1 "Excellent" 2 "Good" 3 "Fair" 4 "Poor"

label values health_status health_status

dummies health_status

/*Biomarkers*/

egen systolic = rowmean(SYSTOL1 SYSTOL2 SYSTOL3)
egen diastolic = rowmean(DIASTOL1 DIASTOL2 DIASTOL3)

clonevar hba1c = Y50  // Hba1c

clonevar glucose_mg = GLUCOSE_MG   // glucose in mg

gen diab_hba1c = 0 if hba1c <.
replace diab_hba1c = 1 if hba1c <. & hba1c > 6.4

gen diab_ud = 0 if hba1c <.
replace diab_ud = 1 if hba1c <. & hba1c > 6.4 & diabetes_sr == 0
/* bmi and other bodytype measures*/

gen bmi = weight/((height/100)^2)

replace bmi =. if bmi > 100 | bmi <10 // cut-off extreme bmi values

clonevar waist = U10 if U10 > 20

/*Diabetes risk*/
gen diab_risk = 0 if hba1c <.
replace diab_risk = 1 if hba1c >= 5.7 & hba1c <.


/*bmi official cut-offs*/
gen obese = 0 if bmi <.
replace obese = 1 if bmi >=30 & bmi <.
gen overweight = 0 if bmi <.
replace overweight = 1 if bmi >=25 & bmi <30

/*bmi cut-offs as suggested by China Obesity Task Force*/

gen obese_china = 0 if bmi <.
replace obese_china = 1 if bmi >=28 & bmi <.
gen overweight_china = 0 if bmi <.
replace overweight_china = 1 if bmi >=24 & bmi <28

/* fat percentage*/

gen fatperc = 76 - 20 *(height/waist) if female == 1
replace fatperc =64 - 20 *(height/waist) if female == 0
replace fatperc = . if fatperc <= 0
/*Smoking */

clonevar eversmoked = U25 if U25 < 9 // if eversmoked

clonevar stillsmoking = U27 if U27 < 9 // if still smoking
replace stillsmoking  = 0 if eversmoked == 0  // replace with 0 if never smoked


/* Drinking */

clonevar anyalc = U40 if U40<8 // if drank any alcohol this year

clonevar alcfreq = U41 if U41 <=5
label var alcfreq "Frequency of alcohol consumption"

gen alc = 0 if anyalc <.
replace alc = 1 if alcfreq < 3
label var alc "=1 if alc > 3 times per week"




clonevar sleephrs = U324 if U324 > 0   // hours of sleep per day

/* Physical Activity 
Similar approach as in https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2731106/ */


* use Compendium of Physical Activities to assign activity level to activity at work, leisure, etc. similar to here 	
*https://sites.google.com/site/compendiumofphysicalactivities/Activity-Categories/volunteer-activities
/*A unit of MET is defined as the ratio of a person's working metabolic rate relative to his/her resting (basal) metabolic rate (Sallis et al., 1985)*/

gen MET_occ = 6 if B4 == 5 // farmer, fisherman, hunter
replace MET_occ = 4 if B4 == 6 | B4 == 7 | B4 == 10 | B4 == 11 | B2A == 4 | B2A == 13 | B2A == 12 | B2A == 9 // skilled worker (foreman, group leader, craftsman); non-skilled worker (ordinary laborer, logger); driver; service worker (housekeeper, cook, waiter, doorkeeper, hairdresser, counter, salesperson, launderer, child care worker); any other; athlete/actor/musician; ordinary soldier/policeman
replace MET_occ = 2 if B4 == 2 | B4 == 3 | B4 == 1 | B4 == 9 // senior professional/technical worker; junior professional/technical worker; administrator/executive/manager; office staff; ordinary soldier, policeman

gen daily_workhours = C6 if C6 >= 0
gen workdays_week = C5 if C5 >= 0 & C5 <8
gen workmonths_year = C3 if C3 >=0

gen workhours_week = daily_workhours*workdays_week // average hours per day worked in 7 days
gen workhours_week_yr = daily_workhours*workdays_week*(workmonths_year/12) // average hours per day worked in 7 days

replace MET_occ = MET_occ * workhours_week_yr // multiply with workhours per week	

replace MET_occ = 0  if works == 0

***garden work***
gen gardenhours = D3C if D3C >= 0 & D3C < 25
gen garden_days = D3B if D3B >= 0 & D3B < 9
gen garden_months = D3A if D3A >= 0

gen gardenhrs_week = gardenhours*garden_days
gen gardenhrs_week_yr = gardenhrs_week*(garden_months/12)

gen MET_garden = gardenhrs_week_yr * 6 // multiply with workhours per week	
replace MET_garden = 0  if works <. & MET_garden ==.

***livestock***

gen livestock_hours = F4C if F4C >= 0 & F4C < 25
gen livestock_days = F4B if F4B >= 0 & F4B < 9
gen livestock_months = F4A if F4A >= 0

gen livestock_week = livestock_hours*livestock_days
gen livestock_week_yr = livestock_week*(livestock_months/12)  // average number of hours per week over entire year

gen MET_livestock = livestock_week_yr * 6 // multiply with workhours per week	
replace MET_livestock = 0  if works <. & MET_livestock==.


***fishing***

gen fishing_hours = G4C if G4C >= 0 & G4C < 25
gen fishing_days = G4B if G4B >= 0 & G4B < 9
gen fishing_months = G4A if G4A >= 0

gen fishing_week = fishing_hours*fishing_days
gen fishing_week_yr = fishing_week*(fishing_months/12)  // average number of hours per week over entire year

gen MET_fishing = fishing_week_yr * 6 // multiply with workhours per week	
replace MET_fishing = 0  if works <. & MET_fishing == .

***farm***

gen farm_hours = E4C if E4C >= 0 & E4C < 25
gen farm_days = E4B if E4B >= 0 & E4B < 9
gen farm_months = E4A if E4A >= 0

gen farm_week = farm_hours*farm_days
gen farm_week_yr = farm_week*(farm_months/12)  // average number of hours per week over entire year

gen MET_farm = farm_week_yr * 6 // multiply with workhours per week	
replace MET_farm = 0  if works <.  & MET_farm ==.

***business***

gen business_hours = H8 if H8 >= 0 & H8 < 25
gen business_days = H7 if H7 >= 0 & H7 < 9
gen business_months = H6 if H6 >= 0

gen business_week = business_hours*business_days
gen business_week_yr = business_week*(business_months/12)  // average number of hours per week over entire year

gen MET_business = business_week_yr * 4 // multiply with workhours per week	
replace MET_business = 0  if works <. & MET_business ==.

egen MET_homework = rowtotal(MET_business MET_business MET_farm MET_fishing MET_garden MET_livestock), miss

egen MET_work = rowtotal(MET_occ MET_homework), miss

***Travel to work/school***

gen time_travel_bike = U144_MN/60  // for 1997,2000
gen time_travel_walk = U144_MN/60  // for 1997,2000
replace time_travel_bike =  U127_MN/60 if wave >=2004 // from 2004 onwards
replace time_travel_bike = 0 if works == 0 | U126 == 0
replace time_travel_walk =  U129_MN/60 if wave >=2004 // from 2004 onwards
replace time_travel_walk = 0 if works == 0 | U128 == 0

replace time_travel_walk = . if time_travel_walk > 10  // drop outliers that report more than 10 hours of travel per day
replace time_travel_bike = . if time_travel_bike > 10  // drop outliers that report more than 10 hours of travel per day

gen MET_bike = time_travel_bike * 7 * 4  // multiply with 7 days and METs
gen MET_walk = time_travel_walk * 7 * 3 // multiply with 7 days and METs

egen MET_worktravel = rowtotal(MET_bike MET_walk), miss

***No secondary jobs in sample, therefore not included in calculations

***Time use***

gen time_buy_food_hr = K3/60 if K3 > = 0
replace time_buy_food = 0 if K2 != 1 & K2 <.
gen MET_buy_food = time_buy_food_hr *7 * 2.3
gen time_prepare_food_hr = K5/60 if K5 > = 0 & wave >= 2009  // from 2009 on expressed in minutes, before in hours
replace time_prepare_food_hr = 0 if K4 != 1 & K4 <.
replace time_prepare_food_hr = K5 if K5 > = 0 & wave < 2009 // before 2009 expressed in hours
gen MET_prepare_food = time_prepare_food_hr *7 * 2
gen time_washed_cloth_hr = K7/60 if K7 > = 0
replace time_washed_cloth_hr = 0 if K6 != 1 & K6 <.
gen MET_washed_cloth = time_washed_cloth_hr *7 * 2.3
gen time_cleaning_hr = K7C/60 if K7C > = 0
replace time_cleaning_hr = 0 if K7B != 1 & K7B <.
gen MET_cleaning = time_cleaning_hr* 3.3 *7


gen time_child_care_hr = K13 if K13 > = 0
gen MET_child_care = time_child_care_hr* 2.5 * 7
gen time_child_care_other_hr = K13C if K13C > = 0
gen MET_child_care_other = time_child_care_other_hr* 2.5 * 7


egen MET_domestic = rowtotal(MET_buy_food MET_prepare_food MET_washed_cloth MET_cleaning), missing //***child care not included in domestic work time because of inconsistent reporting in first two waves***

***Leisure time physical activity***

*track
gen U331_hr = (U331_MN*5)/60 if U331_MN >= 0  // for working days
gen U332_hr = (U332_MN*2)/60 if U332_MN >= 0 // for weekend days
egen pa_track_new = rowtotal(U331_hr U332_hr), missing // total
gen pa_track_hr = U148_MN/60  if wave < 2004  & U148_MN >= 0 // for waves before 2004 is already reported in minutes per week in a different variable
replace pa_track_hr = pa_track_new if wave >= 2004
gen MET_track = pa_track_hr * 7
replace MET_track = 0 if U147 != 1 & U147 <.
*martial arts
gen U327_hr = (U327_MN*5)/60 if U327_MN >= 0
gen U328_hr = (U328_MN*2)/60 if U328_MN >= 0
egen pa_martial_new = rowtotal(U327_hr U328_hr), missing
gen pa_martial_hr = U146_MN /60  if wave < 2004  & U146_MN  >= 0
replace pa_martial_hr = pa_track_new if wave >= 2004
gen MET_martial = pa_martial_hr * 5.3
replace MET_martial = 0 if U145 != 1 & U145 <.
*gymnastics
gen U329_hr = (U329_MN*5)/60 if U329_MN >= 0
gen U330_hr = (U330_MN*2)/60 if U330_MN >= 0
egen pa_gym_new = rowtotal(U329_hr U330_hr), missing
gen pa_gym_hr = U150_MN /60  if wave < 2004  & U150_MN  >= 0
replace pa_gym_hr = pa_gym_new if wave >= 2004
gen MET_gymnastics = pa_gym_hr * 3.8
replace MET_gymnastics = 0 if U149 != 1 & U149 <.
*badminton/volleyball
gen U335_hr = (U335_MN*5)/60 if U335_MN >= 0
gen U336_hr = (U336_MN*2)/60 if U336_MN >= 0
egen pa_badmin_new = rowtotal(U335_hr U336_hr), missing
gen pa_badmin_hr = U154_MN /60  if wave < 2004  & U154_MN  >= 0
replace pa_badmin_hr = pa_badmin_new if wave >= 2004
gen MET_badminton = pa_badmin_hr * 5.5
replace MET_badminton = 0 if U153 != 1 & U153 <.
*soccer/basketball
gen U333_hr = (U333_MN*5)/60 if U333_MN >= 0
gen U334_hr = (U334_MN*2)/60 if U334_MN >= 0
egen pa_socc_new = rowtotal(U333_hr U334_hr), missing
gen pa_socc_hr = U152_MN /60  if wave < 2004  & U152_MN  >= 0
replace pa_socc_hr = pa_socc_new if wave >= 2004
gen MET_soccer = pa_socc_hr * 7
replace MET_soccer = 0 if U151 != 1 & U151 <.
*ping pong
gen U337_hr = (U337_MN*5)/60 if U337_MN >= 0
gen U338_hr = (U338_MN*2)/60 if U338_MN >= 0
egen pa_ping_new = rowtotal(U337_hr U338_hr), missing
gen pa_ping_hr = U156_MN /60  if wave < 2004  & U156_MN  >= 0
replace pa_ping_hr = pa_ping_new if wave >= 2004
gen MET_ping = pa_ping_hr * 4
replace MET_ping = 0 if U155 != 1 & U155 <.

egen MET_leisure = rowtotal(MET_track MET_martial MET_gymnastics MET_badminton MET_soccer MET_ping), missing 


egen MET_PA = rowtotal(MET_leisure MET_worktravel MET_work MET_domestic), missing 


*********check again as bog jump between 2000 and 2004 activity levels probably due to changes in questionnaires. Could make sence to restrict sample to observations after 2000


/*Create lagged LHS variables*/
sort id wave
bysort id: egen seq=seq()  // sequence variable to see how many waves people are in panel


foreach var of varlist lnindwage works stillsmoking anyalc bmi waist d3*{
gen `var'_prev=`var'[_n-1] if seq!=1
label var `var'_prev "Lagged value from previous wave"
}

/* Find those with only one wave reported -- No need to drop them now because we are also interested in those recently diagnosed in 2011
egen countwaves = count(wave), by (id)
drop if countwaves==1  // drop everybody with only 1 wave
*/
/* create baseline values for some of the variables for model as first proposed by Max*/
/*foreach var of varlist lnindwage works age {
gen temp=`var' if seq==1
bysort id: egen `var'_bl = mean(temp)
drop temp
label var `var'_bl "Baseline value"
}
*/


drop temp*


sort id wave
bysort id: egen seq_new=seq()  // new sequence variable after dropping all waves before 1997


label var works "Employed"
label var stillsmoking "Smokes"
label var anyalc "Any alcohol consumption"
label var d3kcal "Kcal (3-day average)"
label var bmi "BMI"
label var waist "Waist circ. (cm)"
label var age "Age"
label var han "Han ethnicity"
label var rural "Rural area"
label var married "Married"
label var secondary "Secondary educ."
label var university "University educ."
label var insurance "Any health insurance"
label var yearsdiagall "Years since diabetes diagnosis"




save "Data/data.dta", replace
*saveold "Data/data_13.dta", replace

 
