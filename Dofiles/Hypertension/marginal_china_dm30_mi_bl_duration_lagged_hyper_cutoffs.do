******************************
********LAGGED dmyet**********
******************************

set more off
capture log close
clear matrix


*global homefolder ""/home/till/Phd/China Data/Data_2014_2""
global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using "Logs/stabweigths_l_hyper.log", replace


use "Data/data_imputed3", clear
*use "Data/data_imputed30", clear


drop if dmyet_bl == 1
drop if age_l == .  //need to drop all missing lagged variables because would mess up calculation of weights. They are missing because these are the lags for the baseline, i.e. there is no prior information available so the lags are missing.
mi update


global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural female ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l university_l married_l insurance_l works_l hhincpc_cpi_l hypertension_l sys_high_l dias_high_l" 
mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl $x if  wave<=firstdmidt | firstdmidt ==.
mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted

mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: drop if (pdmyet ==. & wave<=firstdmidt) | (pdmyet ==.  & firstdmidt ==.)  // There should not be a need to drop missing values here as there should not be any. There are still some because not all baseline values were imputed and are therefor missing, causing the weights to not be calculated. Need to check why they were not imputed and are missing, 
drop pdmyet denom

// by gender


global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural provinc8 provinc7 provinc6 provinc5 provinc4 provinc3 provinc2 hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l university_l married_l insurance_l works_l hhincpc_cpi_l hypertension_l sys_high_l dias_high_l" 


forvalues i=0/1{
mi estimate, esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
eststo mi_predictors_me`i': mimrgns, dydx(*) predict(pr) post
mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav,  esample(denom) storecompleted 
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l married_l insurance_l works_l hhincpc_cpi_l hypertension_l sys_high_l dias_high_l" 

}


//Table for predictors
global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(D{.}{.}{-1}l)    collabels(none) label /// coeflables(1.diabetes Diabetes) 
	
esttab mi_predictors_me0 mi_predictors_me1 using "Tables/predictors_hypertension.tex", replace comp ///
 mti("Males" "Females")    ///
        ${table_reduced} 
	


save "Data/ice3_weighted_dm_l_hypertension", replace

/*
*individual income
// by gender
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl indwage_bl hhincpc_cpi_bl han rural provinc7 provinc6 provinc5 provinc4 provinc3 provinc2 i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l university_l married_l insurance_l indwage_l hhincpc_cpi_l" 

forvalues i=0/1{
mi estimate,  esampvaryok errorok noisily: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
eststo mi_predictors_me`i': mimrgns, dydx(*) predict(pr) post
mi estimate, saving(dmyetden_sav, replace) errorok esample(denom) esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetden_sav, storecompleted esample(denom)
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_wage`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl secondary_bl married_bl insurance_bl indwage_bl hhincpc_cpi_bl han rural provinc8 provinc7 provinc6 provinc5 provinc4 provinc3 provinc2 i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l secondary_l married_l indwage_l hhincpc_cpi_l" 


}

 global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) label nobaselevels /// coeflables(1.diabetes Diabetes) 
	


esttab mi_predictors_me0 mi_predictors_me1 using "Tables/predictors_income.tex", replace comp ///
 mti("Males" "Females")    ///
        ${table_reduced} 

*/

/*USING OBESITY AND OVERWEIGHT MEASUREMENTS BASED ON BMI INSTEAD OF BMI AND WAISTS MEASUREMENTS*/
*global bl "age_bl age_sq_bl index_bl overweight_bl obese_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl hhincpc_cpi_bl han rural i.wave ib45.province"
*global x "age_l age_sq_l overweight obese index_l d3kcal_l smoke_l alc_l secondary_l university_l married_l works_l insurance_l hhincpc_cpi_l" 


/*
mi estimate, saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: gen pdmyet1 = pdmyet
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1


mi rename pdmyet dmyetdenom

mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet $bl if wave<=firstdmidt | firstdmidt ==.
mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_obese =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom

save "Data/ice30_weighted_dm_l", replace
use "Data/ice30_weighted_dm_l", clear
*/
// by gender
global bl "age_bl age_sq_bl index_bl overweight_bl obese_bl obesec_0_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl hhincpc_cpi_bl han rural i.wave ib45.province"
global x "age_l age_sq_l overweight_l obese_l obesec_0_l index_l d3kcal_l smoke_l alc_l secondary_l university_l married_l works_l insurance_l hhincpc_cpi_l" 

forvalues i=0/1{
mi estimate, esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
eststo mi_predictors_me`i': mimrgns, dydx(*) predict(pr) post
mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetden_sav, storecompleted esample(denom)
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_obese`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
global bl "age_bl age_sq_bl index_bl overweight_bl obese_bl obesec_1_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl hhincpc_cpi_bl han rural i.wave ib45.province"
global x "age_l age_sq_l overweight_l obese_l obesec_1_l index_l d3kcal_l smoke_l alc_l secondary_l married_l insurance_l works_l insurance_l hhincpc_cpi_l" 

}

 global table_reduced b(%9.3f) se(%9.3f) wide booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(SS)    collabels(none) label nobaselevels /// coeflables(1.diabetes Diabetes) 
	


esttab mi_predictors_me0 mi_predictors_me1 using "Tables/predictors_obese.tex", replace comp ///
 mti("Males" "Females")    ///
        ${table_reduced} 

estwrite mi_predictors_me* using "savedestimates/diabetes_mi_msm_predictors_hypertension", id() append

save "Data/ice3_weighted_dm_l", replace

log close

log using "Logs/mi_msm_hypertension.log", replace
use "Data/ice3_weighted_dm_l", clear



/*truncate weight to 1 and 99th percentile*/

foreach var of varlist stabweightdmyetall*{
gen `var'_tr1 = `var'
forvalues i= 1/30{
_pctile `var' if _mi_m == `i', nq(100) 
replace `var'_tr1 = r(r99) if `var' > r(r99) & `var' <. & _mi_m == `i'
replace `var'_tr1 = r(r1) if `var' < r(r1) & _mi_m == `i'
	}
}


mi rename stabweightdmyetall_obese0_tr1 stabweight_obese0tr1
mi rename stabweightdmyetall_obese1_tr1 stabweight_obese1tr1

misum stabweight*

eststo stabweights: estpost sum stabweight*

*label var stabweightdmyetall "Untruncated (all)"
label var stabweightdmyetall0 "Untruncated (men)"
label var stabweightdmyetall1 "Untruncated (women)"
*label var stabweightdmyetall_tr1 "Truncated 1 and 99 percentile (all)"
label var stabweightdmyetall0_tr1 "Truncated 1 and 99 percentile (men)"
label var stabweightdmyetall1_tr1 "Truncated 1 and 99 percentile (women)"


*need to drop observations here to make sure that I calculate maximum number of weights using 







 esttab stabweights using "Tables/stabweights_l.tex" , replace cells("mean min max") nonumber mti("Mean" "Min" "Max") ///
 booktabs label 

// have created two globals with controls for regression models. First for works and wage models and the second for all other models
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl lnindinc_bl i.wave hypertension_bl dias_high_bl sys_high_bl"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"
global x_smoke "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"


global x_obese "age_bl age_sq_bl index_bl obese_bl overweight_bl obesec_0_bl obesec_1_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"


/*STABILIZED WEIGHTS UNTRUNCATED*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage [pw=stabweightdmyet_tr] if works == 1, cl(id)
/*
	eststo mi_works_l: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall], cl(id)
	eststo mi_works_l_me: mimrgns, dydx(dmyet) predict(pr) post

	foreach var of varlist overweight obese { // for binary outcomes
	eststo mi_`var'_l: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_obese], cl(id)
	eststo mi_`var'_l_me: mimrgns, dydx(dmyet) predict(pr) post
	}

	foreach var of varlist alc smoke{ // for binary outcomes
	eststo mi_`var'_l: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
	eststo mi_`var'_l_me: mimrgns, dydx(dmyet) predict(pr) post
	}

	foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
	eststo mi_`var'_l: mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id) 
	}
*/
	forvalues i = 0/1{ // by sex
	foreach var of varlist works alc hypertension sys_high dias_high{
	eststo mi_`var'_l_`i': mi est, post errorok or esampvaryok: logistic `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	eststo mi_`var'_l_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}
	/*foreach var of varlist overweight obese obesec_`i'{  //by sex and binary
	eststo mi_`var'_l_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_obese`i'] if female == `i', cl(id)
	eststo mi_`var'_l_me_`i': mimrgns, dydx(dmyet) predict(pr) post
	}*/
	
	eststo mi_smoke_l_`i': mi est, post errorok or esampvaryok: logistic  smoke dmyet $x_smoke  [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	eststo mi_smoke_l_me_`i': mimrgns, dydx(dmyet) predict(pr) post	

	foreach var of varlist bmi waist d3kcal{  // by sex and continuous
	eststo mi_`var'_l_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

	}

}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_hyper", id() replace
*estread mi_* using "savedestimates/diabetes_mi_msm_l_hyper", id() 



//LINEAR DURATION
/*
eststo mi_works_l_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall], cl(id) 
eststo mi_works_l_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese [pw=stabweightdmyetall_obese], cl(id)
eststo mi_`var'_l_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist alc smoke{ // for binary outcomes
eststo mi_`var'_l_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id)
eststo mi_`var'_l_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_dur: mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id) 
}
*/

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl lnindinc_bl i.wave hypertension_bl dias_high_bl sys_high_bl"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl" 

global x_obese "age_bl age_sq_bl index_bl obese_bl overweight_bl obesec_0_bl obesec_1_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave hypertension_bl dias_high_bl sys_high_bl"

forvalues i = 0/1{ // by sex
foreach var of varlist works hypertension sys_high dias_high{

eststo mi_`var'_l_dur_`i': mi est, post errorok or  esampvaryok: logistic `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_`var'_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}
/*
foreach var of varlist overweight obese obesec_`i'{  //by sex and binary
eststo mi_`var'_l_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese  [pw=stabweightdmyetall_obese`i'] if female == `i', cl(id)
eststo mi_`var'_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}
*/

eststo mi_smoke_l_dur_`i': mi est, post errorok or esampvaryok: logistic  smoke yearsdiagall $x_smoke  [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_smoke_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
eststo mi_alc_l_dur_`i': mi est, post errorok or esampvaryok: logistic  alc yearsdiagall $x_other  [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_alc_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_dur_`i': mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append

//DURATION GROUPS
/*
eststo mi_works_l_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall], cl(id) 
eststo mi_works_l_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese] , cl(id)
eststo mi_`var'_l_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist alc smoke{ // for binary outcomes
eststo mi_`var'_l_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
eststo mi_`var'_l_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_dur_g: mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id) 
}


*/

// due to too little ovservations in some groups, have to restrict number of years since diagnosis to before 18 years
forvalues i = 0/1{ // by sex

display `i'
foreach var of varlist works hypertension sys_high dias_high{

eststo mi_`var'_l_dur_g_`i': mi est, post errorok or  esampvaryok: logistic `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
*eststo mi_`var'_l_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}
/*
foreach var of varlist  obese overweight obesec_`i'{  //by sex and binary
eststo mi_`var'_l_dur_g_`i': mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese`i'] if female == `i' & yearsdiagall_g < 5, cl(id)
eststo mi_`var'_l_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}*/
}

eststo mi_smoke_l_dur_g_0: mi est, post errorok or esampvaryok: logistic  smoke i.yearsdiagall_g $x_smoke  [pw=stabweightdmyetall0] if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_smoke_l_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_alc_l_dur_g_0: mi est, post errorok or esampvaryok: logistic  alc i.yearsdiagall_g $x_other  [pw=stabweightdmyetall0] if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_alc_l_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

forvalues i = 0/1{ // by sex
display `i'
foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i']  if female == `i' & yearsdiagall_g < 8, cl(id)
}
}



estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append
estread mi_* using "savedestimates/diabetes_mi_msm_l", id()

coefplot (mi_works_l_dur_g_me_0,label(men) lpattern(solid)) (mi_works_l_dur_g_1, label(women) lpattern(shortdash)) , bylabel("Employed")   || mi_smoke_l_dur_g_me_0 , bylabel("Smoking")|| ///
mi_alc_l_dur_g_me_0 , bylabel("Alcohol") || ///
mi_bmi_l_dur_g_0 mi_bmi_l_dur_g_1, bylabel("BMI") || ///
mi_waist_l_dur_g_0 mi_waist_l_dur_g_1,  bylabel("Waist (cm)")  || ///
mi_d3kcal_l_dur_g_0 mi_d3kcal_l_dur_g_1, bylabel("Kcal") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line)  byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(msm, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_msm_l_all.eps, replace 

coefplot  (mi_overweight_l_dur_g_me_0, label(overweight) lpattern(solid)) (mi_obese_l_dur_g_me_0, label(obese) lpattern(shortdash)), bylabel("Males") || ///
mi_overweight_l_dur_g_me_1 mi_obese_l_dur_g_me_1 , bylabel("Females")   ||, ///
keep(*.yearsdiagall_g) yline(0) recast(line)  byopts(yrescale)  ylabel(#15)  vertical fcolor(none) lwidth(small) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_obese_fe.eps, replace 

graph export mi_msm_l_all_obese.eps, replace 


* Tables

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/
/*esttab mi_works_l mi_smoke_l  mi_alc_l mi_bmi_l mi_waist_l mi_d3kcal_l using "Tables/msm_estimates_mi_l.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_`i' mi_smoke_l_`i'  mi_alc_l_`i' mi_bmi_l_`i' mi_waist_l_`i' mi_d3kcal_l_`i' using "Tables/msm_estimates_mi_l_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


/*MARGINAL EFFECTS*/

/*esttab mi_works_l_me mi_smoke_l_me  mi_alc_l_me mi_bmi_l mi_waist_l mi_d3kcal_l using "Tables/msm_estimates_mi_l_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_me_`i' mi_smoke_l_me_`i'  mi_alc_l_me_`i' mi_bmi_l_`i' mi_waist_l_`i' mi_d3kcal_l_`i' using "Tables/msm_estimates_mi_l_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}



esttab mi_overweight_l_me_0 mi_obese_l_me_0  mi_overweight_l_me_1 mi_obese_l_me_1 using "Tables/msm_estimates_mi_l_obese_me.tex", replace comp ///
 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))



  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
/*esttab mi_works_l_dur mi_smoke_l_dur  mi_alc_l_dur mi_bmi_l_dur mi_waist_l_dur mi_d3kcal_l_dur using "Tables/msm_estimates_mi_l_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_dur_`i' mi_smoke_l_dur_`i'  mi_alc_l_dur_`i' mi_bmi_l_dur_`i' mi_waist_l_dur_`i' mi_d3kcal_l_dur_`i' using "Tables/msm_estimates_mi_l_dur_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_dur_me mi_smoke_l_dur_me  mi_alc_l_dur_me mi_bmi_l_dur mi_waist_l_dur mi_d3kcal_l_dur using "Tables/msm_estimates_mi_l_dur_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_dur_me_`i' mi_smoke_l_dur_me_`i'  mi_alc_l_dur_me_`i' mi_bmi_l_dur_`i' mi_waist_l_dur_`i' mi_d3kcal_l_dur_`i' using "Tables/msm_estimates_mi_l_dur_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}


esttab mi_overweight_l_dur_me_0 mi_obese_l_dur_me_0  mi_overweight_l_dur_me_1 mi_obese_l_dur_me_1 using "Tables/msm_estimates_mi_l_dur_obese_me.tex", replace comp ///
 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))

*Duration groups outcomes

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)    ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/
/*esttab mi_works_l_dur_g mi_smoke_l_dur_g  mi_alc_l_dur_g mi_bmi_l_dur_g mi_waist_l_dur_g mi_d3kcal_l_dur_g using "Tables/msm_estimates_mi_l_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

esttab mi_works_l_dur_g_0 mi_smoke_l_dur_g_0  mi_alc_l_dur_g_0 mi_bmi_l_dur_g_0 mi_waist_l_dur_g_0 mi_d3kcal_l_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_dur_g_1  mi_bmi_l_dur_g_1 mi_waist_l_dur_g_1 mi_d3kcal_l_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_dur_g_me mi_smoke_l_dur_g_me  mi_alc_l_dur_g_me mi_bmi_l_dur_g mi_waist_l_dur_g mi_d3kcal_l_dur_g using "Tables/msm_estimates_mi_l_dur_g_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	

esttab mi_works_l_dur_g_me_0 mi_smoke_l_dur_g_me_0  mi_alc_l_dur_g_me_0 mi_bmi_l_dur_g_0 mi_waist_l_dur_g_0 mi_d3kcal_l_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_me_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_dur_g_me_1  mi_bmi_l_dur_g_1 mi_waist_l_dur_g_1 mi_d3kcal_l_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_me_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)

esttab mi_overweight_l_dur_g_me_0 mi_obese_l_dur_g_me_0  mi_overweight_l_dur_g_me_1 mi_obese_l_dur_g_me_1 using "Tables/msm_estimates_mi_l_dur_g_obese_me.tex", replace comp ///
 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))


log close


log using "Logs/mi_msm_tr.log",  replace

/*STABILIZED WEIGHTS TRUNCATED*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage [pw=stabweightdmyet_tr] if works == 1, cl(id)
/*
eststo mi_works_l_tr: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall_tr], cl(id) 
eststo mi_works_l_tr_me: mimrgns, dydx(dmyet) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_tr: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_tr_obese], cl(id)
eststo mi_`var'_l_tr_me: mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist alc smoke{ // for binary outcomes
eststo mi_`var'_l_tr: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr], cl(id)
eststo mi_`var'_l_tr_me: mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_tr: mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr], cl(id) 
}
*/
forvalues i = 0/1{ // by sex
eststo mi_works_l_tr_`i': mi est, post errorok or esampvaryok: logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_works_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post

foreach var of varlist overweight obese obesec_`i'{  //by sex and binary
eststo mi_`var'_l_tr_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i', cl(id)
eststo mi_`var'_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}


eststo mi_smoke_l_tr_`i': mi est, post errorok or esampvaryok: logistic  smoke dmyet $x_smoke  [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_smoke_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post
eststo mi_alc_l_tr_`i': mi est, post errorok or esampvaryok: logistic  alc dmyet $x_other  [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_alc_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post


foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_tr_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_tr", id() replace

//LINEAR DURATION
/*
eststo mi_works_l_tr_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr], cl(id) 
eststo mi_works_l_tr_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_tr_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese [pw=stabweightdmyetall_tr_obese], cl(id)
eststo mi_`var'_l_tr_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist alc smoke{ // for binary outcomes
eststo mi_`var'_l_tr_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr], cl(id)
eststo mi_`var'_l_tr_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_tr_dur: mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr], cl(id) 
}
*/
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl lnindinc_bl i.wave"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave"

global x_obese "age_bl age_sq_bl index_bl obese_bl overweight_bl obesec_0_bl obesec_1_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl i.wave"

forvalues i = 0/1{ // by sex

eststo mi_works_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_works_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese obesec_`i'{  //by sex and binary
eststo mi_`var'_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i', cl(id)
eststo mi_`var'_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}

eststo mi_smoke_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic  smoke yearsdiagall $x_smoke  [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_smoke_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
eststo mi_alc_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic  alc yearsdiagall $x_other  [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_alc_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_tr_dur_`i': mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)

}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_tr", id() append
estread mi_*tr_* using "savedestimates/diabetes_mi_msm_l_tr", id()

//DURATION GROUPS


eststo mi_obese_l_tr_dur_g_0: mi est, post errorok or esampvaryok: logistic  obese i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese0_tr1] if female == 0 & yearsdiagall_g <5, cl(id)
eststo mi_obese_l_tr_dur_g_me0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post


// due to too little ovservations in some groups, have to restrict number of years since diagnosis to before 18 years
forvalues i = 0/1{ // by sex
eststo mi_works_l_tr_dur_g_`i': mi est, post errorok or esampvaryok: logistic works i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_works_l_tr_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post


foreach var of varlist  obese overweight obesec_`i'{  //by sex and binary
eststo mi_obese_l_tr_dur_g_`i': mi est, post errorok or esampvaryok: logistic  obese i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_obese_l_tr_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_overw_l_tr_dur_g_`i': mi est, post errorok or esampvaryok: logistic  overweight i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_overw_l_tr_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_obesec_`i'_l_tr_dur_g_`i': mi est, post errorok or esampvaryok: logistic  obesec_`i' i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_obesec_`i'_l_tr_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
	}

}
forvalues i = 0/1{ // by sex


foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_tr_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i' & yearsdiagall_g <8, cl(id)
}
}

foreach var of varlist smoke alc{
eststo mi_smoke_l_tr_dur_g_0: mi est, post errorok or esampvaryok: logistic  smoke i.yearsdiagall_g $x_smoke  [pw=stabweightdmyetall0_tr1] if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_smoke_l_tr_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_alc_l_tr_dur_g_0: mi est, post errorok or esampvaryok: logistic  alc i.yearsdiagall_g $x_other  [pw=stabweightdmyetall0_tr1] if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_alc_l_tr_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append
estread mi_* using "savedestimates/diabetes_mi_msm_l", id() 

coefplot (mi_works_l_tr_dur_g_me_0,label(men) lpattern(solid)) (mi_works_l_tr_dur_g_1, label(women) lpattern(shortdash)) , bylabel("Employed")   || mi_smoke_l_tr_dur_g_me_0 , bylabel("Smoking")|| ///
mi_alc_l_tr_dur_g_me_0 , bylabel("Alcohol") || ///
mi_bmi_l_tr_dur_g_0 mi_bmi_l_tr_dur_g_1, bylabel("BMI") || ///
mi_waist_l_tr_dur_g_0 mi_waist_l_tr_dur_g_1,  bylabel("Waist (cm)")  || ///
mi_d3kcal_l_tr_dur_g_0 mi_d3kcal_l_tr_dur_g_1, bylabel("Kcal") ||, ///
keep(*.yearsdiagall_g ) yline(0) recast(line)  byopts(yrescale) ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)

graph export mi_msm_l_tr_all.eps, replace 

coefplot  (mi_overw_l_tr_dur_g_me0, label(overweight) lpattern(solid)) (mi_obese_l_tr_dur_g_me0, label(obese) lpattern(shortdash)), bylabel("Males") || ///
mi_overw_l_tr_dur_g_me1 mi_obese_l_tr_dur_g_me1 , bylabel("Females")   ||, ///
keep(*.yearsdiagall_g) yline(0) recast(line)  byopts(yrescale)  ylabel(#15)  vertical fcolor(none) lwidth(small) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_obese_fe.eps, replace 

graph export mi_msm_l_tr_all_obese.eps, replace 



* Tables

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/
/*esttab mi_works_l_tr mi_smoke_l_tr  mi_alc_l_tr mi_bmi_l_tr mi_waist_l_tr mi_d3kcal_l_tr using "Tables/msm_estimates_mi_l_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_tr_`i' mi_smoke_l_tr_`i'  mi_alc_l_tr_`i' mi_bmi_l_tr_`i' mi_waist_l_tr_`i' mi_d3kcal_l_tr_`i' using "Tables/msm_estimates_mi_l_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_tr_me mi_smoke_l_tr_me  mi_alc_l_tr_me mi_bmi_l_tr mi_waist_l_tr mi_d3kcal_l_tr using "Tables/msm_estimates_mi_l_me_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_tr_me_`i' mi_smoke_l_tr_me_`i'  mi_alc_l_tr_me_`i' mi_bmi_l_tr_`i' mi_waist_l_tr_`i' mi_d3kcal_l_tr_`i' using "Tables/msm_estimates_mi_l_me_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}



  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
/*esttab mi_works_l_tr_dur mi_smoke_l_tr_dur  mi_alc_l_tr_dur mi_bmi_l_tr_dur mi_waist_l_tr_dur mi_d3kcal_l_tr_dur using "Tables/msm_estimates_mi_l_dur_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_tr_dur_`i' mi_smoke_l_tr_dur_`i'  mi_alc_l_tr_dur_`i' mi_bmi_l_tr_dur_`i' mi_waist_l_tr_dur_`i' mi_d3kcal_l_tr_dur_`i' using "Tables/msm_estimates_mi_l_dur_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_tr_dur_me mi_smoke_l_tr_dur_me  mi_alc_l_tr_dur_me mi_bmi_l_tr_dur mi_waist_l_tr_dur mi_d3kcal_l_tr_dur using "Tables/msm_estimates_mi_l_dur_me_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_tr_dur_me_`i' mi_smoke_l_tr_dur_me_`i'  mi_alc_l_tr_dur_me_`i' mi_bmi_l_tr_dur_`i' mi_waist_l_tr_dur_`i' mi_d3kcal_l_tr_dur_`i' using "Tables/msm_estimates_mi_l_dur_me_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}

forvalues i=0/1{
esttab mi_overweight_l_tr_dur_me_`i' mi_obese_l_tr_dur_me_`i' mi_obesec_`i'_l_tr_dur_me_`i' using "Tables/msm_estimates_mi_l_tr_dur_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced}
}



*Duration groups outcomes

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)    ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/
/*esttab mi_works_l_tr_dur_g mi_smoke_l_tr_dur_g  mi_alc_l_tr_dur_g mi_bmi_l_tr_dur_g mi_waist_l_tr_dur_g mi_d3kcal_l_tr_dur_g using "Tables/msm_estimates_mi_l_dur_g_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

esttab mi_works_l_tr_dur_g_0 mi_smoke_l_tr_dur_g_0  mi_alc_l_tr_dur_g_0 mi_bmi_l_tr_dur_g_0 mi_waist_l_tr_dur_g_0 mi_d3kcal_l_tr_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_0_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_tr_dur_g_1  mi_bmi_l_tr_dur_g_1 mi_waist_l_tr_dur_g_1 mi_d3kcal_l_tr_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_1_tr.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)


/*MARGINAL EFFECTS*/

/*esttab mi_works_l_tr_dur_g_me mi_smoke_l_tr_dur_g_me  mi_alc_l_tr_dur_g_me mi_bmi_l_tr_dur_g mi_waist_l_tr_dur_g mi_d3kcal_l_tr_dur_g using "Tables/msm_estimates_mi_l_dur_g_me_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */



esttab mi_works_l_tr_dur_g_me_0 mi_smoke_l_tr_dur_g_me_0  mi_alc_l_tr_dur_g_me_0 mi_bmi_l_tr_dur_g_0 mi_waist_l_tr_dur_g_0 mi_d3kcal_l_tr_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_me_0_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_tr_dur_g_me_1  mi_bmi_l_tr_dur_g_1 mi_waist_l_tr_dur_g_1 mi_d3kcal_l_tr_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_me_1_tr.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)

forvalues i=0/1{
esttab mi_overw_l_tr_dur_g_me`i' mi_obese_l_tr_dur_g_me`i' mi_obesec_`i'_l_tr_dur_g_me`i' using "Tables/msm_estimates_mi_l_tr_dur_g_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}


log close
/*
log using Logs/china_dm30_results_l_covariates.log, replace


/*ONLY COVARIATE ADJUSTED*/

use "Data/data_imputed30", clear



drop if dmyet_bl == 1


global x_prod "age age_sq female rural han ib45.province index d3kcal smoke alc secondary university married insurance hhincpc_cpi i.wave"
global x_binary "age age_sq female rural han ib45.province bmi index waist d3kcal secondary university married works insurance hhincpc_cpi i.wave"
global x_obese "age age_sq female rural han ib45.province index d3kcal secondary university married works insurance hhincpc_cpi i.wave"

global x_cont "age age_sq female rural han ib45.province alc smoke secondary university married works insurance hhincpc_cpi i.wave"

global x "age age_sq female rural han ib45.province bmi index waist d3kcal smoke alc secondary university married works insurance hhincpc_cpi i.wave" 


forvalues i = 0/1{
eststo mi_works_cov_`i': mi est, post errorok or: logistic works dmyet $x_prod if female == `i', cl(id)
eststo mi_works_cov_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}

forvalues i = 0/1{
eststo mi_alc_cov_`i': mi est, post errorok or: logistic  alc dmyet $x_binary smoke if female == `i' , cl(id)
eststo mi_alc_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_smoke_cov_`i': mi est, post errorok or: logistic  smoke dmyet $x_binary alc if female == `i', cl(id)
eststo mi_smoke_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
}

forvalues i = 0/1{
eststo mi_obese_cov_`i': mi est, post errorok or: logistic  obese dmyet $x_obese alc smoke if female == `i' , cl(id)
eststo mi_obese_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_overweight_cov_`i': mi est, post errorok or: logistic  overweight dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_overweight_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_obesec_`i'_cov_`i': mi est, post errorok or: logistic  obesec_`i' dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_obesec_`i'_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
}
forvalues i = 0/1{
eststo mi_bmi_cov_`i': mi est, post errorok: reg  bmi dmyet $x_cont d3kcal if female == `i', cl(id) ro
eststo mi_waist_cov_`i': mi est, post errorok: reg  waist dmyet $x_cont d3kcal if female == `i', cl(id) ro 
eststo mi_d3kcal_cov_`i': mi est, post errorok: reg  d3kcal dmyet $x_cont if female == `i', cl(id) ro
}


/*DURATION*/



forvalues i = 0/1{
eststo mi_works_cov_dur_`i': mi est, post errorok or: logistic works yearsdiagall $x_prod if female == `i', cl(id)
eststo mi_works_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}
forvalues i = 0/1{
eststo mi_alc_cov_dur_`i': mi est, post errorok or: logistic  alc yearsdiagall $x_binary smoke if female == `i' , cl(id)
eststo mi_alc_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
eststo mi_smoke_cov_dur_`i': mi est, post errorok or: logistic  smoke yearsdiagall $x_binary alc if female == `i', cl(id)
eststo mi_smoke_cov_dur_me_`i':mimrgns, dydx(yearsdiagall) predict(pr)
 }
 forvalues i = 0/1{
eststo mi_obese_cov_dur_`i': mi est, post errorok or: logistic  obese dmyet $x_obese alc smoke if female == `i' , cl(id)
eststo mi_obese_cov_dur_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_overweight_cov_dur_`i': mi est, post errorok or: logistic  overweight dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_overweight_cov_dur_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_obesec_`i'_cov_dur_`i': mi est, post errorok or: logistic  obesec_`i' dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_obesec_`i'_cov_dur_me_`i':mimrgns, dydx(dmyet) predict(pr) post
}

forvalues i = 0/1{
eststo mi_bmi_cov_dur_`i': mi est, post errorok: reg  bmi yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro
eststo mi_waist_cov_dur_`i': mi est, post errorok: reg  waist yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro 
eststo mi_d3kcal_cov_dur_`i': mi est, post errorok: reg  d3kcal yearsdiagall $x_cont if female == `i', cl(id) ro
}



/*DURATION GROUPS*/

forvalues i = 0/1{
eststo mi_works_cov_dur_g_`i': mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod if female == `i', cl(id)
eststo mi_works_cov_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

}

// due to too little ovservations in some groups, have to restrict number of years since diagnosis to before 18 years
forvalues i = 0/1{
eststo mi_obese_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic  obese i.yearsdiagall_g $x_obese if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_obese_cov_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_overw_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic  overweight i.yearsdiagall_g $x_obese  if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_overw_cov_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_obesec_`i'_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic  obesec_`i' i.yearsdiagall_g $x_obese if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_obesec_`i'_cov_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}


foreach var of varlist smoke alc{
eststo mi_smoke_cov_dur_g_0: mi est, post errorok or esampvaryok: logistic  smoke i.yearsdiagall_g $x_binary alc  if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_smoke_cov_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_alc_cov_dur_g_0: mi est, post errorok or esampvaryok: logistic  alc i.yearsdiagall_g $x_binary smoke if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_alc_cov_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

forvalues i = 0/1{ // by sex
foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_cov_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_cont  if female == `i' & yearsdiagall_g <8, cl(id)
}
}



estwrite mi_*cov* using "savedestimates/diabetes_mi_msm_l_cov", id() replace


* Tables

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/

forvalues i=0/1{
esttab mi_works_cov_`i' mi_smoke_cov_`i'  mi_alc_cov_`i' mi_bmi_cov_`i' mi_waist_cov_`i' mi_d3kcal_cov_`i' using "Tables/msm_estimates_mi_cov_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

	
forvalues i=0/1{
esttab mi_works_cov_me_`i' mi_smoke_cov_me_`i'  mi_alc_cov_me_`i' mi_bmi_cov_`i' mi_waist_cov_`i' mi_d3kcal_cov_`i' using "Tables/msm_estimates_mi_cov_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}


forvalues i=0/1{
esttab mi_overweight_cov_me_`i' mi_obese_cov_me_`i' mi_obesec_`i'_cov_me_`i' using "Tables/msm_estimates_mi_cov_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
forvalues i=0/1{
esttab mi_works_cov_dur_`i' mi_smoke_cov_dur_`i'  mi_alc_cov_dur_`i' mi_bmi_cov_dur_`i' mi_waist_cov_dur_`i' mi_d3kcal_cov_dur_`i' using "Tables/msm_estimates_mi_cov_dur_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

	
forvalues i=0/1{
esttab mi_works_cov_dur_me_0 mi_smoke_cov_dur_me_`i'  mi_alc_cov_dur_me_`i' mi_bmi_cov_dur_`i' mi_waist_cov_dur_`i' mi_d3kcal_cov_dur_`i' using "Tables/msm_estimates_mi_cov_dur_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}

forvalues i=0/1{
esttab mi_overweight_cov_dur_me_`i' mi_obese_cov_dur_me_`i' mi_obesec_`i'_cov_dur_me_`i' using "Tables/msm_estimates_mi_cov_dur_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}



*Duration groups outcomes

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)    ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/

forvalues i=0/1{
esttab mi_works_cov_dur_g_`i' mi_smoke_cov_dur_g_`i'  mi_alc_cov_dur_g_`i' mi_bmi_cov_dur_g_`i' mi_waist_cov_dur_g_`i' mi_d3kcal_cov_dur_g_`i' using "Tables/msm_estimates_mi_cov_dur_g_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/


esttab mi_works_cov_dur_g_me_0 mi_smoke_cov_dur_g_me_0  mi_alc_cov_dur_g_me_0 mi_bmi_cov_dur_g_0 mi_waist_cov_dur_g_0 mi_d3kcal_cov_dur_g_0 using "Tables/msm_estimates_mi_cov_dur_g_me_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_cov_dur_g_me_1  mi_bmi_cov_dur_g_1 mi_waist_cov_dur_g_1 mi_d3kcal_cov_dur_g_1 using "Tables/msm_estimates_mi_cov_dur_g_me_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)

forvalues i=0/1{
esttab mi_overw_cov_dur_g_me`i' mi_obese_cov_dur_g_me`i' mi_obesec_`i'_cov_dur_g_me`i' using "Tables/msm_estimates_mi_cov_dur_g_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}


log close
