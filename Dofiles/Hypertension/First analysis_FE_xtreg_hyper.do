set more off
capture log close
clear matrix
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
global homefolder ""/home/till/Phd/China Data/Data_2014""

cd $homefolder



/*USE FE LOGIT HERE INSTEAD OF MUNDLAK DEVICE*/

****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************

use "Data/data_imputed3", clear

drop if dmyet_bl == 1
drop if maxseq == 1

global x "yearsdiagall age_sq female i.wave han ib(freq).province rural married secondary university insurance index hhincpc_cpi"

label var yearsdiagall "Years since diagnosis"


log using Logs/fe_duration_mi_results_xtreg_hyper.log, replace

/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo mi_works_fe_dur`i': mi est, post cmdok esampvaryok : xtreg works $x if female == `i', fe ro  

foreach var of varlist smoke alc obese overweight obesec_`i' hypertension sys_high dias_high bmi waist d3kcal{
eststo mi_`var'_fe_dur`i': mi est, post cmdok esampvaryok : xtreg `var' $x works if female == `i', fe ro  
}
}

estwrite mi_*_dur* using "savedestimates/diabetes_duration_mi_fe_xtreg_hyper", id() replace
estread mi_*_dur*  using "savedestimates/diabetes_duration_mi_fe_xtreg_hyper"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs nolz   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")



forvalues i=0/1{
esttab mi_works_fe_dur`i' mi_smoke_fe_dur`i'  mi_alc_fe_dur`i' mi_bmi_fe_dur`i' mi_waist_fe_dur`i' mi_kcal_fe_dur`i' using "Tables/Hypertension/FE_estimates_mi_xtreg_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
   
  *Obesity and Overweight     
   esttab  mi_overweight_fe_dur0 mi_obese_fe_dur0 mi_overweight_fe_dur1 mi_obese_fe_dur1 using "Tables/Hypertension/FE_estimates_mi_xtreg_obese_dur.tex", replace comp ///
	 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))

           
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell eform(1 1 1 0 0 0) ///
	 collabels(none) keep(yearsdiagall) label  nobaselevels /// 
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")



forvalues i=0/1{
esttab mi_works_fe_dur`i' mi_smoke_fe_dur`i'  mi_alc_fe_dur`i' mi_bmi_fe_dur`i' mi_waist_fe_dur`i' mi_kcal_fe_dur`i' using "Tables/Hypertension/FE_estimates_mi_xtreg_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close




log using Logs/fe_binary_mi_results_xtreg.log, replace
***************************************
/*+++++Binary diabetes indicator+++++*/
***************************************

use "Data/data_imputed3", clear

drop if dmyet_bl == 1
drop if maxseq == 1


global x "dmyet age_sq female i.wave han rural married secondary university insurance index ib(freq).province hhincpc_cpi"


//eststo mi_works_fe_ia: mi est, post cmdok esampvaryok : xtreg works $x c.c__dmyet#i.female c.m__dmyet#i.female female, fe ro  

/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo mi_works_fe`i': mi est, post cmdok esampvaryok : xtreg works $x if female == `i'& wave > 1997, fe ro  

foreach var of varlist  hypertension systolic diastolic sys_high dias_high {
eststo mi_`var'_fe`i': mi est, post cmdok esampvaryok : xtreg `var' $x works if female == `i' & wave > 1997, fe ro  
}
}


estwrite mi_* using "savedestimates/diabetes_dummy_mi_fe_xtreg", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_fe_xtreg"


/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs nolz  ///
	 alignment(S S)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")

	forvalues i=0/1{
	esttab mi_works_fe`i' mi_smoke_fe`i'  mi_alc_fe`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/Hypertension/FE_estimates_mi_xtreg`i'.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
	       }
	
		*Obesity and overweight	
	
	esttab  mi_overweight_fe0 mi_obese_fe0 mi_overweight_fe1 mi_obese_fe1 using "Tables/Hypertension/FE_estimates_mi_xtreg_obese.tex", replace comp ///
	 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))
	            



	      
*Word
  global table_reduced b(%9.3f) se(%9.3f)  nolz noobs  ///
	 collabels(none) keep(dmyet) label nobaselevels ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_fe`i' mi_smoke_fe`i'  mi_alc_fe`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/Hypertension/FE_estimates_mi_xtreg`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


estwrite mi_* using "savedestimates/diabetes_dummy_mi_fe_xtreg", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_fe_xtreg"


capture log close


***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************
global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
capture log close

use "Data/data_imputed30", clear
drop if dmyet_bl == 1
drop if maxseq == 1

log using Logs/fe_durationgroups_mi_xtreg.log, append

global x "i.yearsdiagall_g age_sq female i.wave i.han ib(freq).province rural married secondary university insurance index hhincpc_cpi"



/*BY GENDER*/

	* normally use loops, but here need to estimate them seperatley as for female smoke regression gives error because ommits different groups between datasets, i.e. ommits > 20 years duration in some due to lack of data. Thefore exclude this group in female estimation, but not in male
eststo mi_works_fe_g0: mi est, post esampvaryok errorok: xtreg works $x if female == 0, fe ro 
eststo mi_smoke_fe_g0: mi est, post esampvaryok errorok: xtreg smoke $x  works if female == 0, fe ro 
eststo mi_alc_fe_g0: mi est, post esampvaryok errorok: xtreg alc $x i.province works if female == 0, fe ro  

eststo mi_works_fe_g1: mi est, post cmdok esampvaryok errorok: xtreg works $x if female == 1, fe ro 
eststo mi_smoke_fe_g1: mi est, post cmdok esampvaryok noisily: xtreg smoke $x  works if female == 1, fe ro 
eststo mi_alc_fe_g1: mi est, post cmdok esampvaryok errorok: xtreg alc $x i.province works if female == 1, fe ro  

label var bmi "BMI" 
label var waist "Waist circumference (cm)" 
label var works "Employment status"
label var alc "Any alcohol consumption"
label var smoke "Smoking status"
label var obese "Obese"
label var overweight "Overweight"
label var d3kcal "Kcal"




	*Using xtreg and later mimargns command to compare marginal effects to xtreg results

forvalues i =0/1{
eststo mi_obese_fe_g`i': mi est, post cmdok esampvaryok: xtreg obese $x works if female == `i', fe ro 
eststo mi_overweight_fe_g`i': mi est, post cmdok esampvaryok: xtreg overweight $x works if female == `i', fe ro 
eststo mi_bmi_fe_g`i': mi est, post cmdok esampvaryok : xtreg bmi $x works if female == `i', fe ro 
eststo mi_waist_fe_g`i': mi est, post cmdok esampvaryok errorok: xtreg waist $x works if female == `i', fe ro 
eststo mi_kcal_fe_g`i': mi est, post cmdok esampvaryok errorok: xtreg d3kcal $x works if female == `i', fe ro 
}

estwrite mi_*_g* using "savedestimates/diabetes_durationgroups_mi_fe_xtreg", id() replace
estread mi_*_g* using "savedestimates/diabetes_durationgroups_mi_fe_xtreg"

*PLOTTING ALL DURATION GROUP RESULTS*
coefplot mi_works_fe_g0 mi_works_fe_g1 , bylabel("Employed")   || mi_smoke_fe_g0 mi_smoke_fe_g1, bylabel("Smoking")|| ///
 mi_alc_fe_g0 mi_alc_fe_g1, bylabel("Alcohol") || ///
(mi_bmi_fe_g0, label(men) lpattern(solid)) (mi_bmi_fe_g1, label(women) lpattern(shortdash)) , bylabel("BMI") || ///
mi_waist_fe_g0 mi_waist_fe_g1,  bylabel("Waist (cm)") || ///
mi_kcal_fe_g0 mi_kcal_fe_g1, bylabel("Kcal") ||, ///
keep(*.yearsdiagall_g) yline(0) recast(line)  byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_fe.eps, replace 

coefplot  (mi_overweight_fe_g0, label(overweight) lpattern(solid)) (mi_obese_fe_g0, label(obese) lpattern(shortdash)), bylabel("Males") || ///
mi_overweight_fe_g1 mi_obese_fe_g1 , bylabel("Females")   ||, ///
keep(*.yearsdiagall_g) yline(0) recast(line)  byopts(yrescale)  ylabel(#15)  vertical fcolor(none) lwidth(small) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_obese_fe.eps, replace 






/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) ///
	 alignment(S S)   collabels(none) keep(*yearsdiagall_g) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_fe_g`i' mi_smoke_fe_g`i'  mi_alc_fe_g`i' mi_bmi_fe_g`i' mi_waist_fe_g`i' mi_kcal_fe_g`i' using "Tables/Hypertension/FE_estimates_mi_xtreg_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
       }
       
         *Obesity and Overweight     
   esttab  mi_overweight_fe_g0 mi_obese_fe_g0 mi_overweight_fe_g1 mi_obese_fe_g1 using "Tables/Hypertension/FE_estimates_mi_xtreg_obese_dur_g.tex", replace comp ///
	 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))

*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell  eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0)) ///
	 collabels(none) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_fe_g`i' mi_smoke_fe_g`i'  mi_alc_fe_g`i' mi_bmi_fe_g`i' mi_waist_fe_g`i' mi_kcal_fe_g`i' using "Tables/Hypertension/FE_estimates_mi_xtreg_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close



