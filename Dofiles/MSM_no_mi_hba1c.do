set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using Logs/china_non_msm_hba1c.log, replace

//use "ice30_cluster_miss.dta", clear
use "Data/data.dta", clear

xtset id wave
drop *seq*
* drop individuals if in only one wave
bysort id (wave): egen seq=seq()  // creates number of waves that people have went through
bysort id (wave): egen maxseq=max(seq)  // indicates the maximum number of waves an individual attended. I need to drop all those with only one wave because for those the marginal structural model will not work as they have no prior information

drop if maxseq==1
*drop if dmyet_bl == 1

* gen biomarker variables

by id: egen hba1c_09 = max(hba1c)
by id: egen gluc_09 = max(glucose_mg)

* below will give me first wave where person receives diabetes diagnosis.
sort id wave
egen temp=min(wave) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp


foreach var of varlist age age_sq wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works dmyet{
bysort id (wave): gen `var'_bl= `var'[1] 
}


label var dmyet "Self-reported diabetes"
label var yearsdiagall "Diabetes duration"

*Generate duration groups
drop yearsdiagall_*
replace yearsdiagall = 0 if dmyet == 0

gen yearsdiagall_r = round(yearsdiagall,1)
recode yearsdiagall_r (0/1 = 1) ///
			(2/3 = 2) ///
			(4/5 = 3) ///
			(6/7 = 4) ///
			(8/9 = 5) ///
			(10/11 = 6) ///
			(12/13 = 7) ///
			(14/15 = 8) ///
			(16/20 = 9) ///
			(21/100 = 10) (-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g)
			
replace yearsdiagall_g = 0 if dmyet == 0			
label define yearsdiagall_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-10" 7 "11-12" 8 "13-14" 9 "15-19" 10 "20+", replace
label value yearsdiagall_g yearsdiagall_g
label var yearsdiagall_g "Years since diagnosis in groups"

/*CREATING STABILIZED WEIGHTS*/

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl i.wave han rural female ib(freq).province i.wave hba1c_09"
global x "age age_sq bmi index waist d3kcal stillsmoking anyalc secondary university married works insurance" 
/*For dmyet as explanatory variable*/
eststo predictors: logistic dmyet $bl $x if wave<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if wave>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
rename pdmyet dmyetdenom

logistic dmyet $bl if wave<=firstdmidt | firstdmidt ==.
predict pdmyet if e(sample)
replace pdmyet=1 if wave>firstdmidt
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet)
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
rename pdmyet dmyetnum
gen stabweightdmyetall =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum

// by gender
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl i.wave han rural ib(freq).province  i.wave hba1c_09"
global x "age age_sq bmi index waist d3kcal stillsmoking anyalc secondary university married works insurance" 

forvalues i=0/1{
eststo predictors`i': logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
predict pdmyet if e(sample)
replace pdmyet=1 if wave>firstdmidt & female == `i'
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet) if female == `i'
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1  & female == `i'
rename pdmyet dmyetdenom
logistic dmyet age_bl age_sq_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt & female == `i'
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) if female == `i'
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1 & female == `i'
rename pdmyet dmyetnum
gen stabweightdmyetall`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum

}

save "Data/data_msm_gluc", replace


*Table for predictors*
  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) eform label nobaselevels ///
	
*LATEX
esttab predictors predictors0 predictors1 using "Tables/noMI/predictors.tex", replace comp ///
 mti("Complete sample" "Male" "Female")    ///
        ${table_reduced} 



use "Data/data_msm_gluc", clear


/*truncate weight to 1 and 99th percentile*/

foreach var of varlist stabweight*{
gen `var'_tr1 = `var'
gen `var'_tr2 = `var'
gen `var'_tr5 = `var'
_pctile `var', nq(100) 
replace `var'_tr1 = r(r99) if `var' > r(r99) & `var' <.
replace `var'_tr1 = r(r1) if `var' < r(r1) 
replace `var'_tr2 = r(r98) if `var' > r(r98) & `var' <.
replace `var'_tr2 = r(r2) if `var' < r(r2) 
replace `var'_tr5 = r(r95) if `var' > r(r95) & `var' <.
replace `var'_tr5 = r(r5) if `var' < r(r5)
}


sum stabweight*
log close

eststo stabweights: estpost sum stabweight*

label var stabweightdmyetall "Untruncated (all)"
label var stabweightdmyetall0 "Untruncated (men)"
label var stabweightdmyetall1 "Untruncated (women)"
label var stabweightdmyetall_tr1 "Truncated 1 and 99 percentile (all)"
label var stabweightdmyetall0_tr1 "Truncated 1 and 99 percentile (men)"
label var stabweightdmyetall1_tr1 "Truncated 1 and 99 percentile (women)"
label var stabweightdmyetall_tr2 "Truncated 2 and 98 percentile (all)"
label var stabweightdmyetall0_tr2 "Truncated 2 and 98 percentile (men)"
label var stabweightdmyetall1_tr2 "Truncated 2 and 98 percentile (women)"
label var stabweightdmyetall_tr5 "Truncated 5 and 95 percentile (all)"
label var stabweightdmyetall0_tr5 "Truncated 5 and 95 percentile (men)"
label var stabweightdmyetall1_tr5 "Truncated 5 and 95 percentile (women)"



 esttab stabweights using "Tables/noMI/stabweights_gluc.tex" , replace cells("mean min max") nonumber mti("Mean" "Min" "Max") ///
 booktabs label 

// have created two globals with controls for regression models. First for works and wage models and the second for all other models

global x_prod "age_bl age_sq_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave hba1c_09"
global x_other "age_bl age_sq_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave hba1c_09"


log using Logs/msm_gluc_results_no_truncation.log, replace
/*NO TRUNCATION*/


/*Stabilized weights*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo works:  logistic works dmyet $x_prod [pw=stabweightdmyetall], cl(id)

margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var':  logistic  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
margins, dydx(dmyet) 
}

foreach var of varlist bmi waist d3kcal{
eststo `var':  reg  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
}

forvalues i = 0/1{
eststo works_`i':  logistic works dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{
eststo `var'_`i':  logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_`i':  reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}




/*DURATION*/

eststo works_dur:  logistic works yearsdiagall $x_prod [pw=stabweightdmyetall], cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id)
margins, dydx(yearsdiagall) 
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id)
}

forvalues i = 0/1{
eststo works_`i'_dur:  logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{
eststo `var'_`i'_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_`i'_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}



/*DURATION GROUPS*/

eststo works_dur_g:  logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall], cl(id)
//margins, dydx(i.yearsdiagall_g) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_dur_g:  logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
//margins, dydx(i.yearsdiagall_g) 
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_dur_g:  reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
}

eststo stillsmoking_0_dur_g:  logistic  stillsmoking i.yearsdiagall_g $x_other [pw=stabweightdmyetall0] if female == 0, cl(id)
eststo stillsmoking_1_dur_g:  logistic  stillsmoking i.yearsdiagall_g $x_other [pw=stabweightdmyetall1] if female == 1, cl(id)  // need to limit it to those with duration of less then 11 years this because sample size those with longer diabetes duration is to small so that they get ommitted which leads to problem for using mi. Also need to include varying sample option, because a different number of observations are now ommited in each dataset

forvalues i = 0/1{
eststo works_`i'_dur_g:  logistic works i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
//margins, dydx(i.yearsdiagall_g) 
eststo anyalc_`i'_dur_g:  logistic  anyalc i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

foreach var of varlist bmi waist d3kcal{
eststo `var'_`i'_dur_g:  reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}


estwrite * using "savedestimates/diabetes_msm_gluc", id() replace
estread * using "savedestimates/diabetes_msm_gluc"
	*Tables
	
*Binary outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works stillsmoking  anyalc bmi waist d3kcal using "Tables/noMI/msm_gluc_estimates_mi.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_`i' stillsmoking_`i'  anyalc_`i' bmi_`i' waist_`i' d3kcal_`i' using "Tables/noMI/msm_gluc_estimates_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_dur stillsmoking_dur  anyalc_dur bmi_dur waist_dur d3kcal_dur using "Tables/noMI/msm_gluc_estimates_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_`i'_dur stillsmoking_`i'_dur  anyalc_`i'_dur bmi_`i'_dur waist_`i'_dur d3kcal_`i'_dur using "Tables/noMI/msm_gluc_estimates_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}


*Duration groups outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall_g*) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_dur_g stillsmoking_dur_g  anyalc_dur_g bmi_dur_g waist_dur_g d3kcal_dur_g using "Tables/noMI/msm_gluc_estimates_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_`i'_dur_g stillsmoking_`i'_dur_g  anyalc_`i'_dur_g bmi_`i'_dur_g waist_`i'_dur_g d3kcal_`i'_dur_g using "Tables/noMI/msm_gluc_estimates_`i'_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}


log close

log using Logs/msm_gluc_results_tr1.log, replace

/*STABILIZED WEIGHTS TRUNCATED AT 1 PERCENTILE*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo works_tr1:  logistic works dmyet $x_prod [pw=stabweightdmyetall_tr1], cl(id) 
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_tr1:  logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr1], cl(id)
margins, dydx(dmyet) 
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo `var'_tr1:  reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr1], cl(id) 
}

forvalues i = 0/1{ // by sex
eststo works_tr1_`i':  logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{  //by sex and binary
eststo `var'_tr1_`i':  logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo `var'_tr1_`i':  reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}



/*DURATION*/

eststo works_tr1_dur:  logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr1], cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_tr1_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr1], cl(id)
margins, dydx(yearsdiagall) 
}

foreach var of varlist bmi waist d3kcal{  // continuous
eststo `var'_tr1_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr1], cl(id)
}

forvalues i = 0/1{  // by sex
eststo works_tr1_`i'_dur:  logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{  // by sex and binary
eststo `var'_tr1_`i'_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo `var'_tr1_`i'_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}


estwrite * using "savedestimates/diabetes_msm_gluc", id() replace



* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab works_tr1 stillsmoking_tr1  anyalc_tr1 bmi_tr1 waist_tr1 d3kcal_tr1 using "Tables/noMI/msm_estimates_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_tr1_`i' stillsmoking_tr1_`i'  anyalc_tr1_`i' bmi_tr1_`i' waist_tr1_`i' d3kcal_tr1_`i' using "Tables/noMI/msm_gluc_estimates_tr1_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_tr1_dur stillsmoking_tr1_dur  anyalc_tr1_dur bmi_tr1_dur waist_tr1_dur d3kcal_tr1_dur using "Tables/noMI/msm_gluc_estimates_tr1_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_tr1_`i'_dur stillsmoking_tr1_`i'_dur  anyalc_tr1_`i'_dur bmi_tr1_`i'_dur waist_tr1_`i'_dur d3kcal_tr1_`i'_dur using "Tables/noMI/msm_gluc_estimates_tr1_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

log close


log using Logs/msm_gluc_results_tr2.log, replace

/*STABILIZED WEIGHTS TRUNCATED AT 2 PERCENTILE*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo works_tr2:  logistic works dmyet $x_prod [pw=stabweightdmyetall_tr2], cl(id) 
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_tr2:  logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr2], cl(id)
margins, dydx(dmyet) 
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo `var'_tr2:  reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr2], cl(id) 
}

forvalues i = 0/1{ // by sex
eststo works_tr2_`i':  logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{  //by sex and binary
eststo `var'_tr2_`i':  logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo `var'_tr2_`i':  reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}
}



/*DURATION*/

eststo works_tr2_dur:  logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr2], cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_tr2_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr2], cl(id)
margins, dydx(yearsdiagall) 
}

foreach var of varlist bmi waist d3kcal{  // continuous
eststo `var'_tr2_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr2], cl(id)
}

forvalues i = 0/1{  // by sex
eststo works_tr2_`i'_dur:  logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{  // by sex and binary
eststo `var'_tr2_`i'_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo `var'_tr2_`i'_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}
}


estwrite * using "savedestimates/diabetes_msm_gluc", id() append



* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab works_tr2 stillsmoking_tr2  anyalc_tr2 bmi_tr2 waist_tr2 d3kcal_tr2 using "Tables/noMI/msm_gluc_estimates_tr2.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_tr2_`i' stillsmoking_tr2_`i'  anyalc_tr2_`i' bmi_tr2_`i' waist_tr2_`i' d3kcal_tr2_`i' using "Tables/noMI/msm_gluc_estimates_tr2_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_tr2_dur stillsmoking_tr2_dur  anyalc_tr2_dur bmi_tr2_dur waist_tr2_dur d3kcal_tr2_dur using "Tables/noMI/msm_gluc_estimates_tr2_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_tr2_`i'_dur stillsmoking_tr2_`i'_dur  anyalc_tr2_`i'_dur bmi_tr2_`i'_dur waist_tr2_`i'_dur d3kcal_tr2_`i'_dur using "Tables/noMI/msm_gluc_estimates_tr2_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

log close

log using Logs/msm_gluc_results_tr5.log, replace

/*STABILIZED WEIGHTS TRUNCATED AT 5 PERCENTILE*/

//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo works_tr5:  logistic works dmyet $x_prod [pw=stabweightdmyetall_tr5], cl(id)
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_tr5:  logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr5], cl(id)
margins, dydx(dmyet) 
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo `var'_tr5:  reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr5], cl(id)
}

forvalues i = 0/1{ // by sex
eststo works_tr5_`i':  logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{  //by sex and binary
eststo `var'_tr5_`i':  logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo `var'_tr5_`i':  reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}
}



/*DURATION*/

eststo works_tr5_dur:  logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr5], cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_tr5_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr5], cl(id)
margins, dydx(yearsdiagall) 
}

foreach var of varlist bmi waist d3kcal{  // continuous
eststo `var'_tr5_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr5], cl(id)
}

forvalues i = 0/1{  // by sex
eststo works_tr5_`i'_dur:  logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{  // by sex and binary
eststo `var'_tr5_`i'_dur:  logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo `var'_tr5_`i'_dur:  reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}
}


estwrite * using "savedestimates/diabetes_msm_gluc", id() replace



* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab works_tr5 stillsmoking_tr5  anyalc_tr5 bmi_tr5 waist_tr5 d3kcal_tr5 using "Tables/noMI/msm_gluc_estimates_tr5.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_tr5_`i' stillsmoking_tr5_`i'  anyalc_tr5_`i' bmi_tr5_`i' waist_tr5_`i' d3kcal_tr5_`i' using "Tables/noMI/msm_gluc_estimates_tr5_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_tr5_dur stillsmoking_tr5_dur  anyalc_tr5_dur bmi_tr5_dur waist_tr5_dur d3kcal_tr5_dur using "Tables/noMI/msm_gluc_estimates_tr5_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_tr5_`i'_dur stillsmoking_tr5_`i'_dur  anyalc_tr5_`i'_dur bmi_tr5_`i'_dur waist_tr5_`i'_dur d3kcal_tr5_`i'_dur using "Tables/noMI/msm_gluc_estimates_tr5_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

estwrite * using "savedestimates/diabetes_msm_gluc", id() replace

log close


log using Logs/msm_gluc_results_no_weights.log, replace


/*NO WEIGHTS*/


//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo works_nw:  logistic works dmyet $x_prod, cl(id)
eststo works_nw_fe:  logistic works dmyet $x_prod, cl(id)


margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_nw:  logistic  `var' dmyet $x_other, cl(id)
margins, dydx(dmyet) 
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_nw:  reg  `var' dmyet $x_other, cl(id)
}

forvalues i = 0/1{
eststo works_`i'_nw:  logistic works dmyet $x_other if female == `i', cl(id)
margins, dydx(dmyet) 

foreach var of varlist anyalc stillsmoking{
eststo `var'_`i'_nw:  logistic  `var' dmyet $x_other if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_`i'_nw:  reg  `var' dmyet $x_other if female == `i', cl(id)
}
}




/*DURATION*/

eststo works_dur_nw:  logistic works yearsdiagall $x_prod, cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo `var'_dur_nw:  logistic  `var' yearsdiagall $x_other, cl(id)
margins, dydx(yearsdiagall) 
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_dur_nw:  reg  `var' yearsdiagall $x_other, cl(id)
}

forvalues i = 0/1{
eststo works_`i'_dur_nw:  logistic works yearsdiagall $x_other if female == `i', cl(id)
margins, dydx(yearsdiagall) 

foreach var of varlist anyalc stillsmoking{
eststo `var'_`i'_dur_nw:  logistic  `var' yearsdiagall $x_other if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo `var'_`i'_dur_nw:  reg  `var' yearsdiagall $x_other if female == `i', cl(id)
}
}




log close

estwrite * using "savedestimates/diabetes_msm_gluc", id() replace


* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab works stillsmoking_nw  anyalc_nw bmi_nw waist_nw d3kcal_nw using "Tables/noMI/msm_gluc_estimates_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_`i'_nw stillsmoking_`i'_nw  anyalc_`i'_nw bmi_`i'_nw waist_`i'_nw d3kcal_`i'_nw using "Tables/noMI/msm_gluc_estimates_`i'_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_dur_nw stillsmoking_dur_nw  anyalc_dur_nw bmi_dur_nw waist_dur_nw d3kcal_dur_nw using "Tables/noMI/msm_gluc_estimates_dur_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_`i'_dur_nw stillsmoking_`i'_dur_nw  anyalc_`i'_dur_nw bmi_`i'_dur_nw waist_`i'_dur_nw d3kcal_`i'_dur_nw using "Tables/noMI/msm_gluc_estimates_`i'_dur_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}


log using Logs/msm_gluc_results_no_MSM_adjusted.log, replace

/*NO WEIGHTS BUT COVARIATE ADJUSTED*/

global x_prod "age age_sq female rural han ib(freq).province bmi index waist d3kcal stillsmoking anyalc secondary university married insurance i.wave hba1c_09"
global x_binary "age age_sq female rural han ib(freq).province bmi index waist d3kcal secondary university married works insurance i.wave hba1c_09"
global x_cont "age age_sq female rural han ib(freq).province anyalc stillsmoking secondary university married works insurance i.wave hba1c_09"

global x "age age_sq female rural han ib(freq).province bmi index waist d3kcal stillsmoking anyalc secondary university married works insurance i.wave hba1c_09" 

//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo works_adj:  logistic works dmyet $x_prod, cl(id)
margins, dydx(dmyet) 
eststo anyalc_adj:  logistic  anyalc dmyet $x_binary , cl(id)
margins, dydx(dmyet) 
eststo stillsmoking_adj:  logistic  stillsmoking dmyet $x_binary, cl(id)
margins, dydx(dmyet) 


eststo bmi_adj:  reg  bmi dmyet $x_cont d3kcal, cl(id) ro
eststo waist_adj:  reg  waist dmyet $x_cont d3kcal, cl(id) ro 
eststo d3kcal_adj:  reg  d3kcal dmyet $x_cont bmi waist, cl(id) ro


forvalues i = 0/1{
eststo works_`i'_adj:  logistic works dmyet $x_prod if female == `i', cl(id)
margins, dydx(dmyet) 

eststo anyalc_`i'_adj:  logistic  anyalc dmyet $x_binary if female == `i' , cl(id)
margins, dydx(dmyet) 
eststo stillsmoking_`i'_adj:  logistic  stillsmoking dmyet $x_binary if female == `i', cl(id)
margins, dydx(dmyet) 

eststo bmi_`i'_adj:  reg  bmi dmyet $x_cont d3kcal if female == `i', cl(id) ro
eststo waist_`i'_adj:  reg  waist dmyet $x_cont d3kcal if female == `i', cl(id) ro 
eststo d3kcal_`i'_adj:  reg  d3kcal dmyet $x_cont bmi if female == `i', cl(id) ro
}





/*DURATION*/

eststo works_dur_adj:  logistic works yearsdiagall $x_prod, cl(id)
margins, dydx(yearsdiagall) 

eststo anyalc_dur_adj:  logistic  anyalc yearsdiagall $x_binary , cl(id)
margins, dydx(yearsdiagall) 
eststo stillsmoking_dur_adj:  logistic  stillsmoking yearsdiagall $x_binary, cl(id)
margins, dydx(yearsdiagall) 


eststo bmi_dur_adj:  reg  bmi yearsdiagall $x_cont d3kcal, cl(id) ro
eststo waist_dur_adj:  reg  waist yearsdiagall $x_cont d3kcal, cl(id) ro 
eststo d3kcal_dur_adj:  reg  d3kcal yearsdiagall $x_cont bmi, cl(id) ro


forvalues i = 0/1{
eststo works_dur_`i'_adj:  logistic works yearsdiagall $x_prod if female == `i', cl(id)
margins, dydx(yearsdiagall) 

eststo anyalc_dur_`i'_adj:  logistic  anyalc yearsdiagall $x_binary if female == `i' , cl(id)
margins, dydx(yearsdiagall) 
eststo stillsmoking_dur_`i'_adj:  logistic  stillsmoking yearsdiagall $x_binary if female == `i', cl(id)
margins, dydx(yearsdiagall) 

eststo bmi_dur_`i'_adj:  reg  bmi yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro
eststo waist_dur_`i'_adj:  reg  waist yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro 
eststo d3kcal_dur_`i'_adj:  reg  d3kcal yearsdiagall $x_cont bmi waist if female == `i', cl(id) ro
}


estwrite * using "savedestimates/diabetes_msm_gluc", id() replace


* Tables covariate adjusted

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label nobaselevels ///
	 eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab works_adj stillsmoking_adj  anyalc_adj bmi_adj waist_adj d3kcal_adj using "Tables/noMI/msm_gluc_estimates_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_`i'_adj stillsmoking_`i'_adj  anyalc_`i'_adj bmi_`i'_adj waist_`i'_adj d3kcal_`i'_adj using "Tables/noMI/msm_gluc_estimates_`i'_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_dur_adj stillsmoking_dur_adj  anyalc_dur_adj bmi_dur_adj waist_dur_adj d3kcal_dur_adj using "Tables/noMI/msm_gluc_estimates_dur_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_dur_`i'_adj stillsmoking_dur_`i'_adj  anyalc_dur_`i'_adj bmi_dur_`i'_adj waist_dur_`i'_adj d3kcal_dur_`i'_adj using "Tables/noMI/msm_gluc_estimates_dur_`i'_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}



estwrite * using "savedestimates/diabetes_msm_gluc", id() replace
 log close
/*Plotting results from FE and MSM duration group models*/

estread * using "savedestimates/diabetes_msm_gluc"  // MSM results
estread * using "savedestimates/diabetes_durationgroups_fe_clog"  // FE results


*For employment
coefplot (works_1_dur_g, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Employed(odds ratios)") name(works, replace)
graph export Plots/plot_works.eps, replace

coefplot (works_0_dur_g, recast(line)) (works_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Employed (odds ratios)") name(works, replace)
graph export Plots/plot_works_01.eps, replace

*For smoking
coefplot (stillsmoking_dur_g, recast(line)) (stillsmoking_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Smokes (odds ratios)") name(stillsmoking, replace)
graph export Plots/plot_stillsmoking.eps, replace

coefplot (stillsmoking_0_dur_g, recast(line)) (stillsmoking_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Smokes (odds ratios)") name(stillsmoking, replace)
graph export Plots/plot_stillsmoking_01.eps, replace

*For alcohol
coefplot (anyalc_dur_g, recast(line)) (anyalc_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Any alcohol (odds ratios)") name(anyalc, replace)
graph export Plots/plot_anyalc.eps, replace

coefplot (anyalc_0_dur_g, recast(line)) (anyalc_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Any alcohol (odds ratios)") name(anyalc, replace)
graph export Plots/plot_anyalc_01.eps, replace


*For BMI
coefplot (bmi_dur_g, recast(line)) (bfe_g, recast(line)), keep(*yearsdiagall_g*) vertical  title(BMI) name(bmi, replace)
graph export Plots/plot_bmi.eps, replace

coefplot (b0_dur_g, recast(line)) (bfe_g0, recast(line)), keep(*yearsdiagall_g*) vertical  title(BMI) name(bmi, replace)
graph export Plots/plot_b01.eps, replace

*For waist
coefplot (waist_dur_g, recast(line)) (waist_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical  title("Waist circumference") name(waist, replace)
graph export Plots/plot_waist.eps, replace

coefplot (waist_0_dur_g, recast(line)) (waist_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical  title("Waist circumference") name(waist, replace)
graph export Plots/plot_waist_01.eps, replace

*For kcal
coefplot (d3kcal_dur_g, recast(line)) (d3kcal_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical  title("Daily calorie consumption") name(d3kcal, replace)
graph export Plots/plot_d3kcal.eps, replace

coefplot (d3kcal_0_dur_g, recast(line)) (d3kcal_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical  title("Daily calorie consumption") name(d3kcal, replace)
graph export Plots/plot_d3kcal_01.eps, replace



