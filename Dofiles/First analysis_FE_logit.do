set more off
capture log close
clear matrix
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
global homefolder ""/home/till/Phd/China Data/Data_2014""

cd $homefolder


/*NOTE FOR THESIS: PRESENT NO MARGINAL EFFECTS OF MI RESULTS AS IT IS UNCLEAR HOW TO CALCULATE THEM.
 BEST IDEA IS TO ONLY PRESENT MARGINAL EFFECTS OF NON IMPUTED DATA IN THE THESIS VERSION.*/


/*USE FE LOGIT HERE INSTEAD OF MUNDLAK DEVICE*/

****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************

use Data/data_imputed, clear  // multiple imputation dataset for duration


global x "yearsdiagall age_sq female i.wave han ib(freq).province rural married secondary university insurance index"

label var yearsdiagall "Years since diagnosis"


log using Logs/fe_duration_mi_results_clog.log, replace

eststo mi_works_fe: mi est, post cmdok esampvaryok  : xtlogit works $x, fe 
eststo mi_smoking_fe: mi est, post cmdok esampvaryok : xtlogit stillsmoking $x works, fe  
eststo mi_alc_fe: mi est, post cmdok esampvaryok : xtlogit anyalc $x works, fe  
eststo mi_bmi_fe: mi est, post cmdok esampvaryok: xtreg bmi $x works indwage, fe 
eststo mi_waist_fe: mi est, post cmdok esampvaryok: xtreg waist $x works, fe 
eststo mi_kcal_fe: mi est, post cmdok esampvaryok: xtreg d3kcal $x works, fe 
/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo mi_works_fe`i': mi est, post cmdok esampvaryok : xtlogit works $x  if female == `i', fe  
eststo mi_smoking_fe`i': mi est, post cmdok esampvaryok : xtlogit stillsmoking $x works if female == `i', fe  
eststo mi_alc_fe`i': mi est, post cmdok esampvaryok : xtlogit anyalc $x works if female == `i', fe  
}

forvalues i =0/1{
eststo mi_bmi_fe`i': mi est, post cmdok esampvaryok: xtreg bmi $x works if female == `i', fe 
eststo mi_waist_fe`i': mi est, post cmdok esampvaryok: xtreg waist $x works if female == `i', fe 
eststo mi_kcal_fe`i': mi est, post cmdok esampvaryok: xtreg d3kcal $x works if female == `i', fe 

}

estwrite mi_* using "savedestimates/diabetes_duration_mi_fe_clog", id() replace
estread mi_* using "savedestimates/diabetes_duration_mi_fe_clog"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall) label nobaselevels ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_fe mi_smoking_fe  mi_alc_fe mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_clog_duration.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_fe`i' mi_smoking_fe`i'  mi_alc_fe`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_clog_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
           
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell eform(1 1 1 0 0 0) ///
	 collabels(none) keep(*yearsdiagall) label  nobaselevels /// 
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab mi_works_fe mi_smoking_fe  mi_alc_fe mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_clog_duration.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_fe`i' mi_smoking_fe`i'  mi_alc_fe`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_clog_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close




log using Logs/fe_binary_mi_results_clog.log, replace
***************************************
/*+++++Binary diabetes indicator+++++*/
***************************************

global x "dmyet age_sq female i.wave han rural married secondary university insurance index ib(freq).province"


//eststo mi_works_fe_ia: mi est, post cmdok esampvaryok : xtlogit works $x c.c__dmyet#i.female c.m__dmyet#i.female female, fe  
mi est, post cmdok esampvaryok: xtlogit works $x, fe  
mi est, post cmdok esampvaryok: xtlogit works $x, fe  
mi est, post cmdok esampvaryok: xtreg works $x, re ro
mi est, post cmdok esampvaryok: xtreg works $x, re ro
mi est, post cmdok esampvaryok: xtreg works $x, fe ro

eststo mi_works_fe: mi est, post cmdok esampvaryok : xtlogit works $x, fe 
eststo mi_smoking_fe: mi est, post cmdok esampvaryok : xtlogit stillsmoking $x works, fe  
eststo mi_alc_fe: mi est, post cmdok esampvaryok : xtlogit anyalc $x works, fe  
eststo mi_bmi_fe: mi est, post cmdok esampvaryok: xtreg bmi $x works indwage, fe 
eststo mi_waist_fe: mi est, post cmdok esampvaryok: xtreg waist $x works, fe 
eststo mi_kcal_fe: mi est, post cmdok esampvaryok: xtreg d3kcal $x works, fe 

/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo mi_works_fe`i': mi est, post cmdok esampvaryok : xtlogit works $x if female == `i', fe  
eststo mi_smoking_fe`i': mi est, post cmdok esampvaryok : xtlogit stillsmoking $x works if female == `i', fe  
eststo mi_alc_fe`i': mi est, post cmdok esampvaryok : xtlogit anyalc $x works if female == `i', fe  
}

forvalues i =0/1{
eststo mi_bmi_fe`i': mi est, post cmdok esampvaryok: xtreg bmi $x works if female == `i', fe 
eststo mi_waist_fe`i': mi est, post cmdok esampvaryok: xtreg waist $x works if female == `i', fe 
eststo mi_kcal_fe`i': mi est, post cmdok esampvaryok: xtreg d3kcal $x works if female == `i', fe 

}
estwrite mi_* using "savedestimates/diabetes_dummy_mi_fe_clog", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_fe_clog"


/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

	*LATEX
	esttab mi_works_fe mi_smoking_fe  mi_alc_fe mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_clog.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
		${table_reduced} 

	forvalues i=0/1{
	esttab mi_works_fe`i' mi_smoking_fe`i'  mi_alc_fe`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_clog`i'.tex", append comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
	       }
	      
	      
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell eform(1 1 1 0 0 0) ///
	 collabels(none) keep(*dmyet) label nobaselevels ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab mi_works_fe mi_smoking_fe  mi_alc_fe mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_clog.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_fe`i' mi_smoking_fe`i'  mi_alc_fe`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_clog`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


estwrite mi_* using "savedestimates/diabetes_dummy_mi_fe_clog", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_fe_clog"


capture log close


***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************
global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
capture log close

use Data/data_imputed, clear

log using Logs/fe_durationgroups_mi_clog.log, append

global x "i.yearsdiagall_g age_sq female i.wave i.han ib(freq).province rural married secondary university insurance index"


eststo mi_works_fe_g: mi est, post cmdok esampvaryok : xtlogit works $x, fe  
eststo mi_smoking_fe_g: mi est, post cmdok esampvaryok : xtlogit stillsmoking $x works, fe  
eststo mi_alc_fe_g: mi est, post cmdok esampvaryok : xtlogit anyalc $x works, fe  
eststo mi_bmi_fe_g: mi est, post cmdok esampvaryok: xtreg bmi $x works indwage, fe 
eststo mi_waist_fe_g: mi est, post cmdok esampvaryok: xtreg waist $x works, fe 
eststo mi_kcal_fe_g: mi est, post cmdok esampvaryok: xtreg d3kcal $x works, fe 

/*BY GENDER*/

	* normally use loops, but here need to estimate them seperatley as for female smoking regression gives error because ommits different groups between datasets, i.e. ommits > 20 years duration in some due to lack of data. Thefore exclude this group in female estimation, but not in male
eststo mi_works_fe_g0: mi est, post cmdok esampvaryok errorok: xtlogit works $x if female == 0, fe 
eststo mi_smoking_fe_g0: mi est, post cmdok esampvaryok errorok: xtlogit stillsmoking $x  works if female == 0, fe 
eststo mi_alc_fe_g0: mi est, post cmdok esampvaryok errorok: xtlogit anyalc $x i.province works if female == 0, fe  

eststo mi_works_fe_g1: mi est, post cmdok esampvaryok errorok: xtlogit works $x if female == 1, fe 
eststo mi_smoking_fe_g1: mi est, post cmdok esampvaryok errorok: xtlogit stillsmoking $x  works if female == 1 & yearsdiagall_g < 7, fe 
eststo mi_alc_fe_g1: mi est, post cmdok esampvaryok errorok: xtlogit anyalc $x i.province works if female == 1, fe  

	*Using xtlogit and later mimargns command to compare marginal effects to xtreg results

forvalues i =0/1{
eststo mi_bmi_fe_g`i': mi est, post cmdok esampvaryok errorok: xtreg bmi $x works if female == `i', fe 
eststo mi_waist_fe_g`i': mi est, post cmdok esampvaryok errorok: xtreg waist $x works if female == `i', fe 
eststo mi_kcal_fe_g`i': mi est, post cmdok esampvaryok errorok: xtreg d3kcal $x works if female == `i', fe 

}

estwrite mi_* using "savedestimates/diabetes_durationgroups_mi_fe_clog", id() replace
estread * using "savedestimates/diabetes_durationgroups_mi_fe_clog"


/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall*) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_fe_g mi_smoking_fe_g  mi_alc_fe_g mi_bmi_fe_g mi_waist_fe_g mi_kcal_fe_g using "Tables/FE_estimates_mi_clog_duration_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_fe_g`i' mi_smoking_fe_g`i'  mi_alc_fe_g`i' mi_bmi_fe_g`i' mi_waist_fe_g`i' mi_kcal_fe_g`i' using "Tables/FE_estimates_mi_clog_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell  eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0)) ///
	 collabels(none) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab mi_works_fe_g mi_smoking_fe_g  mi_alc_fe_g mi_bmi_fe_g mi_waist_fe_g mi_kcal_fe_g using "Tables/FE_estimates_mi_clog_duration_g.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_fe_g`i' mi_smoking_fe_g`i'  mi_alc_fe_g`i' mi_bmi_fe_g`i' mi_waist_fe_g`i' mi_kcal_fe_g`i' using "Tables/FE_estimates_mi_clog_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}





capture log close
