set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data.dta", clear
***********************************
/*FE RESULTS FOR NON-IMPUTED DATA*/
***********************************
drop if wave < 1997
bysort id (wave): gen dmyet_bl= dmyet[1]

drop if dmyet_bl == 1 

 gen attrite = 0

by id (wave): replace attrite = 1 if id[_n+1] == . & age < 65 
by id (wave): replace attrite = 0 if wave == 2011 

global x "dmyet age_sq i.wave han rural married secondary university insurance index ib(freq).province hhincpc_cpi bmi waist stillsmoking alc d3kcal works"

bysort female: xtreg attrite age $x female if wave < 2011 , ro 

bysort wave: ttest attrite, by(rural)

estpost tabstat attrite, by(wave) ///
statistics(mean) columns(statistics) listwise percent

esttab using Tables/attrition.tex, replace main(mean) nostar noobs nonote nomtitle nonumber
