******************************
********LAGGED dmyet**********
******************************

set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using "Logs/stabweigths_l", replace


use "Data/data_imputed", clear


global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl  female secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l anyalc_l secondary_l university_l married_l works_l hhexpense_cpi_l" 

drop if wave== 1997
drop if dmyet_bl == 1

/*For dmyet as explanatory variable*/


global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l anyalc_l secondary_l university_l married_l works_l hhexpense_cpi_l" 

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: drop if (pdmyet ==. & wave<=firstdmidt) | (pdmyet ==.  & firstdmidt ==.)  // drop all missing predictions here. No need to do it again later. If I don't drop them here it will cause many missings in weights creation
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1  
drop pdmyet denom

save "Data/ice30_weighted_dm_l", replace
use "Data/ice30_weighted_dm_l", clear

// by gender

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l anyalc_l secondary_l university_l married_l works_l hhexpense_cpi_l" 
*	global x "age age_sq bmi index waist d3kcal smoke anyalc secondary university married works insurance" 



forvalues i=0/1{
mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav, storecompleted 
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
}

save "Data/ice30_weighted_dm_l", replace


*individual income
// by gender
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl lnindwage_bl hhexpense_cpi_bl han rural ib45.province i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l anyalc_l secondary_l university_l married_l lnindwage_l insurance_l hhexpense_cpi_l" 

forvalues i=0/1{
mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav, storecompleted 
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_wage`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
}


/*USING OBESITY AND OVERWEIGHT MEASUREMENTS BASED ON BMI INSTEAD OF BMI AND WAISTS MEASUREMENTS*/
*global bl "age_bl age_sq_bl index_bl overweight_bl obese_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl hhexpense_cpi_bl han rural i.wave ib(freq).province"
*global x "age_l age_sq_l overweight obese index_l d3kcal_l smoke_l anyalc_l secondary_l university_l married_l works_l insurance_l hhexpense_cpi_l" 


/*
mi estimate, saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: gen pdmyet1 = pdmyet
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1


mi rename pdmyet dmyetdenom

mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet $bl if wave<=firstdmidt | firstdmidt ==.
mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_obese =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom

save "Data/ice30_weighted_dm_l", replace
use "Data/ice30_weighted_dm_l", clear
*/
// by gender

global bl "age_bl age_sq_bl index_bl overweight_bl obese_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl hhexpense_cpi_bl han rural i.wave ib(freq).province"
global x "age_l age_sq_l overweight_l obese_l index_l d3kcal_l smoke_l anyalc_l secondary_l university_l married_l works_l insurance_l hhexpense_cpi_l" 

forvalues i=0/1{
mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl *_l if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav, storecompleted 
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_obese`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
}


save "Data/ice30_weighted_dm_l", replace



log close
log using Logs/china_dm30_results_l.log, replace

use "Data/ice30_weighted_dm_l", clear


/*truncate weight to 1 and 99th percentile*/

foreach var of varlist stabweightdmyetall*{
gen `var'_tr1 = `var'
forvalues i= 1/30{
_pctile `var' if _mi_m == `i', nq(100) 
replace `var'_tr1 = r(r99) if `var' > r(r99) & `var' <. & _mi_m == `i'
replace `var'_tr1 = r(r1) if `var' < r(r1) & _mi_m == `i'
	}
}


sum stabweight*

eststo stabweights: estpost sum stabweight*

*label var stabweightdmyetall "Untruncated (all)"
label var stabweightdmyetall0 "Untruncated (men)"
label var stabweightdmyetall1 "Untruncated (women)"
*label var stabweightdmyetall_tr1 "Truncated 1 and 99 percentile (all)"
label var stabweightdmyetall0_tr1 "Truncated 1 and 99 percentile (men)"
label var stabweightdmyetall1_tr1 "Truncated 1 and 99 percentile (women)"


*need to drop observations here to make sure that I calculate maximum number of weights using 







 esttab stabweights using "Tables/stabweights_l.tex" , replace cells("mean min max") nonumber mti("Mean" "Min" "Max") ///
 booktabs label 

// have created two globals with controls for regression models. First for works and wage models and the second for all other models
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl lnindinc_bl i.wave"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

global x_obese "age_bl age_sq_bl index_bl obese_bl overweight_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"


/*STABILIZED WEIGHTS UNTRUNCATED*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage [pw=stabweightdmyet_tr] if works == 1, cl(id)
/*
	eststo mi_works_l: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall], cl(id)
	eststo mi_works_l_me: mimrgns, dydx(dmyet) predict(pr) post

	foreach var of varlist overweight obese { // for binary outcomes
	eststo mi_`var'_l: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_obese], cl(id)
	eststo mi_`var'_l_me: mimrgns, dydx(dmyet) predict(pr) post
	}

	foreach var of varlist anyalc smoke{ // for binary outcomes
	eststo mi_`var'_l: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
	eststo mi_`var'_l_me: mimrgns, dydx(dmyet) predict(pr) post
	}

	foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
	eststo mi_`var'_l: mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id) 
	}
*/
	forvalues i = 0/1{ // by sex
	eststo mi_works_l_`i': mi est, post errorok or esampvaryok: logistic works dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	eststo mi_works_l_me_`i': mimrgns, dydx(dmyet) predict(pr) post

	foreach var of varlist overweight obese{  //by sex and binary
	eststo mi_`var'_l_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_obese`i'] if female == `i', cl(id)
	eststo mi_`var'_l_me_`i': mimrgns, dydx(dmyet) predict(pr) post
	}


	foreach var of varlist anyalc smoke{  //by sex and binary
	eststo mi_`var'_l_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other  [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	eststo mi_`var'_l_me_`i': mimrgns, dydx(dmyet) predict(pr) post
	}

	foreach var of varlist bmi waist d3kcal{  // by sex and continuous
	eststo mi_`var'_l_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	}
	}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() replace

//LINEAR DURATION
/*
eststo mi_works_l_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall], cl(id) 
eststo mi_works_l_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese [pw=stabweightdmyetall_obese], cl(id)
eststo mi_`var'_l_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id)
eststo mi_`var'_l_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_dur: mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id) 
}
*/

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl lnindinc_bl i.wave"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

global x_obese "age_bl age_sq_bl index_bl obese_bl overweight_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

forvalues i = 0/1{ // by sex
eststo mi_works_l_dur_`i': mi est, post errorok or  esampvaryok: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_works_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese{  //by sex and binary
eststo mi_`var'_l_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese  [pw=stabweightdmyetall_obese`i'] if female == `i', cl(id)
eststo mi_`var'_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}


foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_`var'_l_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_dur_`i': mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append

//DURATION GROUPS
/*
eststo mi_works_l_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall], cl(id) 
eststo mi_works_l_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese] , cl(id)
eststo mi_`var'_l_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
eststo mi_`var'_l_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_dur_g: mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id) 
}


*/

// due to too little ovservations in some groups, have to restrict number of years since diagnosis to before 18 years
forvalues i = 1/1{ // by sex
eststo mi_works_l_dur_g_`i': mi est, post errorok or  esampvaryok: logistic works i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
eststo mi_works_l_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

/*
foreach var of varlist  obese overweight{  //by sex and binary
eststo mi_`var'_l_dur_g_`i': mi est, post errorok or noisily esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_obese`i'] if female == `i' & yearsdiagall_g <8, cl(id)
eststo mi_`var'_l_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}*/
}
forvalues i = 0/1{ // by sex

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i']  if female == `i', cl(id)
}
}

foreach var of varlist smoke anyalc{
eststo mi_`var'_l_dur_g_0: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall0] if female == 0, cl(id)
eststo mi_`var'_l_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

coefplot (mi_works_l_dur_g_me_0,label(men) lpattern(solid)) (mi_works_l_dur_g_1, label(women) lpattern(shortdash)) , bylabel("Employed")   || mi_smoke_l_dur_g_me_0 , bylabel("Smoking")|| ///
mi_anyalc_l_dur_g_me_0 , bylabel("Alcohol") || ///
mi_bmi_l_dur_g_0 mi_bmi_l_dur_g_1, bylabel("BMI") || ///
mi_waist_l_dur_g_0 mi_waist_l_dur_g_1,  bylabel("Waist (cm)")  || ///
mi_d3kcal_l_dur_g_0 mi_d3kcal_l_dur_g_1, bylabel("Kcal") ||, ///
keep(1.yearsdiagall_g 2.yearsdiagall_g 3.yearsdiagall_g 4.yearsdiagall_g 5.yearsdiagall_g 6.yearsdiagall_g 7.yearsdiagall_g 8.yearsdiagall_g 9.yearsdiagall_g ) yline(0) recast(line)  byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(msm, replace) ylabel(,  angle(horizontal) nogrid) xsize(8)  xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") xtitle("Years after diagnosis")

graph export mi_msm_l_all.eps, replace 



* Tables

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/
/*esttab mi_works_l mi_smoke_l  mi_anyalc_l mi_bmi_l mi_waist_l mi_d3kcal_l using "Tables/msm_estimates_mi_l.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_`i' mi_smoke_l_`i'  mi_anyalc_l_`i' mi_bmi_l_`i' mi_waist_l_`i' mi_d3kcal_l_`i' using "Tables/msm_estimates_mi_l_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_me mi_smoke_l_me  mi_anyalc_l_me mi_bmi_l mi_waist_l mi_d3kcal_l using "Tables/msm_estimates_mi_l_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_me_`i' mi_smoke_l_me_`i'  mi_anyalc_l_me_`i' mi_bmi_l_`i' mi_waist_l_`i' mi_d3kcal_l_`i' using "Tables/msm_estimates_mi_l_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}



  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
/*esttab mi_works_l_dur mi_smoke_l_dur  mi_anyalc_l_dur mi_bmi_l_dur mi_waist_l_dur mi_d3kcal_l_dur using "Tables/msm_estimates_mi_l_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_dur_`i' mi_smoke_l_dur_`i'  mi_anyalc_l_dur_`i' mi_bmi_l_dur_`i' mi_waist_l_dur_`i' mi_d3kcal_l_dur_`i' using "Tables/msm_estimates_mi_l_dur_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_dur_me mi_smoke_l_dur_me  mi_anyalc_l_dur_me mi_bmi_l_dur mi_waist_l_dur mi_d3kcal_l_dur using "Tables/msm_estimates_mi_l_dur_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_dur_me_`i' mi_smoke_l_dur_me_`i'  mi_anyalc_l_dur_me_`i' mi_bmi_l_dur_`i' mi_waist_l_dur_`i' mi_d3kcal_l_dur_`i' using "Tables/msm_estimates_mi_l_dur_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}




*Duration groups outcomes

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)    ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/
/*esttab mi_works_l_dur_g mi_smoke_l_dur_g  mi_anyalc_l_dur_g mi_bmi_l_dur_g mi_waist_l_dur_g mi_d3kcal_l_dur_g using "Tables/msm_estimates_mi_l_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

esttab mi_works_l_dur_g_0 mi_smoke_l_dur_g_0  mi_anyalc_l_dur_g_0 mi_bmi_l_dur_g_0 mi_waist_l_dur_g_0 mi_d3kcal_l_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_dur_g_1  mi_bmi_l_dur_g_1 mi_waist_l_dur_g_1 mi_d3kcal_l_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_dur_g_me mi_smoke_l_dur_g_me  mi_anyalc_l_dur_g_me mi_bmi_l_dur_g mi_waist_l_dur_g mi_d3kcal_l_dur_g using "Tables/msm_estimates_mi_l_dur_g_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	

esttab mi_works_l_dur_g_me_0 mi_smoke_l_dur_g_me_0  mi_anyalc_l_dur_g_me_0 mi_bmi_l_dur_g_0 mi_waist_l_dur_g_0 mi_d3kcal_l_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_me_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_dur_g_me_1  mi_bmi_l_dur_g_1 mi_waist_l_dur_g_1 mi_d3kcal_l_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_me_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)


log close



 log using Logs/china_dm30_results_l_truncated.log, replace

/*STABILIZED WEIGHTS TRUNCATED*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage [pw=stabweightdmyet_tr] if works == 1, cl(id)
/*
eststo mi_works_l_tr: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall_tr], cl(id) 
eststo mi_works_l_tr_me: mimrgns, dydx(dmyet) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_tr: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_tr_obese], cl(id)
eststo mi_`var'_l_tr_me: mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_tr: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr], cl(id)
eststo mi_`var'_l_tr_me: mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_tr: mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr], cl(id) 
}
*/
forvalues i = 0/1{ // by sex
eststo mi_works_l_tr_`i': mi est, post errorok or esampvaryok: logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_works_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post

foreach var of varlist overweight obese{  //by sex and binary
eststo mi_`var'_l_tr_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i', cl(id)
eststo mi_`var'_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}


foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_tr_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_`var'_l_tr_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_tr_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_tr", id() replace

//LINEAR DURATION
/*
eststo mi_works_l_tr_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr], cl(id) 
eststo mi_works_l_tr_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_tr_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese [pw=stabweightdmyetall_tr_obese], cl(id)
eststo mi_`var'_l_tr_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_tr_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr], cl(id)
eststo mi_`var'_l_tr_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_tr_dur: mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr], cl(id) 
}
*/
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl lnindinc_bl i.wave"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

global x_obese "age_bl age_sq_bl index_bl obese_bl overweight_bl d3kcal_bl smoke_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib(freq).province hhexpense_cpi_bl i.wave"

forvalues i = 0/1{ // by sex
eststo mi_works_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_works_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese{  //by sex and binary
eststo mi_`var'_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese [pw=stabweightdmyetall_obese`i'_tr1] if female == `i', cl(id)
eststo mi_`var'_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}


foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_tr_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_`var'_l_tr_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_tr_dur_`i': mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_tr", id() append

//DURATION GROUPS
/*
eststo mi_works_l_tr_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall_tr], cl(id) 
eststo mi_works_l_tr_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_tr_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese [pw=stabweightdmyetall_tr_obese], cl(id)
eststo mi_`var'_l_tr_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_tr_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall_tr], cl(id)
eststo mi_`var'_l_tr_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_tr_dur_g: mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall_tr], cl(id) 
}
*/



// due to too little ovservations in some groups, have to restrict number of years since diagnosis to before 18 years
forvalues i = 0/1{ // by sex
eststo mi_works_l_tr_dur_g_`i': mi est, post errorok or esampvaryok: logistic works i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
eststo mi_works_l_tr_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

/*
foreach var of varlist  obese overweight{  //by sex and binary
eststo mi_`var'_l_tr_dur_g_`i': mi est, post errorok or noisily esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese [pw=ostabweightdmyetall_obese`i'_tr1] if female == `i' & yearsdiagall_g <8, cl(id)
eststo mi_`var'_l_tr_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}*/
}
forvalues i = 0/1{ // by sex


foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_tr_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}
foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_tr_dur_g_0: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall0_tr1] if female == 0, cl(id)
eststo mi_`var'_l_tr_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}





* Tables

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/
/*esttab mi_works_l_tr mi_smoke_l_tr  mi_anyalc_l_tr mi_bmi_l_tr mi_waist_l_tr mi_d3kcal_l_tr using "Tables/msm_estimates_mi_l_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_tr_`i' mi_smoke_l_tr_`i'  mi_anyalc_l_tr_`i' mi_bmi_l_tr_`i' mi_waist_l_tr_`i' mi_d3kcal_l_tr_`i' using "Tables/msm_estimates_mi_l_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_tr_me mi_smoke_l_tr_me  mi_anyalc_l_tr_me mi_bmi_l_tr mi_waist_l_tr mi_d3kcal_l_tr using "Tables/msm_estimates_mi_l_me_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_tr_me_`i' mi_smoke_l_tr_me_`i'  mi_anyalc_l_tr_me_`i' mi_bmi_l_tr_`i' mi_waist_l_tr_`i' mi_d3kcal_l_tr_`i' using "Tables/msm_estimates_mi_l_me_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}



  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
/*esttab mi_works_l_tr_dur mi_smoke_l_tr_dur  mi_anyalc_l_tr_dur mi_bmi_l_tr_dur mi_waist_l_tr_dur mi_d3kcal_l_tr_dur using "Tables/msm_estimates_mi_l_dur_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

forvalues i=0/1{
esttab mi_works_l_tr_dur_`i' mi_smoke_l_tr_dur_`i'  mi_anyalc_l_tr_dur_`i' mi_bmi_l_tr_dur_`i' mi_waist_l_tr_dur_`i' mi_d3kcal_l_tr_dur_`i' using "Tables/msm_estimates_mi_l_dur_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

/*esttab mi_works_l_tr_dur_me mi_smoke_l_tr_dur_me  mi_anyalc_l_tr_dur_me mi_bmi_l_tr_dur mi_waist_l_tr_dur mi_d3kcal_l_tr_dur using "Tables/msm_estimates_mi_l_dur_me_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */
	
forvalues i=0/1{
esttab mi_works_l_tr_dur_me_`i' mi_smoke_l_tr_dur_me_`i'  mi_anyalc_l_tr_dur_me_`i' mi_bmi_l_tr_dur_`i' mi_waist_l_tr_dur_`i' mi_d3kcal_l_tr_dur_`i' using "Tables/msm_estimates_mi_l_dur_me_`i'_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}




*Duration groups outcomes

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)    ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/
/*esttab mi_works_l_tr_dur_g mi_smoke_l_tr_dur_g  mi_anyalc_l_tr_dur_g mi_bmi_l_tr_dur_g mi_waist_l_tr_dur_g mi_d3kcal_l_tr_dur_g using "Tables/msm_estimates_mi_l_dur_g_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */

esttab mi_works_l_tr_dur_g_0 mi_smoke_l_tr_dur_g_0  mi_anyalc_l_tr_dur_g_0 mi_bmi_l_tr_dur_g_0 mi_waist_l_tr_dur_g_0 mi_d3kcal_l_tr_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_0_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_tr_dur_g_1  mi_bmi_l_tr_dur_g_1 mi_waist_l_tr_dur_g_1 mi_d3kcal_l_tr_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_1_tr.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)


/*MARGINAL EFFECTS*/

/*esttab mi_works_l_tr_dur_g_me mi_smoke_l_tr_dur_g_me  mi_anyalc_l_tr_dur_g_me mi_bmi_l_tr_dur_g mi_waist_l_tr_dur_g mi_d3kcal_l_tr_dur_g using "Tables/msm_estimates_mi_l_dur_g_me_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} */



esttab mi_works_l_tr_dur_g_me_0 mi_smoke_l_tr_dur_g_me_0  mi_anyalc_l_tr_dur_g_me_0 mi_bmi_l_tr_dur_g_0 mi_waist_l_tr_dur_g_0 mi_d3kcal_l_tr_dur_g_0 using "Tables/msm_estimates_mi_l_dur_g_me_0_tr.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_tr_dur_g_me_1  mi_bmi_l_tr_dur_g_1 mi_waist_l_tr_dur_g_1 mi_d3kcal_l_tr_dur_g_1 using "Tables/msm_estimates_mi_l_dur_g_me_1_tr.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)


log close

log using Logs/china_dm30_results_l_covariates.log, replace


/*ONLY COVARIATE ADJUSTED*/


global x_prod "age age_sq female rural han i.province index bmi waist d3kcal smoke anyalc secondary university married  insurance hhexpense_cpi i.wave"
global x_other "age age_sq female rural han i.province index bmi waist d3kcal smoke anyalc secondary university married  insurance works hhexpense_cpi i.wave"

global x_obese "age age_sq female rural han i.province index overweight obese d3kcal smoke anyalc secondary university married hhexpense_cpi  insurance works i.wave"


//mi estimate: regress lnindwage dmyet $x_prod lnindwage [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works_l_cov: mi est, post errorok or: logistic works dmyet $x_prod , cl(id) 
eststo mi_works_l_cov_me: mimrgns, dydx(dmyet) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_cov: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese , cl(id)
eststo mi_`var'_l_cov_me: mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_cov: mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other , cl(id)
eststo mi_`var'_l_cov_me: mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_cov: mi est, post errorok esampvaryok: reg  `var' dmyet $x_other , cl(id) 
}

forvalues i = 0/1{ // by sex
eststo mi_works_l_cov_`i': mi est, post errorok or esampvaryok: logistic works dmyet $x_other  if female == `i', cl(id)
eststo mi_works_l_cov_me_`i': mimrgns, dydx(dmyet) predict(pr) post

foreach var of varlist overweight obese{  //by sex and binary
eststo mi_`var'_l_cov_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_obese  if female == `i', cl(id)
eststo mi_`var'_l_cov_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}


foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_cov_`i': mi est, post errorok or esampvaryok: logistic  `var' dmyet $x_other  if female == `i', cl(id)
eststo mi_`var'_l_cov_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_cov_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other  if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_cov", id() replace

//LINEAR DURATION

eststo mi_works_l_cov_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod , cl(id) 
eststo mi_works_l_cov_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_cov_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese , cl(id)
eststo mi_`var'_l_cov_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_cov_dur: mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other , cl(id)
eststo mi_`var'_l_cov_dur_me: mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_cov_dur: mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other , cl(id) 
}

global x_prod "age age_sq female rural han i.province index bmi waist d3kcal smoke anyalc secondary university married  insurance hhexpense_cpi i.wave"
global x_other "age age_sq female rural han i.province index bmi waist d3kcal smoke anyalc secondary university married  insurance works hhexpense_cpi i.wave"

global x_obese "age age_sq female rural han i.province index overweight obese d3kcal smoke anyalc secondary university married  insurance works hhexpense_cpi i.wave"

forvalues i = 0/1{ // by sex
eststo mi_works_l_cov_dur_`i': mi est, post errorok or esampvaryok: logistic works yearsdiagall $x_other  if female == `i', cl(id)
eststo mi_works_l_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post

foreach var of varlist overweight obese{  //by sex and binary
eststo mi_`var'_l_cov_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_obese  if female == `i', cl(id)
eststo mi_`var'_l_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}


foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_cov_dur_`i': mi est, post errorok or esampvaryok: logistic  `var' yearsdiagall $x_other  if female == `i', cl(id)
eststo mi_`var'_l_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_cov_dur_`i': mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other  if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l_cov", id() append

//DURATION GROUPS

eststo mi_works_l_cov_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod , cl(id) 
eststo mi_works_l_cov_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

foreach var of varlist overweight obese { // for binary outcomes
eststo mi_`var'_l_cov_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese , cl(id)
eststo mi_`var'_l_cov_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist anyalc smoke{ // for binary outcomes
eststo mi_`var'_l_cov_dur_g: mi est, post errorok or esampvaryok: logistic  `var' i.yearsdiagall_g $x_other , cl(id)
eststo mi_`var'_l_cov_dur_g_me: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_l_cov_dur_g: mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other , cl(id) 
}




// due to too little ovservations in some groups, have to restrict number of years since diagnosis to before 18 years
forvalues i = 0/1{ // by sex
eststo mi_works_l_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic works i.yearsdiagall_g $x_other  if female == `i', cl(id)
eststo mi_works_l_cov_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post


foreach var of varlist  obese overweight{  //by sex and binary
eststo mi_`var'_l_cov_dur_g_`i': mi est, post errorok or noisily esampvaryok: logistic  `var' i.yearsdiagall_g $x_obese  if female == `i', cl(id)
eststo mi_`var'_l_cov_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}
}
forvalues i = 0/1{ // by sex
foreach var of varlist anyalc smoke{  //by sex and binary
eststo mi_`var'_l_cov_dur_g_`i': mi est, post errorok or noisily esampvaryok: logistic  `var' i.yearsdiagall_g $x_other  if female == `i', cl(id)
eststo mi_`var'_l_cov_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_l_cov_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_other  if female == `i', cl(id)
}
}



* Tables

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/
esttab mi_works_l_cov mi_smoke_l_cov  mi_anyalc_l_cov mi_bmi_l_cov mi_waist_l_cov mi_d3kcal_l_cov using "Tables/msm_estimates_mi_l_cov.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_l_cov_`i' mi_smoke_l_cov_`i'  mi_anyalc_l_cov_`i' mi_bmi_l_cov_`i' mi_waist_l_cov_`i' mi_d3kcal_l_cov_`i' using "Tables/msm_estimates_mi_l_cov_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

esttab mi_works_l_cov_me mi_smoke_l_cov_me  mi_anyalc_l_cov_me mi_bmi_l_cov mi_waist_l_cov mi_d3kcal_l_cov using "Tables/msm_estimates_mi_l_cov_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 
	
forvalues i=0/1{
esttab mi_works_l_cov_me_`i' mi_smoke_l_cov_me_`i'  mi_anyalc_l_cov_me_`i' mi_bmi_l_cov_`i' mi_waist_l_cov_`i' mi_d3kcal_l_cov_`i' using "Tables/msm_estimates_mi_l_cov_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}



  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
esttab mi_works_l_cov_dur mi_smoke_l_cov_dur  mi_anyalc_l_cov_dur mi_bmi_l_cov_dur mi_waist_l_cov_dur mi_d3kcal_l_cov_dur using "Tables/msm_estimates_mi_l_cov_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_l_cov_dur_`i' mi_smoke_l_cov_dur_`i'  mi_anyalc_l_cov_dur_`i' mi_bmi_l_cov_dur_`i' mi_waist_l_cov_dur_`i' mi_d3kcal_l_cov_dur_`i' using "Tables/msm_estimates_mi_l_cov_dur_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

esttab mi_works_l_cov_dur_me mi_smoke_l_cov_dur_me  mi_anyalc_l_cov_dur_me mi_bmi_l_cov_dur mi_waist_l_cov_dur mi_d3kcal_l_cov_dur using "Tables/msm_estimates_mi_l_cov_dur_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 
	
forvalues i=0/1{
esttab mi_works_l_cov_dur_me_`i' mi_smoke_l_cov_dur_me_`i'  mi_anyalc_l_cov_dur_me_`i' mi_bmi_l_cov_dur_`i' mi_waist_l_cov_dur_`i' mi_d3kcal_l_cov_dur_`i' using "Tables/msm_estimates_mi_l_cov_dur_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}




*Duration groups outcomes

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)    ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/
esttab mi_works_l_cov_dur_g mi_smoke_l_cov_dur_g  mi_anyalc_l_cov_dur_g mi_bmi_l_cov_dur_g mi_waist_l_cov_dur_g mi_d3kcal_l_cov_dur_g using "Tables/msm_estimates_mi_l_cov_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_l_cov_dur_g_`i' mi_smoke_l_cov_dur_g_`i'  mi_anyalc_l_cov_dur_g_`i' mi_bmi_l_cov_dur_g_`i' mi_waist_l_cov_dur_g_`i' mi_d3kcal_l_cov_dur_g_`i' using "Tables/msm_estimates_mi_l_cov_dur_g_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

esttab mi_works_l_cov_dur_g_me mi_smoke_l_cov_dur_g_me  mi_anyalc_l_cov_dur_g_me mi_bmi_l_cov_dur_g mi_waist_l_cov_dur_g mi_d3kcal_l_cov_dur_g using "Tables/msm_estimates_mi_l_cov_dur_g_me.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 
	
forvalues i=0/1{
esttab mi_works_l_cov_dur_g_me_`i' mi_smoke_l_cov_dur_g_me_`i'  mi_anyalc_l_cov_dur_g_me_`i' mi_bmi_l_cov_dur_g_`i' mi_waist_l_cov_dur_g_`i' mi_d3kcal_l_cov_dur_g_`i' using "Tables/msm_estimates_mi_l_cov_dur_g_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}


esttab mi_works_l_cov_dur_g_me_0 mi_smoke_l_cov_dur_g_me_0  mi_anyalc_l_cov_dur_g_me_0 mi_bmi_l_cov_dur_g_0 mi_waist_l_cov_dur_g_0 mi_d3kcal_l_cov_dur_g_0 using "Tables/msm_estimates_mi_l_cov_dur_g_me_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_l_cov_dur_g_me_1  mi_bmi_l_cov_dur_g_1 mi_waist_l_cov_dur_g_1 mi_d3kcal_l_cov_dur_g_1 using "Tables/msm_estimates_mi_l_cov_dur_g_me_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)


log close
