******************************
********LAGGED dmyet**********
******************************

set more off
capture log close
clear matrix


*global homefolder ""/home/till/Phd/China Data/Data_2014_2""
global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using "Logs/stabweigths_l_190219.log", replace


*use "Data/data_imputed2", clear
use "Data/data_imputed30", clear

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural female ib45.province hhincpc_cpi_bl hypertension_bl hypert_bio_bl MET_PA_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l university_l married_l insurance_l works_l MET_PA_l hypertension_l hypert_bio_l hhincpc_cpi_l" 
mi estimate, esampvaryok saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if  (wave<=firstdmidt | firstdmidt ==.) & dmyet_bl == 0
mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted

mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: gen pdmyet_miss = 1 if (pdmyet ==. & wave<=firstdmidt) | (pdmyet ==.  & firstdmidt ==.) 
drop pdmyet denom

// by gender

*	global x "age age_sq bmi index waist d3kcal smoke alc secondary university married works insurance" 

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl hypertension_bl hypert_bio_bl MET_PA_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l university_l married_l insurance_l works_l MET_PA_l hypertension_l hypert_bio_l hhincpc_cpi_l" 


forvalues i=0/1{
eststo mi_predictors`i': mi estimate, saving(dmyetden_sav, replace) esample(denom) esampvaryok: logistic dmyet $bl $x if  dmyet_bl == 0 & (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav,  esample(denom) storecompleted 
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  &  pdmyet[_n-1] <.
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom): logistic dmyet $bl if dmyet_bl == 0 & (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  &  pdmyet[_n-1] <.
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l married_l insurance_l works_l hhincpc_cpi_l MET_PA_l hypertension_l hypert_bio_l" 

}




//Table for predictors
global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar  ///
	 alignment(D{.}{.}{-1}l)    collabels(none) label /// coeflables(1.diabetes Diabetes) 
	
esttab mi_predictors0 mi_predictors1 using "Tables/predictors.tex", replace comp ///
 mti("Males" "Females")    ///
        ${table_reduced} 

//accounting for censoring, i.e. the last time of people in the sample
mi xeq: egen temp=max(wave) if dmyet <., by(id) // last wave of individual
mi xeq: gen cens=1 if wave == temp & wave < 2011 // censoring = 1 if wave is last wave but not 2011, which is last wave for everyone
mi xeq: replace cens=0 if wave < temp | wave == 2011 // censoring = 0 if wave is not last wave
drop temp

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x "age age_sq bmi index waist d3kcal smoke alc secondary married insurance works hhincpc_cpi MET_PA hypertension hypert_bio" 


forvalues i=0/1{
mi estimate, saving(censden_sav, replace) esample(denom) esampvaryok: logistic cens $bl $x dmyet if female == `i'
mi predict pcens using censden_sav,  esample(denom) storecompleted 
qui mi xeq: replace pcens=invlogit(pcens)
qui mi xeq: replace pcens=1-pcens if female == `i' 
mi xeq: replace pcens=1 if wave == 2011 & female == `i'
qui mi xeq: by id (wave),sort: replace pcens=pcens*pcens[_n-1] if  female == `i' & _n!=1 & pcens[_n-1] <.
mi rename pcens censdenom

mi estimate, saving(censnom_sav, replace) esampvaryok esample(nom): logistic cens dmyet $bl if female == `i'
mi predict pcens using censnom_sav,  esample(nom) storecompleted 
qui mi xeq: replace pcens=invlogit(pcens)
qui mi xeq: replace pcens=1-pcens if female == `i'
mi xeq: replace pcens=1 if wave == 2011 & female == `i'
qui mi xeq: by id (wave),sort: replace pcens=pcens*pcens[_n-1] if  female == `i' & _n!=1 & pcens[_n-1] <.
rename pcens censnum
mi xeq: gen censweight`i' = censnum/censdenom
drop censdenom censnum denom nom
/* Calculate overall weight: */
mi xeq: gen stabweightcens`i' = stabweightdmyetall`i' * censweight`i' 
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x "age_l age_sq_l bmi_l index_l waist_l d3kcal_l smoke_l alc_l secondary_l married_l insurance_l works_l hhincpc_cpi_l MET_PA_l hypertension_l hypert_bio_l" 

}


estwrite mi_predictors* using "savedestimates/diabetes_mi_msm_predictors_cens", id() replace

save "Data/ice30_weighted_dm_l", replace

log close

log using "Logs/mi_msm.log", replace
use "Data/ice30_weighted_dm_l", clear



/*truncate weight to 1 and 99th percentile*/

foreach var of varlist stabweightdmyetall*{
gen `var'_tr1 = `var'
forvalues i= 1/30{
_pctile `var' if _mi_m == `i', nq(100) 
replace `var'_tr1 = r(r99) if `var' > r(r99) & `var' <. & _mi_m == `i'
replace `var'_tr1 = r(r1) if `var' < r(r1) & _mi_m == `i'
	}
}


misum stabweight*

eststo stabweights: estpost sum stabweight*

*label var stabweightdmyetall "Untruncated (all)"
label var stabweightdmyetall0 "Untruncated (men)"
label var stabweightdmyetall1 "Untruncated (women)"
*label var stabweightdmyetall_tr1 "Truncated 1 and 99 percentile (all)"
label var stabweightdmyetall0_tr1 "Truncated 1 and 99 percentile (men)"
label var stabweightdmyetall1_tr1 "Truncated 1 and 99 percentile (women)"


 esttab stabweights using "Tables/stabweightscens_l.tex" , replace cells("mean min max") nonumber mti("Mean" "Min" "Max") ///
 booktabs label 

// have created two globals with controls for regression models. First for works and wage models and the second for all other models

// have created two globals with controls for regression models. First for works and wage models and the second for all other models
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl lnindinc_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x_smoke "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"


drop if retired == 1

/*STABILIZED WEIGHTS UNTRUNCATED*/

	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA{  // by sex and continuous
	eststo mi_`var'_l_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
}

	forvalues i = 0/1{ // by sex
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{  // by sex and continuous
	eststo mi_`var'_l_`i': mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

	}

}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append
estread mi_* using "savedestimates/diabetes_mi_msm_l", id() 



//LINEAR DURATION


global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"

global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"

forvalues i = 0/1{ // by sex
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{  // by sex and continuous
	eststo mi_`var'_l_dur_`i': mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append

//DURATION GROUPS


forvalues i = 0/1{ // by sex
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{  // by sex and continuous
	eststo mi_`var'_l_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g_alt $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
	}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append
estread mi_* using "savedestimates/diabetes_mi_msm_l", id()

coefplot (mi_works_l_dur_g_0,label(men) lpattern(solid)) (mi_works_l_dur_g_1, label(women) lpattern(shortdash)) , bylabel("Employed")   || mi_smoke_l_dur_g_0 mi_smoke_l_dur_g_1 , bylabel("Smoking")|| ///
mi_alc_l_dur_g_0 mi_alc_l_dur_g_1, bylabel("Alcohol") || ///
mi_bmi_l_dur_g_0 mi_bmi_l_dur_g_1, bylabel("BMI") || ///
mi_waist_l_dur_g_0 mi_waist_l_dur_g_1,  bylabel("Waist (cm)")  || ///
mi_d3kcal_l_dur_g_0 mi_d3kcal_l_dur_g_1, bylabel("Kcal") || ///
mi_MET_PA_l_dur_g_0 mi_MET_PA_l_dur_g_1, bylabel("MET PA") || ///
mi_hypert_bio_l_dur_g_0 mi_hypert_bio_l_dur_g_1, bylabel("Hypertension") ||, ///
keep(*.yearsdiagall_g_alt ) yline(0) recast(line)  byopts(yrescale cols(4))  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(msm, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_msm_l_all.eps, replace 




* Tables

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar  ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(dmyet) label /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX


forvalues i=0/1{
esttab mi_works_l_`i' mi_smoke_l_`i'  mi_alc_l_`i' mi_bmi_l_`i' mi_waist_l_`i' mi_d3kcal_l_`i' mi_MET_PA_l_`i' mi_hypert_bio_l_`i' using "Tables/msm_estimates_mi_l_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} 
}




  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar  ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes

forvalues i=0/1{
esttab mi_works_l_dur_`i' mi_smoke_l_dur_`i'  mi_alc_l_dur_`i' mi_bmi_l_dur_`i' mi_waist_l_dur_`i' mi_d3kcal_l_dur_`i' mi_MET_PA_l_dur_`i' mi_hypert_bio_l_dur_`i' using "Tables/msm_estimates_mi_l_dur_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} fragment
}





*Duration groups outcomes

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar    ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(*.yearsdiagall_g_alt) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


forvalues i=0/1{
esttab mi_works_l_dur_g_`i' mi_smoke_l_dur_g_`i'  mi_alc_l_dur_g_`i' mi_bmi_l_dur_g_`i' mi_waist_l_dur_g_`i' mi_d3kcal_l_dur_g_`i' mi_MET_PA_l_dur_g_`i' mi_hypert_bio_l_dur_g_`i'  using "Tables/msm_estimates_mi_l_dur_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} fragment
}






log close


*********Using truncated weights************

log using "Logs/mi_msm_tr.log",  replace
// have created two globals with controls for regression models. First for works and wage models and the second for all other models
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"

global x_wage "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl lnindinc_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"
global x_smoke "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"



/*STABILIZED WEIGHTS UNTRUNCATED*/


	forvalues i = 0/1{ // by sex
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{  // by sex and continuous
	eststo mi_`var'_l_`i'_tr1: mi est, post errorok esampvaryok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)

	}

}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append
estread mi_* using "savedestimates/diabetes_mi_msm_l", id() 



//LINEAR DURATION


global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"

global x_other "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl han rural ib45.province hhincpc_cpi_bl MET_PA_bl hypertension_bl hypert_bio_bl i.wave"

forvalues i = 0/1{ // by sex
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{  // by sex and continuous
	eststo mi_`var'_l_dur_`i'_tr1: mi est, post errorok esampvaryok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
	}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm_l", id() append

//DURATION GROUPS


forvalues i = 0/1{ // by sex
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{  // by sex and continuous
	eststo `var'_l_dur_g`i'_tr1: mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g_alt $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
	}
}


estwrite *_l_dur_g* using "savedestimates/diabetes_mi_msm_l", id() append
estread * using "savedestimates/diabetes_mi_msm_l", id()

coefplot (works_l_dur_g0_tr1,label(men) lpattern(solid)) (works_l_dur_g1_tr1, label(women) lpattern(shortdash)) , bylabel("Employed")   || smoke_l_dur_g0_tr1 smoke_l_dur_g1_tr1 , bylabel("Smoking")|| ///
alc_l_dur_g0_tr1 alc_l_dur_g1_tr1, bylabel("Alcohol") || ///
bmi_l_dur_g0_tr1  bmi_l_dur_g1_tr1, bylabel("BMI") || ///
waist_l_dur_g0_tr1 waist_l_dur_g1_tr1,  bylabel("Waist (cm)")  || ///
d3kcal_l_dur_g0_tr1 d3kcal_l_dur_g1_tr1, bylabel("Kcal") || ///
MET_PA_l_dur_g0_tr1 MET_PA_l_dur_g1_tr1, bylabel("MET PA") || ///
hypert_bio_l_dur_g0_tr1 hypert_bio_l_dur_g1_tr1, bylabel("Hypertension") ||, ///
keep(*.yearsdiagall_g_alt ) yline(0) recast(line)  byopts(yrescale cols(4))  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(msm_tr1, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_msm_l_all_tr1.eps, replace 




* Tables

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar  ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(dmyet) label /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX


forvalues i=0/1{
esttab mi_works_l_`i'_tr1 mi_smoke_l_`i'_tr1  mi_alc_l_`i'_tr1 mi_bmi_l_`i'_tr1 mi_waist_l_`i'_tr1 mi_d3kcal_l_`i'_tr1 mi_MET_PA_l_`i'_tr1 mi_hypert_bio_l_`i'_tr1 using "Tables/msm_estimates_mi_l_`i'_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} 
}




  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar  ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes

forvalues i=0/1{
esttab mi_works_l_dur_`i'_tr1 mi_smoke_l_dur_`i'_tr1  mi_alc_l_dur_`i'_tr1 mi_bmi_l_dur_`i'_tr1 mi_waist_l_dur_`i'_tr1 mi_d3kcal_l_dur_`i'_tr1 mi_MET_PA_l_dur_`i'_tr1 mi_hypert_bio_l_dur_`i'_tr1 using "Tables/msm_estimates_mi_l_dur_`i'_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} fragment
}





*Duration groups outcomes

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar    ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(*.yearsdiagall_g_alt) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


forvalues i=0/1{
esttab works_l_dur_g`i'_tr1 smoke_l_dur_g`i'_tr1  alc_l_dur_g`i'_tr1 bmi_l_dur_g`i'_tr1 waist_l_dur_g`i'_tr1 d3kcal_l_dur_g`i'_tr1 MET_PA_l_dur_g`i'_tr1 hypert_bio_l_dur_g`i'_tr1  using "Tables/msm_estimates_mi_l_dur_g`i'_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "PA" "Hypertension") ${table_reduced} fragment
}









log close
/*
log using Logs/china_dm30_results_l_covariates.log, replace


/*ONLY COVARIATE ADJUSTED*/

use "Data/data_imputed30", clear



drop if dmyet_bl == 1


global x_prod "age age_sq female rural han ib45.province index d3kcal smoke alc secondary university married insurance hhincpc_cpi i.wave"
global x_binary "age age_sq female rural han ib45.province bmi index waist d3kcal secondary university married works insurance hhincpc_cpi i.wave"
global x_obese "age age_sq female rural han ib45.province index d3kcal secondary university married works insurance hhincpc_cpi i.wave"

global x_cont "age age_sq female rural han ib45.province alc smoke secondary university married works insurance hhincpc_cpi i.wave"

global x "age age_sq female rural han ib45.province bmi index waist d3kcal smoke alc secondary university married works insurance hhincpc_cpi i.wave" 


forvalues i = 0/1{
eststo mi_works_cov_`i': mi est, post errorok or: logistic works dmyet $x_prod if female == `i', cl(id)
eststo mi_works_cov_me_`i': mimrgns, dydx(dmyet) predict(pr) post
}

forvalues i = 0/1{
eststo mi_alc_cov_`i': mi est, post errorok or: logistic  alc dmyet $x_binary smoke if female == `i' , cl(id)
eststo mi_alc_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_smoke_cov_`i': mi est, post errorok or: logistic  smoke dmyet $x_binary alc if female == `i', cl(id)
eststo mi_smoke_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
}

forvalues i = 0/1{
eststo mi_obese_cov_`i': mi est, post errorok or: logistic  obese dmyet $x_obese alc smoke if female == `i' , cl(id)
eststo mi_obese_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_overweight_cov_`i': mi est, post errorok or: logistic  overweight dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_overweight_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_obesec_`i'_cov_`i': mi est, post errorok or: logistic  obesec_`i' dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_obesec_`i'_cov_me_`i':mimrgns, dydx(dmyet) predict(pr) post
}
forvalues i = 0/1{
eststo mi_bmi_cov_`i': mi est, post errorok: reg  bmi dmyet $x_cont d3kcal if female == `i', cl(id) ro
eststo mi_waist_cov_`i': mi est, post errorok: reg  waist dmyet $x_cont d3kcal if female == `i', cl(id) ro 
eststo mi_d3kcal_cov_`i': mi est, post errorok: reg  d3kcal dmyet $x_cont if female == `i', cl(id) ro
}


/*DURATION*/



forvalues i = 0/1{
eststo mi_works_cov_dur_`i': mi est, post errorok or: logistic works yearsdiagall $x_prod if female == `i', cl(id)
eststo mi_works_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
}
forvalues i = 0/1{
eststo mi_alc_cov_dur_`i': mi est, post errorok or: logistic  alc yearsdiagall $x_binary smoke if female == `i' , cl(id)
eststo mi_alc_cov_dur_me_`i': mimrgns, dydx(yearsdiagall) predict(pr) post
eststo mi_smoke_cov_dur_`i': mi est, post errorok or: logistic  smoke yearsdiagall $x_binary alc if female == `i', cl(id)
eststo mi_smoke_cov_dur_me_`i':mimrgns, dydx(yearsdiagall) predict(pr)
 }
 forvalues i = 0/1{
eststo mi_obese_cov_dur_`i': mi est, post errorok or: logistic  obese dmyet $x_obese alc smoke if female == `i' , cl(id)
eststo mi_obese_cov_dur_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_overweight_cov_dur_`i': mi est, post errorok or: logistic  overweight dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_overweight_cov_dur_me_`i':mimrgns, dydx(dmyet) predict(pr) post
eststo mi_obesec_`i'_cov_dur_`i': mi est, post errorok or: logistic  obesec_`i' dmyet $x_obese alc smoke if female == `i', cl(id)
eststo mi_obesec_`i'_cov_dur_me_`i':mimrgns, dydx(dmyet) predict(pr) post
}

forvalues i = 0/1{
eststo mi_bmi_cov_dur_`i': mi est, post errorok: reg  bmi yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro
eststo mi_waist_cov_dur_`i': mi est, post errorok: reg  waist yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro 
eststo mi_d3kcal_cov_dur_`i': mi est, post errorok: reg  d3kcal yearsdiagall $x_cont if female == `i', cl(id) ro
}



/*DURATION GROUPS*/

forvalues i = 0/1{
eststo mi_works_cov_dur_g_`i': mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod if female == `i', cl(id)
eststo mi_works_cov_dur_g_me_`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post

}

// due to too little ovservations in some groups, have to restrict number of yearD{.}{.}{-1}ince diagnosis to before 18 years
forvalues i = 0/1{
eststo mi_obese_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic  obese i.yearsdiagall_g $x_obese if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_obese_cov_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_overw_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic  overweight i.yearsdiagall_g $x_obese  if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_overw_cov_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_obesec_`i'_cov_dur_g_`i': mi est, post errorok or esampvaryok: logistic  obesec_`i' i.yearsdiagall_g $x_obese if female == `i' & yearsdiagall_g <5, cl(id)
eststo mi_obesec_`i'_cov_dur_g_me`i': mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}


foreach var of varlist smoke alc{
eststo mi_smoke_cov_dur_g_0: mi est, post errorok or esampvaryok: logistic  smoke i.yearsdiagall_g $x_binary alc  if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_smoke_cov_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
eststo mi_alc_cov_dur_g_0: mi est, post errorok or esampvaryok: logistic  alc i.yearsdiagall_g $x_binary smoke if female == 0 & yearsdiagall_g < 8, cl(id)
eststo mi_alc_cov_dur_g_me_0: mimrgns, dydx(i.yearsdiagall_g) predict(pr) post
}

forvalues i = 0/1{ // by sex
foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_cov_dur_g_`i': mi est, post errorok esampvaryok: reg  `var' i.yearsdiagall_g $x_cont  if female == `i' & yearsdiagall_g <8, cl(id)
}
}



estwrite mi_*cov* using "savedestimates/diabetes_mi_msm_l_cov", id() replace


* Tables

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar  ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX

/*ODDS RATIOS*/

forvalues i=0/1{
esttab mi_works_cov_`i' mi_smoke_cov_`i'  mi_alc_cov_`i' mi_bmi_cov_`i' mi_waist_cov_`i' mi_d3kcal_cov_`i' using "Tables/msm_estimates_mi_cov_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

	
forvalues i=0/1{
esttab mi_works_cov_me_`i' mi_smoke_cov_me_`i'  mi_alc_cov_me_`i' mi_bmi_cov_`i' mi_waist_cov_`i' mi_d3kcal_cov_`i' using "Tables/msm_estimates_mi_cov_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}


forvalues i=0/1{
esttab mi_overweight_cov_me_`i' mi_obese_cov_me_`i' mi_obesec_`i'_cov_me_`i' using "Tables/msm_estimates_mi_cov_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar   ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*Duration outcomes
/*ODDS RATIOS*/
forvalues i=0/1{
esttab mi_works_cov_dur_`i' mi_smoke_cov_dur_`i'  mi_alc_cov_dur_`i' mi_bmi_cov_dur_`i' mi_waist_cov_dur_`i' mi_d3kcal_cov_dur_`i' using "Tables/msm_estimates_mi_cov_dur_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/

	
forvalues i=0/1{
esttab mi_works_cov_dur_me_0 mi_smoke_cov_dur_me_`i'  mi_alc_cov_dur_me_`i' mi_bmi_cov_dur_`i' mi_waist_cov_dur_`i' mi_d3kcal_cov_dur_`i' using "Tables/msm_estimates_mi_cov_dur_me_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
}

forvalues i=0/1{
esttab mi_overweight_cov_dur_me_`i' mi_obese_cov_dur_me_`i' mi_obesec_`i'_cov_dur_me_`i' using "Tables/msm_estimates_mi_cov_dur_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}



*Duration groups outcomes

  global table_reduced cells(b( fmt(%9.3f)) ci( fmt(%9.3f) par("(" "," ")")) )    booktabs nostar    ///
	 alignment(D{.}{.}{-1})   collabels(none) keep(*.yearsdiagall_g) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

/*ODDS RATIOS*/

forvalues i=0/1{
esttab mi_works_cov_dur_g_`i' mi_smoke_cov_dur_g_`i'  mi_alc_cov_dur_g_`i' mi_bmi_cov_dur_g_`i' mi_waist_cov_dur_g_`i' mi_d3kcal_cov_dur_g_`i' using "Tables/msm_estimates_mi_cov_dur_g_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

/*MARGINAL EFFECTS*/


esttab mi_works_cov_dur_g_me_0 mi_smoke_cov_dur_g_me_0  mi_alc_cov_dur_g_me_0 mi_bmi_cov_dur_g_0 mi_waist_cov_dur_g_0 mi_d3kcal_cov_dur_g_0 using "Tables/msm_estimates_mi_cov_dur_g_me_0.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}

esttab mi_works_cov_dur_g_me_1  mi_bmi_cov_dur_g_1 mi_waist_cov_dur_g_1 mi_d3kcal_cov_dur_g_1 using "Tables/msm_estimates_mi_cov_dur_g_me_1.tex", replace comp ///
 mti("Employment"  "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} extracols(2 2)

forvalues i=0/1{
esttab mi_overw_cov_dur_g_me`i' mi_obese_cov_dur_g_me`i' mi_obesec_`i'_cov_dur_g_me`i' using "Tables/msm_estimates_mi_cov_dur_g_obese`i'.tex", replace comp ///
 mti("Overweight" "Obese" "Central obesity") ${table_reduced} 
}


log close
