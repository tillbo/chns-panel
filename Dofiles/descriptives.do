*******************************
********DESCRIPTIVES***********
*******************************

set more off
capture log close
clear matrix

global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder

use "Data/data_imputed30", clear  // multiple imputation dataset for duration

drop if dmyet_bl == 1
drop if maxseq == 1
global table "/home/till/Phd/China Data/Data_2014/Tables/" // directory to save tables to



global y "works smoke alc d3kcal bmi waist"
global controls_nf "age han rural married secondary university insurance index dmyet yearsdiagall hhincpc_cpi i.wave i.province female"

mi estimate, esample(esample): xtreg $y $controls_nf, fe

bysort female dmyet: misum works if esample == 1

/*Descriptives by sex and diabetes status*/
*eststo malettest: estpost ttest ${nofactors} if sex==0 & works < . & wealth < . & indig < ., by(diabetes)
*eststo femttest: estpost ttest ${nofactors} if sex==1 & works < . & wealth < . & indig < ., by(diabetes)
	forvalues sex=0/1{
	foreach var of varlist $y $controls_nf {
	eststo ttest_`var'`sex': mi estimate, post: reg `var' dmyet if female==`sex' & esample==1
	}
	}


	forvalues i=0/1{
	forvalues sex=0/1{
	eststo descriptives`sex'`i': mi estimate, post: mean $y $controls_nf if female==`sex' & dmyet == `i' & esample==1
	}
	}



label var works "Employed"
label var smoke "Smokes"
label var alc "Any alcohol consumption"
label var d3kcal "Kcal (3-day average)"
label var bmi "BMI"
label var waist "Waist circ. (cm)"
label var age "Age"
label var han "Han ethnicity"
label var rural "Rural area"
label var married "Married"
label var secondary "Secondary educ."
label var university "University educ."
label var insurance "Any health insurance"
label var yearsdiagall "Years since diabetes diagnosis"
label var hhincpc_cpi "Per capita household income"
estwrite * using "savedestimates/descriptives", id() replace

*esttab descriptives0 descriptives1 using "$table/descriptives.tex" , replace cells(b(fmt(2))) nonumber mti("Males" "Females") ///
* booktabs collabels(none) label
 
 esttab descriptives00 descriptives01 ttest_works0 descriptives10 descriptives11 ttest_works1 using "$table/descriptives_diab.tex" , replace alignment(SS) cells(b(fmt(2))) nonumber mti("No diabetes" "Diabetes" "Sign. difference" "No diabetes" "Diabetes" "Sign. difference") ///
 booktabs collabels(none) label mgroups("Males" "Females", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))



foreach var of varlist $y $controls_nf {
esttab ttest_`var'0 ttest_`var'1 using "$table/ttest.tex" , append cells(p(fmt(3))) nonumber mti("Males" "Females") ///
 booktabs collabels(none) label fragment stats(depvar)
 }
 
 
 
 /* proportion of missing data that was imputed */
 
 
 
 mdesc works smoke alc d3kcal bmi waist age han rural married secondary university insurance index dmyet yearsdiagall hhincpc_cpi female if _mi_m == 0
 
 /* numer of complete cases. drop all variables not used in models and run mi describe
