set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

	use "Data/ice3_weighted_dm_l", clear

mi extract 0 

xtset id wave
***********************************
/*FE RESULTS FOR NON-IMPUTED DATA*/
***********************************

// graph showing retirement over age groups by gender and urban rural
	twoway lpolyci retired age if female == 1 & rural == 0, fcolor(none) alpattern(dash_dot)  || lpolyci retired age if female == 1  & rural == 1  || lpolyci retired age if female == 0  & rural == 0  || lpolyci retired age if female == 0  & rural == 1, ///
	name(works, replace) ylabel(0(0.2)1, ang(hor) nogrid) xlabel(#15) bw(0.75) xtitle(age) ytitle(Retirment) title(Retirement) ///
	 xline(50 55 60, lstyle(grid))  lpattern(dash) alpattern(shortdash) fcolor(none) 
	twoway lpolyci works age if female == 1 & rural == 0, fcolor(none) alpattern(dash_dot)  || lpolyci works age if female == 1  & rural == 1  || lpolyci works age if female == 0  & rural == 0  || lpolyci works age if female == 0  & rural == 1, ///
	name(works, replace) ylabel(0(0.2)1, ang(hor) nogrid) xlabel(#15) bw(0.75) xtitle(age) ytitle(Retirment) title(Retirement) ///
	 xline(50 55 60, lstyle(grid))  lpattern(dash) alpattern(shortdash) fcolor(none) 
 



log using Logs/fe_no_mi_binary_results_xtreg.log, replace
***************************************
/*+++++Binary diabetes indicator+++++*/
***************************************


/*BY GENDER*/


	global x "dmyet age_sq female i.wave han rural married secondary university insurance index ib45.province hhincpc_cpi"


	//eststo mi_works_fe_ia: mi est, post cmdok esampvaryok : xtreg works $x c.c__dmyet#i.female c.m__dmyet#i.female female, fe ro  

	/*BY GENDER*/


	forvalues i =0/1{
	display `i'
	foreach var of varlist works smoke alc bmi waist d3kcal MET_PA hypertension hypert_bio{
	eststo `var'_fe`i': xtreg `var' $x if female == `i' & stabweightdmyetall`i' <., fe ro  
	}
	}
	

estwrite * using "savedestimates/diabetes_dummy_fe_xtreg", id() replace
estread * using "savedestimates/diabetes_dummy_fe_xtreg"


/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

	*LATEX
	esttab works_fe smoking_fe  alc_fe bmi_fe waist_fe kcal_fe using "Tables/noMI/FE_estimates_xtreg.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
		${table_reduced} 

	forvalues i=0/1{
	esttab works_fe`i' smoking_fe`i'  alc_fe`i' bmi_fe`i' waist_fe`i' kcal_fe`i' using "Tables/noMI/FE_estimates_xtreg`i'.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
	       }
	      
	      
*Word
  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	  collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

esttab works_fe smoking_fe  alc_fe bmi_fe waist_fe kcal_fe using "Tables/noMI/FE_estimates_xtreg.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_fe`i' smoking_fe`i'  alc_fe`i' bmi_fe`i' waist_fe`i' kcal_fe`i' using "Tables/noMI/FE_estimates_xtreg`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


estwrite * using "savedestimates/diabetes_dummy_fe_xtreg", id() replace
estread * using "savedestimates/diabetes_dummy_fe_xtreg"


capture log close

****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************

global x "yearsdiagall age_sq female i.wave han ib(freq).province rural married secondary university insurance retired index hhincpc_cpi"

label var yearsdiagall "Years since diagnosis"


log using Logs/fe_duration_results_xtreg.log, replace

eststo works_fe_dur:  xtreg works $x if retired == 0, fe ro
eststo smoking_fe_dur:  xtreg stillsmoking $x works, fe ro 
eststo alc_fe_dur:  xtreg alc $x works, fe ro 
eststo bmi_fe_dur:  xtreg bmi $x works , fe ro
eststo waist_fe_dur:  xtreg waist $x works, fe ro
eststo kcal_fe_dur:  xtreg d3kcal $x works, fe ro
eststo PA_fe_dur:  xtreg MET_leisure $x works, fe ro

/*BY GENDER*/


forvalues i =0/1{
display `i'
eststo works_fe_dur`i':  xtreg works $x  if female == `i' & retired == 0, fe ro 
eststo smoking_fe_dur`i':  xtreg stillsmoking $x works if female == `i', fe ro 
eststo alc_fe_dur`i':  xtreg alc $x works if female == `i', fe ro 
}

forvalues i =0/1{
eststo bmi_fe_dur`i':  xtreg bmi $x works if female == `i', fe ro
eststo waist_fe_dur`i':  xtreg waist $x works if female == `i', fe ro
eststo kcal_fe_dur`i':  xtreg d3kcal $x works if female == `i', fe ro

}

estwrite * using "savedestimates/diabetes_duration_fe_dur_xtreg", id() replace
estread * using "savedestimates/diabetes_duration_fe_dur_xtreg"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_fe_dur smoking_fe_dur  alc_fe_dur bmi_fe_dur waist_fe_dur kcal_fe_dur using "Tables/noMI/FE_estimates_xtreg_duration.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_fe_dur`i' smoking_fe_dur`i'  alc_fe_dur`i' bmi_fe_dur`i' waist_fe_dur`i' kcal_fe_dur`i' using "Tables/noMI/FE_estimates_xtreg_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
           
*Word
  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01)  ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab works_fe_dur smoking_fe_dur  alc_fe_dur bmi_fe_dur waist_fe_dur kcal_fe_dur using "Tables/noMI/FE_estimates_xtreg_duration.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_fe_dur`i' smoking_fe_dur`i'  alc_fe_dur`i' bmi_fe_dur`i' waist_fe_dur`i' kcal_fe_dur`i' using "Tables/noMI/FE_estimates_xtreg_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close






***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************

drop yearsdiagall_*

replace yearsdiagall = 0 if dmyet == 0

gen yearsdiagall_r = round(yearsdiagall,1)
recode yearsdiagall_r (0/1 = 1) ///
			(2/3 = 2) ///
			(4/5 = 3) ///
			(6/7 = 4) ///
			(8/9 = 5) ///
			(10/11 = 6) ///
			(12/13 = 7) ///
			(14/15 = 8) ///
			(16/20 = 9) ///
			(21/100 = 10) (-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g)
			
replace yearsdiagall_g = 0 if dmyet == 0			
label define yearsdiagall_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-10" 7 "11-12" 8 "13-14" 9 "15-19" 10 "20+", replace
label value yearsdiagall_g yearsdiagall_g
label var yearsdiagall_g "Years since diagnosis in groups"
dummies yearsdiagall_g 


log using Logs/fe_durationgroups_xtreg.log, append

global x "i.yearsdiagall_g age_sq female i.wave i.han ib(freq).province rural married secondary university insurance index hhincpc_cpi"


eststo works_fe_g:  xtreg works $x, fe ro 
eststo smoking_fe_g:  xtreg stillsmoking $x works, fe ro 
eststo alc_fe_g:  xtreg alc $x works, fe ro 
eststo bmi_fe_g:  xtreg bmi $x works , fe ro
eststo waist_fe_g:  xtreg waist $x works, fe ro
eststo kcal_fe_g:  xtreg d3kcal $x works, fe ro

/*BY GENDER*/

	* normally use loops, but here need to estimate them seperatley as for female smoking regression gives error because ommits different groups between datasets, i.e. ommits > 20 years duration in some due to lack of data. Thefore exclude this group in female estimation, but not in male
eststo works_fe_g0:  xtreg works $x if female == 0, fe ro
eststo smoking_fe_g0:  xtreg stillsmoking $x  works if female == 0, fe ro
eststo alc_fe_g0:  xtreg alc $x i.province works if female == 0, fe ro 

eststo works_fe_g1:  xtreg works $x if female == 1, fe ro
eststo smoking_fe_g1:  xtreg stillsmoking $x  works if female == 1, fe ro
eststo alc_fe_g1:  xtreg alc $x i.province works if female == 1, fe ro 


forvalues i =0/1{
eststo bmi_fe_g`i':  xtreg bmi $x works if female == `i', fe ro
eststo waist_fe_g`i':  xtreg waist $x works if female == `i', fe ro
eststo kcal_fe_g`i':  xtreg d3kcal $x works if female == `i', fe ro
}

estwrite * using "savedestimates/diabetes_durationgroups_fe_xtreg", id() replace
estread * using "savedestimates/diabetes_durationgroups_fe_xtreg"

*PLOTTING ALL DURATION GROUP RESULTS*
coefplot works_fe_g0 works_fe_g1 , bylabel("Employed")   || smoking_fe_g0 smoking_fe_g1, bylabel("Smoking")|| ///
 alc_fe_g0 alc_fe_g1, bylabel("Alcohol") || ///
(bmi_fe_g0, label(men) lpattern(solid)) (bmi_fe_g1, label(women) lpattern(shortdash)) , bylabel("BMI") || ///
waist_fe_g0 waist_fe_g1,  bylabel("Waist (cm)") || ///
kcal_fe_g0 kcal_fe_g1, bylabel("Kcal") ||, ///
keep(1.yearsdiagall_g 2.yearsdiagall_g 3.yearsdiagall_g 4.yearsdiagall_g 5.yearsdiagall_g 6.yearsdiagall_g 7.yearsdiagall_g 8.yearsdiagall_g 9.yearsdiagall_g ) yline(0) recast(line)  byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(continuous_xtreg, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono)

graph export fe.eps, replace 

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) ///
	 alignment(S S)   collabels(none) keep(*.yearsdiagall_g) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab works_fe_g smoking_fe_g  alc_fe_g bmi_fe_g waist_fe_g kcal_fe_g using "Tables/noMI/FE_estimates_xtreg_duration_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_fe_g`i' smoking_fe_g`i'  alc_fe_g`i' bmi_fe_g`i' waist_fe_g`i' kcal_fe_g`i' using "Tables/noMI/FE_estimates_xtreg_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
*Word
  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) ///
	 collabels(none) keep(*.yearsdiagall_g) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab works_fe_g smoking_fe_g  alc_fe_g bmi_fe_g waist_fe_g kcal_fe_g using "Tables/noMI/FE_estimates_xtreg_duration_g.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab works_fe_g`i' smoking_fe_g`i'  alc_fe_g`i' bmi_fe_g`i' waist_fe_g`i' kcal_fe_g`i' using "Tables/noMI/FE_estimates_xtreg_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}





capture log close
