*Do file to prepare imputed dataset for use with MSM and FE do-files

set more off
capture log close
clear matrix
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
global homefolder ""/home/till/Phd/China Data/Data_2014""

cd $homefolder
use Data/ice3_long_duration_nodmyetmiss, clear  // multiple imputation dataset for duration
*use Data/ice30_long_duration_nodmyetmiss, clear  // multiple imputation dataset for duration
log using "Logs/mi_data_preparation", replace

mi import ice, automatic  // importing ice dataset to mi

drop miss*

drop _I* // drop missing indicators created by ice
mi xtset id wave
*mi convert wide, clear  // converting to wide whihc makes stata work faster for data manipulation



mi xeq: gen obese = 1 if bmi >= 28.00 & bmi <.
mi xeq: replace obese = 0 if bmi < 28.00
mi xeq: gen overweight = 0 if bmi < 24.00
mi xeq: replace overweight = 1 if bmi >= 24.00 & bmi < 28.00
mi xeq: replace overweight = 0 if bmi >=28.00 & bmi <.

mi xeq: gen obesec_0 = 0 if waist <.
mi xeq: replace obesec_0 = 1 if waist >= 85.00 & waist <. & female == 0
label var obesec_0 "central obesity for men"
mi xeq: gen obesec_1 = 0 if waist <.
mi xeq: replace obesec_1 = 1 if waist >= 80.00 & waist <. & female == 1
 mi update

/*Blood pressure cut-offs*/
mi xeq: gen high_systolic = 0 if systolic < 140 
mi xeq: replace high_systolic = 1 if systolic >= 140 & systolic <.
label var high_systolic "High systolic blood pressure (\geq 140)"


mi xeq: gen high_diastolic = 0 if diastolic < 90 
mi xeq: replace high_diastolic = 1 if diastolic >= 90 & diastolic <.

label var high_diastolic "High diastolic blood pressure (\geq 90)"


mi xeq: bysort id (wave): egen seq=seq()  // creates number of waves that people have went through
mi xeq: bysort id (wave): egen maxseq=max(seq)  // indicates the maximum number of waves an individual attended. I need to drop all those with only one wave because for those the marginal structural model will not work as they have no prior information

drop if maxseq==1
replace works =. if works == 2

dummies province
dummies wave
* drop individuals if in only one wave

mi xeq: gen age_sq = age^2

mi xeq: bysort id (wave): gen age_sq_bl = age_sq[1]
 



foreach var of varlist obese overweight obesec_*{ // creates baseline variables not yet created
mi xeq: bysort id (wave): gen `var'_bl= `var'[1] 
}

mi rename stillsmoking smoke
mi rename stillsmoking_bl smoke_bl
mi xeq: gen lnindwage = log(indwage)
mi xeq: by id (wave), sort: gen lnindwage_bl=lnindwage[1]
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl smoke_bl alc_bl secondary_bl university_bl married_bl insurance_bl works_bl  female rural han indwage_bl hhincpc_cpi_bl i.wave diastolic_bl hypertension_bl systolic_bl"
global x "age age_sq bmi index waist d3kcal smoke alc secondary university married works insurance indwage lnindwage hhincpc_cpi diastolic hypertension systolic" 

* creating lagged variables
foreach var of varlist $x obese overweight obesec_*{
mi xeq: bysort id (wave): gen `var'_l = `var'[_n-1]
}



* below will give me first wave where person receives diabetes diagnosis.
sort id _mi_m wave
mi xeq: egen temp=min(wave) if dmyet == 1, by(id)
mi xeq: egen firstdmidt=mean(temp), by(id)
drop temp

sort id wave _mi_m

label var dmyet "Self-reported diabetes"
label var yearsdiagall "Diabetes duration"

*Generate duration groups

mi xeq: replace yearsdiagall = 0 if dmyet == 0
mi xeq: replace yearsdiagall = round(yearsdiagall,1)
mi xeq: gen yearsdiagall_r = round(yearsdiagall,1)
mi xeq: recode yearsdiagall (0/1 = 1) ///
			(2/3 = 2) ///
			(4/5 = 3) ///
			(6/7 = 4) ///
			(8/9 = 5) ///
			(10/11 = 6) ///
			(12/13 = 7) ///
			(14/15 = 8) ///
			(16/20 = 9) ///
			(21/100 = 10) (-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g)
			
mi xeq: replace yearsdiagall_g = 0 if dmyet == 0			
label define yearsdiagall_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-10" 7 "11-12" 8 "13-14" 9 "15-19" 10 "20+", replace
label value yearsdiagall_g yearsdiagall_g
label var yearsdiagall_g "Years since diagnosis in groups"


*Generate alternative duration groups

mi xeq: recode yearsdiagall (0/2 = 1) ///
			(3/5 = 2) ///
			(6/8 = 3) ///
			(9/11 = 4) ///
			(12/16 = 5) ///
			(17/100 = 6) ///
			(-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g_alt)
			
mi xeq: replace yearsdiagall_g_alt = 0 if dmyet == 0			
label define yearsdiagall_g_alt 0 "No" 1 "0-1" 2 "2-4" 3 "5-7" 4 "8-10" 5 "11-15" 6 "16+", replace
label value yearsdiagall_g_alt yearsdiagall_g_alt
label var yearsdiagall_g_alt "Alternative years since diagnosis in groups"


save Data/data_imputed3.dta, replace
*save Data/data_imputed30.dta, replace

