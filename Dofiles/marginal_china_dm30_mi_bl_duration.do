set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using Logs/china_dm30_bl_duration.log, replace

//use "ice30_cluster_miss.dta", clear
use "Data/ice30_long_nodmyetmiss_imported.dta", clear
// drop lagged variables used for better imputation in ice but not needed here. Only keep first which is equal to baseline values

drop _I* // drop missing indicators created by ice


mi xtset id wave
mi convert wide, clear  // converting to wide whihc makes stata work faster for data manipulation

* drop individuals if in only one wave
mi xeq: bysort id (wave): egen seq=seq()  // creates number of waves that people have went through
mi xeq: bysort id (wave): egen maxseq=max(seq)  // indicates the maximum number of waves an individual attended. I need to drop all those with only one wave because for those the marginal structural model will not work as they have no prior information

drop if maxseq==1
*drop if dmyet_bl == 1

gen age_sq = age^2

mi xeq: bysort id (wave): gen age_sq_bl = age_sq[1]


* below will give me first wave where person receives diabetes diagnosis.
sort id _mi_m wave
mi xeq: egen temp=min(wave) if dmyet == 1, by(id)
mi xeq: egen firstdmidt=mean(temp), by(id)
drop temp

sort id wave _mi_m



mi xeq: gen lnindwage = log(indwage)
mi xeq: by id (wave), sort: gen lnindwage_bl=lnindwage[1]

label var dmyet "Self-reported diabetes"
label var yearsdiagall "Diabetes duration"

*Generate duration groups

replace yearsdiagall = 0 if dmyet == 0

mi xeq: gen yearsdiagall_r = round(yearsdiagall,1)
mi xeq: recode yearsdiagall_r (0/1 = 1) ///
			(2/3 = 2) ///
			(4/5 = 3) ///
			(6/7 = 4) ///
			(8/9 = 5) ///
			(10/11 = 6) ///
			(12/13 = 7) ///
			(14/15 = 8) ///
			(16/20 = 9) ///
			(21/100 = 10) (-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g)
			
mi xeq: replace yearsdiagall_g = 0 if dmyet == 0			
label define yearsdiagall_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-10" 7 "11-12" 8 "13-14" 9 "15-19" 10 "20+", replace
label value yearsdiagall_g yearsdiagall_g
label var yearsdiagall_g "Years since diagnosis in groups"

/*CREATING STABILIZED WEIGHTS*/
mi convert flong, clear
save "Data/ice30_weighted_bl", replace

use "Data/ice30_weighted_bl", clear

global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl i.wave han rural female ib(freq).province  i.wave"
global x "age age_sq bmi index waist d3kcal stillsmoking anyalc secondary university married works insurance" 
/*For dmyet as explanatory variable*/
eststo predictors: mi estimate, post or saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet $bl if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom




save "Data/ice30_weighted_bl_dm", replace
use "Data/ice30_weighted_bl_dm", clear

// by gender
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl works_bl i.wave han rural ib(freq).province  i.wave"
global x "age age_sq bmi index waist d3kcal stillsmoking anyalc secondary university married works insurance" 

eststo predictors0: mi estimate, post or: logistic dmyet $bl $x if (female == 0 & wave<=firstdmidt) | (female == 0 & firstdmidt ==.)
eststo predictors1: mi estimate, post or: logistic dmyet $bl $x if (female == 1 & wave<=firstdmidt) | (female == 1 & firstdmidt ==.)


forvalues i=0/1{
eststo predictors`i': mi estimate, post or saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet age_bl age_sq_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom

}

save "Data/ice30_weighted_bl_dm", replace

estwrite predictor* using "savedestimates/diabetes_mi_msm", id() append

estread	predictor* using "savedestimates/diabetes_mi_msm"

*Table for predictors*
  global table_reduced b(%9.3f) ci(%9.3f) wide booktabs nolz nostar noobs  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) eform label nobaselevels ///

*LATEX
esttab predictors predictors0 predictors1 using "Tables/predictors.tex", replace comp ///
 mti("Complete sample" "Male" "Female")    ///
        ${table_reduced} 

**********************
/* Weights for wages
because wages uses different 
sample than employment due 
to zero wages, which are 
missing using log wages
*/
**********************
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl insurance_bl i.wave han rural female ib(freq).province  i.wave works_bl indwage_bl"
global x "age age_sq bmi index waist d3kcal stillsmoking anyalc secondary university married insurance works indwage" 
/*For dmyet as explanatory variable*/
eststo predictors: mi estimate, post or errorok esampvaryok saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
mi rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet $bl if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: bysort id (wave): replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_wage =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom




save "Data/ice30_weighted_bl_dm", replace
use "Data/ice30_weighted_bl_dm", clear

// by gender
global bl0 "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl married_bl secondary_bl university_bl insurance_bl i.wave han rural ib45.province  i.wave lnindwage_bl"
global x0 "age age_sq bmi index waist d3kcal stillsmoking anyalc married secondary university insurance lnindwage" 


* need to drop married and smoking for female model because they are dropped for some of the mi datasets and prevent vaclualtion of weights. They don't play a role anyway
global bl "age_bl age_sq_bl index_bl bmi_bl waist_bl d3kcal_bl anyalc_bl secondary_bl university_bl insurance_bl i.wave han rural ib45.province  i.wave lnindwage_bl"
global x "age age_sq bmi index waist d3kcal anyalc secondary university insurance lnindwage" 


forvalues i=0/1{
eststo predictors`i': mi estimate, post esampvaryok or noisily saving(dmyetden_sav, replace) esample(denom): logistic dmyet $bl $x if (female == `i' & wave<=firstdmidt) | (female == `i' & firstdmidt ==.)
mi predict pdmyet using dmyetden_sav, storecompleted esample(denom)
mi xeq: replace pdmyet= . if denom ==0
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if female == `i' & wave>firstdmidt
mi xeq: replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1  
mi rename pdmyet dmyetdenom

mi estimate, saving(dmyetnom_sav, replace) esampvaryok esample(nom) noisily: logistic dmyet $bl if (female == 0 & wave<=firstdmidt) | (female == 0 & firstdmidt ==.)
mi predict pdmyet using dmyetnom_sav, storecompleted
mi xeq: replace pdmyet= . if nom ==0
mi xeq: replace pdmyet=invlogit(pdmyet)
mi xeq: replace pdmyet=1 if  female == `i' & wave>firstdmidt 
mi xeq: replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: by id (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if  female == `i' & _n!=1   
mi rename pdmyet dmyetnum
mi xeq: gen stabweightdmyetall_wage`i' =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
}
log close
log using Logs/china_dm30_bl_results_tr.log, replace

use "Data/ice30_weighted_bl_dm", clear


/*truncate weight to 1 and 99th percentile*/

foreach var of varlist stabweight*{
gen `var'_tr1 = `var'
gen `var'_tr2 = `var'
gen `var'_tr5 = `var'
forvalues i= 1/30{
_pctile `var' if _mi_m == `i', nq(100) 
replace `var'_tr1 = r(r99) if `var' > r(r99) & `var' <. & _mi_m == `i'
replace `var'_tr1 = r(r1) if `var' < r(r1) & _mi_m == `i'
replace `var'_tr2 = r(r98) if `var' > r(r98) & `var' <.  & _mi_m == `i'
replace `var'_tr2 = r(r2) if `var' < r(r2) & _mi_m == `i'
replace `var'_tr5 = r(r95) if `var' > r(r95) & `var' <.  & _mi_m == `i'
replace `var'_tr5 = r(r5) if `var' < r(r5) & _mi_m == `i'
	}
}


misum stabweight*

eststo stabweights: estpost sum stabweight*

label var stabweightdmyetall "Untruncated (all)"
label var stabweightdmyetall0 "Untruncated (men)"
label var stabweightdmyetall1 "Untruncated (women)"
label var stabweightdmyetall_tr1 "Truncated 1 and 99 percentile (all)"
label var stabweightdmyetall0_tr1 "Truncated 1 and 99 percentile (men)"
label var stabweightdmyetall1_tr1 "Truncated 1 and 99 percentile (women)"
label var stabweightdmyetall_tr2 "Truncated 2 and 98 percentile (all)"
label var stabweightdmyetall0_tr2 "Truncated 2 and 98 percentile (men)"
label var stabweightdmyetall1_tr2 "Truncated 2 and 98 percentile (women)"
label var stabweightdmyetall_tr5 "Truncated 5 and 95 percentile (all)"
label var stabweightdmyetall0_tr5 "Truncated 5 and 95 percentile (men)"
label var stabweightdmyetall1_tr5 "Truncated 5 and 95 percentile (women)"



 esttab stabweights using "Tables/stabweights.tex" , replace cells("mean min max") nonumber mti("Mean" "Min" "Max") ///
 booktabs label 

// have created two globals with controls for regression models. First for works and wage models and the second for all other models

global x_prod "age_bl age_sq_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl indwage_bl i.wave"
global x_other "age_bl age_sq_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave"

/*STABILIZED WEIGHTS TRUNCATED AT 1 PERCENTILE*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works_tr1: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall_tr1], cl(id) 
mimrgns, dydx(dmyet) predict(pr)

eststo mi_wage_tr1: mi est, post errorok esampvaryok: reg  indwage dmyet $x_prod [pw=stabweightdmyetall_wage], cl(id) 



foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr1: mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr1], cl(id)
mimrgns, dydx(dmyet) predict(pr)
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_tr1: mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr1], cl(id) 
}

forvalues i = 0/1{ // by sex
eststo mi_works_tr1_`i': mi est, post errorok or: logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)

eststo mi_wage_tr1_`i': mi est, post errorok esampvaryok: reg  lnindwage dmyet $x_prod [pw=stabweightdmyetall_wage`i'_tr1] if female == `i', cl(id) 

foreach var of varlist anyalc stillsmoking{  //by sex and binary
eststo mi_`var'_tr1_`i': mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_tr1_`i': mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}



/*DURATION*/

eststo mi_works_tr1_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr1], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

eststo mi_wage_tr1_dur: mi est, post errorok esampvaryok: reg  lnindwage yearsdiagall $x_prod [pw=stabweightdmyetall_wage_tr1], cl(id) 


foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr1_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr1], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
}

foreach var of varlist bmi waist d3kcal{  // continuous
eststo mi_`var'_tr1_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr1], cl(id)
}

forvalues i = 0/1{  // by sex
eststo mi_works_tr1_`i'_dur: mi est, post errorok or: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
eststo mi_wage_tr1_`i'_dur: mi est, post errorok esampvaryok: reg  lnindwage yearsdiagall $x_prod [pw=stabweightdmyetall_wage`i'_tr1] if female == `i', cl(id) 


foreach var of varlist anyalc stillsmoking{  // by sex and binary
eststo mi_`var'_tr1_`i'_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_tr1_`i'_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr1] if female == `i', cl(id)
}
}

/*Duration groups*/


eststo mi_works_tr1_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall], cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
eststo mi_wage_tr1_dur_g: mi est, post errorok esampvaryok: reg lnindwage i.yearsdiagall_g $x_prod [pw=stabweightdmyetall_wage], cl(id)


foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr1_dur_g: mi est, post errorok or: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_tr1_dur_g: mi est, post errorok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
}

eststo mi_stillsmoking_tr1_0_dur_g: mi est, post errorok or: logistic  stillsmoking i.yearsdiagall_g $x_other [pw=stabweightdmyetall0] if female == 0, cl(id)
eststo mi_stillsmoking_tr1_1_dur_g: mi est, post errorok esampvaryok: logistic  stillsmoking i.yearsdiagall_g $x_other [pw=stabweightdmyetall1] if female == 1 & yearsdiagall_g < 7, cl(id)  // need to limit it to those with duration of less then 11 years this because sample size those with longer diabetes duration is to small so that they get ommitted which leads to problem for using mi. Also need to include varying sample option, because a different number of observations are now ommited in each dataset

forvalues i = 0/1{
eststo mi_works_tr1_`i'_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
eststo mi_wage_tr1_`i'_dur_g: mi est, post errorok esampvaryok: reg  indwage i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

eststo mi_anyalc_tr1_`i'_dur_g: mi est, post errorok or esampvaryok: logistic  anyalc i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_tr1_`i'_dur_g: mi est, post errorok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}





* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab mi_works_tr1 mi_stillsmoking_tr1  mi_anyalc_tr1 mi_bmi_tr1 mi_waist_tr1 mi_d3kcal_tr1 mi_wage_tr1 using "Tables/msm_estimates_mi_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr1_`i' mi_stillsmoking_tr1_`i'  mi_anyalc_tr1_`i' mi_bmi_tr1_`i' mi_waist_tr1_`i' mi_d3kcal_tr1_`i' mi_wage_tr1_`i' using "Tables/msm_estimates_mi_tr1_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_tr1_dur mi_stillsmoking_tr1_dur  mi_anyalc_tr1_dur mi_bmi_tr1_dur mi_waist_tr1_dur mi_d3kcal_tr1_dur mi_wage_tr1_dur using "Tables/msm_estimates_mi_tr1_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr1_`i'_dur mi_stillsmoking_tr1_`i'_dur  mi_anyalc_tr1_`i'_dur mi_bmi_tr1_`i'_dur mi_waist_tr1_`i'_dur mi_d3kcal_tr1_`i'_dur mi_wage_tr1_`i'_dur using "Tables/msm_estimates_mi_tr1_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage") ${table_reduced} fragment
}


estwrite mi_* using "savedestimates/diabetes_mi_msm", id() replace

*Duration groups
  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0 ) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall_g*) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_tr1_dur_g mi_stillsmoking_tr1_dur_g  mi_anyalc_tr1_dur_g mi_bmi_tr1_dur_g mi_waist_tr1_dur_g mi_d3kcal_tr1_dur_g using "Tables/msm_estimates_mi_dur_g_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr1_`i'_dur_g mi_stillsmoking_tr1_`i'_dur_g  mi_anyalc_tr1_`i'_dur_g mi_bmi_tr1_`i'_dur_g mi_waist_tr1_`i'_dur_g mi_d3kcal_tr1_`i'_dur_g using "Tables/msm_estimates_mi_`i'_dur_g_tr1.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

log close

log using Logs/china_dm30_bl_result_no_truncation.log, replace
/*NO TRUNCATION*/


/*Stabilized weights*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall], cl(id)

eststo mi_wage: mi est, post errorok esampvaryok: reg lnindwage dmyet $x_prod [pw=stabweightdmyetall_wage], cl(id)

mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var': mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
mimrgns, dydx(dmyet) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var': mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall], cl(id)
}

forvalues i = 0/1{
eststo mi_works_`i': mi est, post errorok or: logistic works dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)
eststo mi_wage_`i': mi est, post errorok esampvaryok: reg lnindwage dmyet $x_prod [pw=stabweightdmyetall_wage`i'] if female == `i', cl(id)

foreach var of varlist anyalc stillsmoking{
eststo mi_`var'_`i': mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_`i': mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}




/*DURATION*/

eststo mi_works_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
eststo mi_wage_dur: mi est, post errorok esampvaryok: reg lnindwage yearsdiagall $x_prod [pw=stabweightdmyetall_wage], cl(id)


foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall], cl(id)
}

forvalues i = 0/1{
eststo mi_works_`i'_dur: mi est, post errorok or: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
eststo mi_wage_`i'_dur: mi est, post errorok esampvaryok: reg  lnindwage yearsdiagall $x_prod [pw=stabweightdmyetall_wage`i'] if female == `i', cl(id)


foreach var of varlist anyalc stillsmoking{
eststo mi_`var'_`i'_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_`i'_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}



/*DURATION GROUPS*/

eststo mi_works_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall], cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
eststo mi_wage_dur_g: mi est, post errorok esampvaryok: reg lnindwage i.yearsdiagall_g $x_prod [pw=stabweightdmyetall_wage], cl(id)


foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_dur_g: mi est, post errorok or: logistic  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_dur_g: mi est, post errorok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall], cl(id)
}

eststo mi_stillsmoking_0_dur_g: mi est, post errorok or: logistic  stillsmoking i.yearsdiagall_g $x_other [pw=stabweightdmyetall0] if female == 0, cl(id)
eststo mi_stillsmoking_1_dur_g: mi est, post errorok esampvaryok: logistic  stillsmoking i.yearsdiagall_g $x_other [pw=stabweightdmyetall1] if female == 1 & yearsdiagall_g < 7, cl(id)  // need to limit it to those with duration of less then 11 years this because sample size those with longer diabetes duration is to small so that they get ommitted which leads to problem for using mi. Also need to include varying sample option, because a different number of observations are now ommited in each dataset

forvalues i = 0/1{
eststo mi_works_`i'_dur_g: mi est, post errorok or: logistic works i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
eststo mi_wage_`i'_dur_g: mi est, post errorok esampvaryok: reg  indwage i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

eststo mi_anyalc_`i'_dur_g: mi est, post errorok or esampvaryok: logistic  anyalc i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_`i'_dur_g: mi est, post errorok: reg  `var' i.yearsdiagall_g $x_other [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm", id() replace
estread mi_* using "savedestimates/diabetes_mi_msm"
	*Tables
	
*Binary outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works mi_stillsmoking  mi_anyalc mi_bmi mi_waist mi_d3kcal mi_wage using "Tables/msm_estimates_mi.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i' mi_stillsmoking_`i'  mi_anyalc_`i' mi_bmi_`i' mi_waist_`i' mi_d3kcal_`i' mi_wage_`i' using "Tables/msm_estimates_mi_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0 0 ) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_dur mi_stillsmoking_dur  mi_anyalc_dur mi_bmi_dur mi_waist_dur mi_d3kcal_dur mi_wage_dur using "Tables/msm_estimates_mi_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i'_dur mi_stillsmoking_`i'_dur  mi_anyalc_`i'_dur mi_bmi_`i'_dur mi_waist_`i'_dur mi_d3kcal_`i'_dur mi_wage_`i'_dur using "Tables/msm_estimates_mi_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage") ${table_reduced} fragment
}


*Duration groups outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall_g*) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_dur_g mi_stillsmoking_dur_g  mi_anyalc_dur_g mi_bmi_dur_g mi_waist_dur_g mi_d3kcal_dur_g mi_wage_dur_g using "Tables/msm_estimates_mi_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i'_dur_g mi_stillsmoking_`i'_dur_g  mi_anyalc_`i'_dur_g mi_bmi_`i'_dur_g mi_waist_`i'_dur_g mi_d3kcal_`i'_dur_g mi_wage_`i'_dur_g using "Tables/msm_estimates_mi_`i'_dur_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)" "Wage") ${table_reduced} fragment
}


log close

log using Logs/china_dm30_bl_result_no_weights.log, replace


/*NO WEIGHTS*/


//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works_nw: mi est, post errorok or: logistic works dmyet $x_prod, cl(id)
eststo mi_works_nw_fe: mi est, post errorok or: logistic works dmyet $x_prod, cl(id)


mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_nw: mi est, post errorok or: logistic  `var' dmyet $x_other, cl(id)
mimrgns, dydx(dmyet) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_nw: mi est, post errorok: reg  `var' dmyet $x_other, cl(id)
}

forvalues i = 0/1{
eststo mi_works_`i'_nw: mi est, post errorok or: logistic works dmyet $x_other if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{
eststo mi_`var'_`i'_nw: mi est, post errorok or: logistic  `var' dmyet $x_other if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_`i'_nw: mi est, post errorok: reg  `var' dmyet $x_other if female == `i', cl(id)
}
}




/*DURATION*/

eststo mi_works_dur_nw: mi est, post errorok or: logistic works yearsdiagall $x_prod, cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_dur_nw: mi est, post errorok or: logistic  `var' yearsdiagall $x_other, cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_dur_nw: mi est, post errorok: reg  `var' yearsdiagall $x_other, cl(id)
}

forvalues i = 0/1{
eststo mi_works_`i'_dur_nw: mi est, post errorok or: logistic works yearsdiagall $x_other if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

foreach var of varlist anyalc stillsmoking{
eststo mi_`var'_`i'_dur_nw: mi est, post errorok or: logistic  `var' yearsdiagall $x_other if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_`i'_dur_nw: mi est, post errorok: reg  `var' yearsdiagall $x_other if female == `i', cl(id)
}
}




log close

estwrite mi_* using "savedestimates/diabetes_mi_msm", id() replace


* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab mi_works mi_stillsmoking_nw  mi_anyalc_nw mi_bmi_nw mi_waist_nw mi_d3kcal_nw using "Tables/msm_estimates_mi_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i'_nw mi_stillsmoking_`i'_nw  mi_anyalc_`i'_nw mi_bmi_`i'_nw mi_waist_`i'_nw mi_d3kcal_`i'_nw using "Tables/msm_estimates_mi_`i'_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_dur_nw mi_stillsmoking_dur_nw  mi_anyalc_dur_nw mi_bmi_dur_nw mi_waist_dur_nw mi_d3kcal_dur_nw using "Tables/msm_estimates_mi_dur_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i'_dur_nw mi_stillsmoking_`i'_dur_nw  mi_anyalc_`i'_dur_nw mi_bmi_`i'_dur_nw mi_waist_`i'_dur_nw mi_d3kcal_`i'_dur_nw using "Tables/msm_estimates_mi_`i'_dur_nw.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}



estread * using "savedestimates/diabetes_mi_msm"






log using Logs/china_dm30_bl_result_no_MSM_adjusted.log, replace

/*NO WEIGHTS BUT COVARIATE ADJUSTED*/

global x_prod "age age_sq female rural han ib(freq).province bmi index waist d3kcal stillsmoking anyalc secondary university married insurance i.wave"
global x_binary "age age_sq female rural han ib(freq).province bmi index waist d3kcal secondary university married works insurance i.wave"
global x_cont "age age_sq female rural han ib(freq).province anyalc stillsmoking secondary university married works insurance i.wave"

global x "age age_sq female rural han ib(freq).province bmi index waist d3kcal stillsmoking anyalc secondary university married works insurance i.wave" 

//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works_adj: mi est, post errorok: logistic works dmyet $x_prod, cl(id)
mimrgns, dydx(dmyet) predict(pr)
eststo mi_anyalc_adj: mi est, post errorok or: logistic  anyalc dmyet $x_binary , cl(id)
mimrgns, dydx(dmyet) predict(pr)
eststo mi_stillsmoking_adj: mi est, post errorok or: logistic  stillsmoking dmyet $x_binary, cl(id)
mimrgns, dydx(dmyet) predict(pr)


eststo mi_bmi_adj: mi est, post errorok: reg  bmi dmyet $x_cont d3kcal, cl(id) ro
eststo mi_waist_adj: mi est, post errorok: reg  waist dmyet $x_cont d3kcal, cl(id) ro 
eststo mi_d3kcal_adj: mi est, post errorok: reg  d3kcal dmyet $x_cont bmi waist, cl(id) ro


forvalues i = 0/1{
eststo mi_works_`i'_adj: mi est, post errorok or: logistic works dmyet $x_prod if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)

eststo mi_anyalc_`i'_adj: mi est, post errorok or: logistic  anyalc dmyet $x_binary if female == `i' , cl(id)
mimrgns, dydx(dmyet) predict(pr)
eststo mi_stillsmoking_`i'_adj: mi est, post errorok or: logistic  stillsmoking dmyet $x_binary if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)

eststo mi_bmi_`i'_adj: mi est, post errorok: reg  bmi dmyet $x_cont d3kcal if female == `i', cl(id) ro
eststo mi_waist_`i'_adj: mi est, post errorok: reg  waist dmyet $x_cont d3kcal if female == `i', cl(id) ro 
eststo mi_d3kcal_`i'_adj: mi est, post errorok: reg  d3kcal dmyet $x_cont bmi if female == `i', cl(id) ro
}





/*DURATION*/

eststo mi_works_dur_adj: mi est, post errorok noisily: logistic works yearsdiagall $x_prod, cl(id) iterate(20)
mimrgns, dydx(yearsdiagall) predict(pr)

eststo mi_anyalc_dur_adj: mi est, post errorok or: logistic  anyalc yearsdiagall $x_binary , cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
eststo mi_stillsmoking_dur_adj: mi est, post errorok or: logistic  stillsmoking yearsdiagall $x_binary, cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)


eststo mi_bmi_dur_adj: mi est, post errorok: reg  bmi yearsdiagall $x_cont d3kcal, cl(id) ro
eststo mi_waist_dur_adj: mi est, post errorok: reg  waist yearsdiagall $x_cont d3kcal, cl(id) ro 
eststo mi_d3kcal_dur_adj: mi est, post errorok: reg  d3kcal yearsdiagall $x_cont bmi, cl(id) ro


forvalues i = 0/1{
eststo mi_works_dur_`i'_adj: mi est, post errorok or: logistic works yearsdiagall $x_prod if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

eststo mi_anyalc_dur_`i'_adj: mi est, post errorok or: logistic  anyalc yearsdiagall $x_binary if female == `i' , cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
eststo mi_stillsmoking_dur_`i'_adj: mi est, post errorok or: logistic  stillsmoking yearsdiagall $x_binary if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

eststo mi_bmi_dur_`i'_adj: mi est, post errorok: reg  bmi yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro
eststo mi_waist_dur_`i'_adj: mi est, post errorok: reg  waist yearsdiagall $x_cont d3kcal if female == `i', cl(id) ro 
eststo mi_d3kcal_dur_`i'_adj: mi est, post errorok: reg  d3kcal yearsdiagall $x_cont bmi waist if female == `i', cl(id) ro
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_dur_adj mi_stillsmoking_dur_adj  mi_anyalc_dur_adj mi_bmi_dur_adj mi_waist_dur_adj mi_d3kcal_dur_adj using "Tables/msm_estimates_mi_dur_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_dur_`i'_adj mi_stillsmoking_dur_`i'_adj  mi_anyalc_dur_`i'_adj mi_bmi_dur_`i'_adj mi_waist_dur_`i'_adj mi_d3kcal_dur_`i'_adj using "Tables/msm_estimates_mi_dur_`i'_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}


/*DURATION GROUPS*/

eststo mi_works_dur_g_adj: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall], cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
eststo mi_wage_dur_g_adj: mi est, post errorok esampvaryok: reg lnindwage i.yearsdiagall_g $x_prod [pw=stabweightdmyetall_wage], cl(id)


foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_dur_g_adj: mi est, post errorok or: logistic  `var' i.yearsdiagall_g $x_binary [pw=stabweightdmyetall], cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
}

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_dur_g_adj: mi est, post errorok: reg  `var' i.yearsdiagall_g $x_cont [pw=stabweightdmyetall], cl(id)
}

eststo mi_stillsmoking_0_dur_g_adj: mi est, post errorok or: logistic  stillsmoking i.yearsdiagall_g $x_binary [pw=stabweightdmyetall0] if female == 0, cl(id)
eststo mi_stillsmoking_1_dur_g_adj: mi est, post errorok esampvaryok: logistic  stillsmoking i.yearsdiagall_g $x_binary [pw=stabweightdmyetall1] if female == 1  & yearsdiagall_g < 7, cl(id)  // need to limit it to those with duration of less then 11 years this because sample size those with longer diabetes duration is to small so that they get ommitted which leads to problem for using mi. Also need to include varying sample option, because a different number of observations are now ommited in each dataset

forvalues i = 0/1{
eststo mi_works_`i'_dur_g_adj: mi est, post errorok or: logistic works i.yearsdiagall_g $x_prod [pw=stabweightdmyetall`i'] if female == `i', cl(id)
//mimrgns, dydx(i.yearsdiagall_g) predict(pr)
eststo mi_wage_`i'_dur_g_adj: mi est, post errorok esampvaryok: reg  indwage i.yearsdiagall_g $x_prod [pw=stabweightdmyetall`i'] if female == `i', cl(id)

eststo mi_anyalc_`i'_dur_g_adj: mi est, post errorok or esampvaryok: logistic  anyalc i.yearsdiagall_g $x_binary [pw=stabweightdmyetall`i'] if female == `i', cl(id)

foreach var of varlist bmi waist d3kcal{
eststo mi_`var'_`i'_dur_g_adj: mi est, post errorok: reg  `var' i.yearsdiagall_g $x_cont [pw=stabweightdmyetall`i'] if female == `i', cl(id)
}
}

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall_g*) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_dur_g_adj mi_stillsmoking_dur_g_adj  mi_anyalc_dur_g_adj mi_bmi_dur_g_adj mi_waist_dur_g_adj mi_d3kcal_dur_g_adj using "Tables/msm_estimates_mi_dur_g_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i'_dur_g_adj mi_stillsmoking_`i'_dur_g_adj  mi_anyalc_`i'_dur_g_adj mi_bmi_`i'_dur_g_adj mi_waist_`i'_dur_g_adj mi_d3kcal_`i'_dur_g_adj using "Tables/msm_estimates_mi_dur_`i'_g_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

estwrite mi_* using "savedestimates/diabetes_mi_msm", id() append


* Tables covariate adjusted

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label nobaselevels ///
	 eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab mi_works_adj mi_stillsmoking_adj  mi_anyalc_adj mi_bmi_adj mi_waist_adj mi_d3kcal_adj using "Tables/msm_estimates_mi_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_`i'_adj mi_stillsmoking_`i'_adj  mi_anyalc_`i'_adj mi_bmi_`i'_adj mi_waist_`i'_adj mi_d3kcal_`i'_adj using "Tables/msm_estimates_mi_`i'_adj.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}





/*Plotting results from FE and MSM duration group models*/

estread mi_* using "savedestimates/diabetes_mi_msm"  // MSM results
estread * using "savedestimates/diabetes_durationgroups_mi_fe_clog"  // FE results


*For employment
coefplot (mi_works_dur_g, recast(line)) (mi_works_fe_g, recast(line)), yline(1) keep(*yearsdiagall_g*) vertical eform  title("Employed(odds ratios)") name(works, replace) ylabel(1(1)6)
graph export Plots/plot_works.eps, replace

coefplot (mi_works_0_dur_g, recast(line)) (mi_works_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Employed (odds ratios)") name(works, replace) ylabel(1(1)6)
graph export Plots/plot_works_01.eps, replace

*For smoking
coefplot (mi_stillsmoking_dur_g, recast(line)) (mi_smoking_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Smokes (odds ratios)") name(stillsmoking, replace) ylabel(1(1)6)
graph export Plots/plot_stillsmoking.eps, replace 

coefplot (mi_stillsmoking_0_dur_g, recast(line)) (mi_stillsmoking_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Smokes (odds ratios)") name(stillsmoking, replace) ylabel(1(1)6)
graph export Plots/plot_stillsmoking_01.eps, replace

*For alcohol
coefplot (mi_anyalc_dur_g, recast(line)) (mi_anyalc_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Any alcohol (odds ratios)") name(anyalc, replace) ylabel(1(1)6) 
graph export Plots/plot_anyalc.eps, replace

coefplot (mi_anyalc_0_dur_g, recast(line)) (mi_anyalc_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical eform  title("Any alcohol (odds ratios)") name(anyalc, replace) ylabel(1(1)6)
graph export Plots/plot_anyalc_01.eps, replace


global options "yline(0) ylabel(,nogrid) keep(*yearsdiagall_g) vertical xlabel(,valuelabel angle(60))"
*For BMI
coefplot (mi_bmi_dur_g, recast(line)) (mi_bmi_fe_g, recast(line)), $options title(BMI) name(bmi, replace)  plotlabels("marginal structural model" "Fixed effects")
graph export Plots/plot_bmi.eps, replace

coefplot (mi_bmi_0_dur_g, recast(line)) (mi_bmi_fe_g0, recast(line)),  || ///
(mi_bmi_0_dur_g, recast(line)) (mi_bmi_1_dur_g, recast(line)) || , $options plotlabels("Males" "Females") name("BMI", replace) base
graph export Plots/plot_bmi_01.eps, replace

coefplot (mi_bmi_0_dur_g, recast(line)) (mi_bmi_1_dur_g, recast(line)), keep(*yearsdiagall_g*) vertical  title("BMI (MSM)") name(bmi, replace)
graph export Plots/plot_bmi_msm.eps, replace

coefplot (mi_bmi_fe_g0, recast(line)) (mi_bmi_fe_g1, recast(line)), keep(*yearsdiagall_g*) vertical  title("BMI (MSM)") name(FE, replace)
graph export Plots/plot_bmi_fe.eps, replace



*For waist
coefplot (mi_waist_dur_g, recast(line)) (mi_waist_fe_g, recast(line)), keep(*yearsdiagall_g*) vertical  title(waist) name(waist, replace) 
graph export Plots/plot_waist.eps, replace

coefplot (mi_waist_0_dur_g, recast(line)) (mi_waist_fe_g0, recast(line)), keep(*yearsdiagall_g*) vertical  title("Waist (men)") name(waist, replace)
graph export Plots/plot_waist_01.eps, replace

coefplot (mi_waist_0_dur_g, recast(line)) (mi_waist_1_dur_g, recast(line)), keep(*yearsdiagall_g*) vertical  title("waist (MSM)") name(waist, replace)
graph export Plots/plot_waist_msm.eps, replace

coefplot (mi_waist_fe_g0, recast(line)) (mi_waist_fe_g1, recast(line)), keep(*yearsdiagall_g*) vertical  title("waist (MSM)") name(FE, replace)
graph export Plots/plot_waist_fe.eps, replace
*For kcal
coefplot (mi_d3kcal_dur_g, recast(line)) (mi_kcal_fe_g, recast(line)), label keep(*yearsdiagall_g*) vertical  title("Daily calorie consumption") name(d3kcal, replace)
graph export Plots/plot_d3kcal.eps, replace

coefplot (mi_d3kcal_0_dur_g, recast(line)) (mi_kcal_fe_g0, recast(line)), keep(*yearsdiagall_g*) label vertical  title("Daily calorie consumption") name(d3kcal, replace)
graph export Plots/plot_d3kcal_01.eps, replace


/*
log using Logs/china_dm30_bl_result_tr2.log, replace

/*STABILIZED WEIGHTS TRUNCATED AT 2 PERCENTILE*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works_tr2: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall_tr2], cl(id) 
mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr2: mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr2], cl(id)
mimrgns, dydx(dmyet) predict(pr)
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_tr2: mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr2], cl(id) 
}

forvalues i = 0/1{ // by sex
eststo mi_works_tr2_`i': mi est, post errorok or: logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{  //by sex and binary
eststo mi_`var'_tr2_`i': mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_tr2_`i': mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}
}



/*DURATION*/

eststo mi_works_tr2_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr2], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr2_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr2], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
}

foreach var of varlist bmi waist d3kcal{  // continuous
eststo mi_`var'_tr2_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr2], cl(id)
}

forvalues i = 0/1{  // by sex
eststo mi_works_tr2_`i'_dur: mi est, post errorok or: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

foreach var of varlist anyalc stillsmoking{  // by sex and binary
eststo mi_`var'_tr2_`i'_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_tr2_`i'_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr2] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm", id() append



* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab mi_works_tr2 mi_stillsmoking_tr2  mi_anyalc_tr2 mi_bmi_tr2 mi_waist_tr2 mi_d3kcal_tr2 using "Tables/msm_estimates_mi_tr2.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr2_`i' mi_stillsmoking_tr2_`i'  mi_anyalc_tr2_`i' mi_bmi_tr2_`i' mi_waist_tr2_`i' mi_d3kcal_tr2_`i' using "Tables/msm_estimates_mi_tr2_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_tr2_dur mi_stillsmoking_tr2_dur  mi_anyalc_tr2_dur mi_bmi_tr2_dur mi_waist_tr2_dur mi_d3kcal_tr2_dur using "Tables/msm_estimates_mi_tr2_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr2_`i'_dur mi_stillsmoking_tr2_`i'_dur  mi_anyalc_tr2_`i'_dur mi_bmi_tr2_`i'_dur mi_waist_tr2_`i'_dur mi_d3kcal_tr2_`i'_dur using "Tables/msm_estimates_mi_tr2_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

log close

log using Logs/china_dm30_bl_result_tr5.log, replace

/*STABILIZED WEIGHTS TRUNCATED AT 5 PERCENTILE*/

//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)

eststo mi_works_tr5: mi est, post errorok or: logistic works dmyet $x_prod [pw=stabweightdmyetall_tr5], cl(id)
mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr5: mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall_tr5], cl(id)
mimrgns, dydx(dmyet) predict(pr)
}

foreach var of varlist bmi waist d3kcal{  // for continuous outcomes
eststo mi_`var'_tr5: mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall_tr5], cl(id)
}

forvalues i = 0/1{ // by sex
eststo mi_works_tr5_`i': mi est, post errorok or: logistic works dmyet $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
mimrgns, dydx(dmyet) predict(pr)

foreach var of varlist anyalc stillsmoking{  //by sex and binary
eststo mi_`var'_tr5_`i': mi est, post errorok or: logistic  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_tr5_`i': mi est, post errorok: reg  `var' dmyet $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}
}



/*DURATION*/

eststo mi_works_tr5_dur: mi est, post errorok or: logistic works yearsdiagall $x_prod [pw=stabweightdmyetall_tr5], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

foreach var of varlist anyalc stillsmoking{ // for binary outcomes
eststo mi_`var'_tr5_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr5], cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)
}

foreach var of varlist bmi waist d3kcal{  // continuous
eststo mi_`var'_tr5_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall_tr5], cl(id)
}

forvalues i = 0/1{  // by sex
eststo mi_works_tr5_`i'_dur: mi est, post errorok or: logistic works yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
mimrgns, dydx(yearsdiagall) predict(pr)

foreach var of varlist anyalc stillsmoking{  // by sex and binary
eststo mi_`var'_tr5_`i'_dur: mi est, post errorok or: logistic  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}

foreach var of varlist bmi waist d3kcal{  // by sex and continuous
eststo mi_`var'_tr5_`i'_dur: mi est, post errorok: reg  `var' yearsdiagall $x_other [pw=stabweightdmyetall`i'_tr5] if female == `i', cl(id)
}
}


estwrite mi_* using "savedestimates/diabetes_mi_msm", id() replace



* Tables

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")
*Binary outcomes
*LATEX
esttab mi_works_tr5 mi_stillsmoking_tr5  mi_anyalc_tr5 mi_bmi_tr5 mi_waist_tr5 mi_d3kcal_tr5 using "Tables/msm_estimates_mi_tr5.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr5_`i' mi_stillsmoking_tr5_`i'  mi_anyalc_tr5_`i' mi_bmi_tr5_`i' mi_waist_tr5_`i' mi_d3kcal_tr5_`i' using "Tables/msm_estimates_mi_tr5_`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

*Duration outcomes

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(yearsdiagall) label  nobaselevels ///
	 addnote("Other control variables: Baseline values of age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_tr5_dur mi_stillsmoking_tr5_dur  mi_anyalc_tr5_dur mi_bmi_tr5_dur mi_waist_tr5_dur mi_d3kcal_tr5_dur using "Tables/msm_estimates_mi_tr5_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_tr5_`i'_dur mi_stillsmoking_tr5_`i'_dur  mi_anyalc_tr5_`i'_dur mi_bmi_tr5_`i'_dur mi_waist_tr5_`i'_dur mi_d3kcal_tr5_`i'_dur using "Tables/msm_estimates_mi_tr5_`i'_dur.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
}

estwrite mi_* using "savedestimates/diabetes_mi_msm", id() append

log close
*/

