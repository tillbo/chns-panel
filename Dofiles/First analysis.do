set more off
capture log close
clear matrix
global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder


use Data/ice30_long_duration_nodmyetmiss , clear  // multiple imputation dataset for duration
mi import ice, automatic  // importing ice dataset to mi
drop miss*
save Data/ice30_long_nodmyetmiss_imported, replace
saveold Data/ice30_long_nodmyetmiss_imported, replace
/*
// preparational do files
global homefolder ""/home/till/Dropbox/PhD/Data/China Data""

        do "/home/till/Dropbox/PhD/Data/China Data/merging.do"	//master china files
	do "/home/till/Dropbox/PhD/Data/China Data/variablecreation.do" //creating new variables
	
	*/
	

/*NOTE FOR THESIS: PRESENT NO MARGINAL EFFECTS OF MI RESULTS AS IT IS UNCLEAR HOW TO CALCULATE THEM.
 BEST IDEA IS TO ONLY PRESENT MARGINAL EFFECTS OF NON IMPUTED DATA IN THE THESIS VERSION.*/


****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************
global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
use Data/ice30_long_nodmyetmiss_imported, clear  // multiple imputation dataset for duration

mi xeq: gen age_sq = age^2

dummies province
dummies wave
* center variables and create means to later use them in within-between effects models for binary outcomes
mi xeq: bysort id: center yearsdiagall works anyalc bmi waist stillsmoking age_sq married secondary university insurance index wave2-wave6 d3kcal, casewise meansave(m__) prefix(c__) // use center to create demeande

foreach var of varlist *__works *__anyalc *__bmi *__waist *__stillsmoking *__d3kcal{  // need to be renamed to not be always included as explanatory variables
mi rename `var' dep_`var', noupdate
}
mi update

save Data/ice30_long_nodmyetmiss_imported_wb, replace // save including means and demeaned values

use Data/ice30_long_nodmyetmiss_imported_wb, clear

//mi extract 0
global x "yearsdiagall age_sq female wave han ib(freq).province rural married secondary university insurance index"
global x_nf "yearsdiagall age_sq female  wave2 wave3  wave4 wave5 wave6 han rural married secondary university insurance index provinc1 - provinc9 provinc11 provinc12"  // global without factor variabels

log using Logs/fe_duration_mi_robust_checks.log, append


xthybrid works $x_nf if _mi_m == 0, family(binomial) link(logit)  // vce(cluster id) clusterid(id) 
margins, dydx(c__yearsdiagall)
xtlogit works $x_nf if _mi_m == 0, re //vce(ro)
margins, dydx(c__yearsdiagall)
mi xeq: bysort id (wave): egen seq=seq()  // creates number of waves that people have went through
mi xeq: bysort id (wave): egen maxseq=max(seq)  // indicates the maximum number of waves an individual attended. I need to drop all those with only one wave because for those the marginal structural model will not work as they have no prior information

//drop if maxseq==1

xtlogit works $x if _mi_m == 0, fe
margins, dydx(yearsdiagall)
xtreg works $x if _mi_m == 0, fe
mi estimate, post cmdok  cmdok: xthybrid works $x_nf, clusterid(id) family(binomial) link(logit) full 
mimrgns, dydx(W_yearsdiagall)

log close 


log using Logs/fe_duration_mi_results.log, replace

eststo mi_works_gee: mi est, post cmdok  : xthybrid works $x_nf, family(binomial) link(logit)    vce(cluster id) clusterid(id) full
eststo mi_smoking_gee: mi est, post cmdok : xthybrid stillsmoking $x_nf works, family(binomial) link(logit)    vce(cluster id) clusterid(id)  full
eststo mi_alc_gee: mi est, post cmdok : xthybrid anyalc $x_nf works, family(binomial) link(logit)    vce(cluster id) clusterid(id)  full
eststo mi_bmi_fe: mi est, post cmdok: xtreg bmi $x works indwage, fe 
eststo mi_waist_fe: mi est, post cmdok: xtreg waist $x works, fe 
eststo mi_kcal_fe: mi est, post cmdok: xtreg d3kcal $x works, fe 
/*BY GENDER*/

global x_nf "yearsdiagall age_sq wave2 - wave6 han rural married secondary university insurance index provinc1 - provinc9 provinc11 provinc12"  // global without factor variabels

forvalues i =0/1{
display `i'
eststo mi_works_gee`i': mi est, post cmdok : xthybrid works $x_nf  if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_smoking_gee`i': mi est, post cmdok : xthybrid stillsmoking $x_nf works if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_alc_gee`i': mi est, post cmdok : xthybrid anyalc $x_nf works if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id) 
}

forvalues i =0/1{
eststo mi_bmi_fe`i': mi est, post cmdok: xtreg bmi $x works if female == `i', fe 
eststo mi_waist_fe`i': mi est, post cmdok: xtreg waist $x works if female == `i', fe 
eststo mi_kcal_fe`i': mi est, post cmdok: xtreg d3kcal $x works if female == `i', fe 

}

estwrite mi_* using "savedestimates/diabetes_duration_mi_fe", id() replace
estread mi_* using "savedestimates/diabetes_duration_mi_fe"

//label var c__yearsdiagall "Time since diagnosis"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*yearsdiagall)  label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_gee mi_smoking_gee  mi_alc_gee mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_duration.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_gee`i' mi_smoking_gee`i'  mi_alc_gee`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell  ///
	 collabels(none) keep(*yearsdiagall) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab mi_works_gee mi_smoking_gee  mi_alc_gee mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_duration.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_gee`i' mi_smoking_gee`i'  mi_alc_gee`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

***************************************
/*+++++Binary diabetes indicator+++++*/
***************************************

global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
use Data/ice30_long_nodmyetmiss_imported, clear  // multiple imputation dataset for duration


mi xeq: gen age_sq = age^2
global x "dmyet age_sq i.han i.province rural married secondary university insurance index i.wave"
global x_nf "dmyet age_sq female  wave2 - wave6 han rural married secondary university insurance index provinc1 - provinc9 provinc11 provinc12"  // global without factor variabels

dummies province
dummies wave
* center variables and create means to later use them in within-between effects models for binary outcomes
mi xeq: bysort id: center dmyet works anyalc bmi waist stillsmoking age_sq married secondary university insurance index wave2-wave6 d3kcal, casewise meansave(m__) prefix(c__) // use center to create demeande

foreach var of varlist *__works *__anyalc *__bmi *__waist *__stillsmoking *__d3kcal{  // need to be renamed to not be always included as explanatory variables
mi rename `var' dep_`var', noupdate
}
mi update

//eststo mi_works_gee_ia: mi est, post cmdok : xthybrid works $x_nf c.c__dmyet#i.female c.m__dmyet#i.female female, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
mi est, post cmdok: xthybrid works $x_nf, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
mi est, post cmdok: xthybrid works $x_nf, family(binomial) link(probit)    vce(cluster id) clusterid(id)
mi est, post cmdok: xtreg works $x_nf, re ro
mi est, post cmdok: xtreg works $x_nf, re ro
mi est, post cmdok: xtreg works $x, fe ro

 
eststo mi_smoking_gee: mi est, post cmdok : xthybrid stillsmoking $x_nf works, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_alc_gee: mi est, post cmdok : xthybrid anyalc $x_nf works, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_bmi_fe: mi est, post cmdok: xtreg bmi $x works indwage, fe 
eststo mi_waist_fe: mi est, post cmdok: xtreg waist $x works, fe 
eststo mi_kcal_fe: mi est, post cmdok: xtreg d3kcal $x works, fe 
/*BY GENDER*/
global x_nf "dmyet age_sq  wave2 - wave6 han rural married secondary university insurance index provinc1 - provinc9 provinc11 provinc12"  // global without factor variabels

forvalues i =0/1{
display `i'
eststo mi_works_gee`i': mi est, post cmdok : xthybrid works $x_nf i.province  if female == `i', family(binomial) link(logit)   vce(cluster id) clusterid(id) 
eststo mi_smoking_gee`i': mi est, post cmdok : xthybrid stillsmoking $x_nf i.province works if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_alc_gee`i': mi est, post cmdok : xthybrid anyalc $x_nf i.province works if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id) 
}

forvalues i =0/1{
eststo mi_bmi_fe`i': mi est, post cmdok: xtreg bmi $x works if female == `i', fe 
eststo mi_waist_fe`i': mi est, post cmdok: xtreg waist $x works if female == `i', fe 
eststo mi_kcal_fe`i': mi est, post cmdok: xtreg d3kcal $x works if female == `i', fe 

}
estwrite mi_* using "savedestimates/diabetes_dummy_mi_fe", id() replace

label var c__dmyet "Diabetes"

/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

	  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar  ///
		 alignment(SS)   collabels(none) keep(dmyet c__dmyet) label  nobaselevels /// coeflables(1.diabetes Diabetes) ///
		 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

	*LATEX
	esttab mi_works_gee mi_smoking_gee  mi_alc_gee mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
		${table_reduced} 

	forvalues i=0/1{
	esttab mi_works_gee`i' mi_smoking_gee`i'  mi_alc_gee`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi.tex", append comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
	       }
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell  ///
	 collabels(none) keep(*dmyet) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab mi_works_gee mi_smoking_gee  mi_alc_gee mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_gee`i' mi_smoking_gee`i'  mi_alc_gee`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


estwrite mi_* using "savedestimates/diabetes_dummy_mi_fe", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_fe"





***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************
global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
capture log close

use Data/ice30_long_nodmyetmiss_imported, clear  // multiple imputation dataset for duration

log using Logs/fe_durationgroups_mi.log, replace
mi xtset id wave

replace yearsdiagall = 0 if dmyet == 0
dummies province
dummies wave
gen age_sq = age^2
gen yearsdiagall_r = round(yearsdiagall,1)

twoway lpolyci works yearsdiagall_r if female == 0, fcolor(none) alpattern(dash_dot)  || lpolyci works yearsdiagall_r if female == 1, ///
name(lpoly_duration, replace) ylabel(0(0.2)1, ang(hor) nogrid) xlabel(#15) bw(0.75) xtitle(Years since diagnosis) ytitle(Employment) ///
 xline(4 11 20, lstyle(grid)) legend(label(1 "CI") label(2 "Males") label(4 "Females") cols(1) position(3) order(2 4))  lpattern(dash) alpattern(shortdash) fcolor(none)  || if yearsdiagall_r < 25  // fintensity(inten20)
graph export lpoly_works_diabetesduration.eps, replace 

twoway lpolyci stillsmoking yearsdiagall_r if female == 0, fcolor(none) alpattern(dash_dot)  || lpolyci stillsmoking yearsdiagall_r if female == 1, ///
name(lpoly_duration, replace) ylabel(0(0.2)1, ang(hor) nogrid) xlabel(#15) bw(0.75) xtitle(Years since diagnosis) ytitle(Employment) ///
 xline(4 11 20, lstyle(grid)) legend(label(1 "CI") label(2 "Males") label(4 "Females") cols(1) position(3) order(2 4))  lpattern(dash) alpattern(shortdash) fcolor(none)  || if yearsdiagall_r < 25  // fintensity(inten20)
graph export lpoly_works_diabetesduration.eps, replace 

twoway lpolyci anyalc yearsdiagall_r if female == 0, fcolor(none) alpattern(dash_dot)  || lpolyci anyalc yearsdiagall_r if female == 1, ///
name(lpoly_duration, replace) ylabel(0(0.2)1, ang(hor) nogrid) xlabel(#15) bw(0.75) xtitle(Years since diagnosis) ytitle(Employment) ///
 xline(4 11 20, lstyle(grid)) legend(label(1 "CI") label(2 "Males") label(4 "Females") cols(1) position(3) order(2 4))  lpattern(dash) alpattern(shortdash) fcolor(none)  || if yearsdiagall_r < 25  // fintensity(inten20)


recode yearsdiagall_r (0/1 = 1) ///
			(2/3 = 2) ///
			(4/5 = 3) ///
			(6/7 = 4) ///
			(8/9 = 5) ///
			(10/11 = 6) ///
			(12/13 = 7) ///
			(14/15 = 8) ///
			(16/20 = 9) ///
			(21/100 = 10) (-1000/-0.001 = 0) if dmyet == 1, gen(yearsdiagall_g)
			
replace yearsdiagall_g = 0 if dmyet == 0			
label define yearsdiagall_g 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-10" 7 "11-12" 8 "13-14" 9 "15-19" 10 "20+", replace
label value yearsdiagall_g yearsdiagall_g
label var yearsdiagall_g "Years since diagnosis in groups"
dummies yearsdiagall_g 

mi xeq: mkspline yearsdiagall_sp1 2 yearsdiagall_sp2 5 yearsdiagall_sp3 8 yearsdiagall_sp4 11 yearsdiagall_sp5 15 yearsdiagall_sp6 20  yearsdiagall_sp7   = yearsdiagall_r
mi update

save Data/ice30_long_duration_g , replace  // multiple imputation dataset for duration
use Data/ice30_long_duration_g , clear
//log using Logs/fe_durationgroups_mi.log, append

global x "i.yearsdiagall_g age_sq female i.wave i.han i.province rural married secondary university insurance index"
global x_nf "yearsdiagall_g2 - yearsdiagall_g11 age_sq  wave2 - wave6 han rural married secondary university insurance index provinc1 - provinc9 provinc11 provinc12"



eststo mi_works_gee: mi est, post cmdok : xthybrid works $x_nf, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
mimrgns, dydx(*yearsdiagall*)

eststo mi_smoking_gee: mi est, post cmdok : xthybrid stillsmoking $x_nf works, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_alc_gee: mi est, post cmdok : xthybrid anyalc $x_nf works, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
eststo mi_bmi_fe_g: mi est, post cmdok: xtreg bmi $x works, fe 
eststo mi_waist_fe: mi est, post cmdok: xtreg waist $x works, fe 
eststo mi_kcal_fe: mi est, post cmdok: xtreg d3kcal $x works, fe 
/*BY GENDER*/

eststo mi_works_gee1: mi est, post cmdok : xthybrid works $x_nf i.province  if female == 1, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
mimrgns , dydx(*__yearsdiagall_g*) predict(pr)
eststo mi_smoking_gee1: mi est, post cmdok errorok noisily: xthybrid stillsmoking $x_nf ib(#10).province works if female == 1, family(binomial) link(logit)    vce(cluster id) clusterid(id) 
*/

	*Using xtreg to see marginal effects
forvalues i =0/1 {
display `i'
eststo mi_works_gee`i'_sp: mi est, post cmdok errorok: xtreg works $x_nf i.province  if female == `i', re ro
eststo mi_smoking_gee`i'_sp: mi est, post cmdok errorok: xtreg stillsmoking $x_nf i.province works if female == `i', re ro
eststo mi_alc_gee`i'_sp: mi est, post cmdok errorok: xtreg anyalc $x_nf i.province works if female == `i', re ro
}
	*Using xthybrid and later mimargns command to compare marginal effects to xtreg results
forvalues i =0/1 {
display `i'
eststo mi_works_gee1_sp: mi est, post cmdok errorok: xthybrid works $x_nf i.province  if female == 1, family(binomial) link(logit)    vce(cluster id) clusterid(id)
mimrgns, dydx(*yearsdiagall*) predict(pr)
eststo mi_smoking_gee`i'_sp: mi est, post cmdok errorok: xthybrid stillsmoking $x_nf i.province works if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id)
mimrgns, dydx(*yearsdiagall*)
eststo mi_alc_gee`i'_sp: mi est, post cmdok errorok: xthybrid anyalc $x_nf i.province works if female == `i', family(binomial) link(logit)    vce(cluster id) clusterid(id) 
mimrgns, dydx(*yearsdiagall*)
mimrgns, dydx(*yearsdiagall*) predict(pr)
}
/*eststo mi_works_gee1: mi est, post cmdok : xthybrid works $x_nf i.province  if female == 1, family(binomial) link(logit)    vce(cluster id) clusterid(id) force
eststo mi_smoking_gee1: mi est, post cmdok : xthybrid stillsmoking $x_nf i.province works if female == 1, family(binomial) link(logit)    vce(cluster id) clusterid(id) force
eststo mi_alc_gee1: mi est, post cmdok : xthybrid anyalc $x_nf i.province works if female == 1, family(binomial) link(logit)    vce(cluster id) clusterid(id) force
eststo mi_smoking_gee1: mi est, post cmdok: xtreg stillsmoking $x works if female == 1, fe ro
eststo mi_alc_gee1: mi est, post cmdok: xtreg anyalc $x works if female == 1, fe ro
*/

forvalues i =0/1{
eststo mi_bmi_fe`i'_sp: mi est, post cmdok errorok: xtreg bmi $x works if female == `i', fe 
eststo mi_waist_fe`i'_sp: mi est, post cmdok errorok: xtreg waist $x works if female == `i', fe 
eststo mi_kcal_fe`i'_sp: mi est, post cmdok errorok: xtreg d3kcal $x works if female == `i', fe 

}

estwrite mi_* using "savedestimates/diabetes_durationgroups_mi_fe", id() replace
estread * using "savedestimates/diabetes_durationgroups_mi_fe"

label var c__yearsdiagall "Time since diagnosis"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced b(%9.3f) ci(%9.3f)  booktabs nolz nostar noobs  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(c__yearsdiagall_g* *.yearsdiagall_g*) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")

*LATEX
esttab mi_works_gee mi_smoking_gee  mi_alc_gee mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_duration_g.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_gee`i' mi_smoking_gee`i'  mi_alc_gee`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell  ///
	 collabels(none) keep(c__yearsdiagall_g1-c__yearsdiagall_g8) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status")


esttab mi_works_gee mi_smoking_gee  mi_alc_gee mi_bmi_fe mi_waist_fe mi_kcal_fe using "Tables/FE_estimates_mi_duration_g.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)")    ///
        ${table_reduced} 

forvalues i=0/1{
esttab mi_works_gee`i' mi_smoking_gee`i'  mi_alc_gee`i' mi_bmi_fe`i' mi_waist_fe`i' mi_kcal_fe`i' using "Tables/FE_estimates_mi_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


/*Use fixed effects logit command instead*/
global x "dmyet age_sq i.han i.province rural married secondary university insurance index i.wave"
global x_nf "dmyet age_sq  wave2 - wave6 secondary university insurance index"  // global without factor variabels

mi extract 0, clear

xtlogit works $x if female == 1, fe
gen inmodel = 1 if e(sample)==1
xthybrid works $x_nf if female == 1 & inmodel==1, family(binomial) link(logit) vce(cluster id) clusterid(id) full

qui mi xeq: bysort id (wave): egen seq=seq()  // creates number of waves that people have went through
qui mi xeq: bysort id (wave): egen maxseq=max(seq)  // indicates the maximum number of waves an individual attended. I need to drop all those with only one wave because for those the marginal structural model will not work as they have no prior information

drop if maxseq==1

capture log close
log using Logs/fe_mi_diabetes_dummy.log, replace
