set more off
capture log close

global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
**log-file starten**

log using "Logs/merging.log", replace
*merging datasets


// clean some datasets as they have duplicate observations and cannot be merged 
// clean education
use "Data/educ_00", clear

duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both


drop dups

save "Data/educ_cleaned.dta", replace

// clean jobs

use "Data/jobs_00", clear


duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both
drop dups



save "Data/jobs_cleaned.dta", replace

// clean roster

use "Data/rst_00", clear

duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups


save "Data/roster_cleaned.dta", replace


// clean healthcare

use "Data/hlth_00", clear


duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both


drop dups

save "Data/healthcare_cleaned.dta", replace


// clean biomarkers
use "Data/bio", clear

drop if HHID == . | LINE ==.

	gen str9 var1 = string(HHID, "%09.0f")
	gen str3 var2 = string(LINE, "%03.0f") 

	gen IDind = var1 + var2

destring IDind, replace

duplicates tag IDind, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both
 gen wave = 2009
 
 drop dups


save "Data/bio_cleaned.dta", replace


// clean insurance data
use "Data/ins_00", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both
drop dups


save "Data/insurance_cleaned.dta", replace


// clean wage data
use "Data/wages_01", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both
drop dups


save "Data/wages_cleaned.dta", replace

// clean individual income data
use "Data/indinc_pub_00.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups>0  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/indinc_cleaned.dta", replace

// clean individual income data
use "Data/surveys_pub_01.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/surveys_pub_cleaned.dta", replace


// for gender and other dempgraphic data
use "Data/mast_pub_01.dta", clear
	
rename idind IDind

save "Data/mast_pub_cleaned.dta", replace


// clean individual income data
use "Data/hhinc_pub_00.dta", clear
	
duplicates tag hhid wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/hhinc_cleaned.dta", replace	

// clean garden data
use "Data/farmg_00.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/farmg_00_cleaned.dta", replace	

// clean household garden data for 1997 and 2000
use "Data/livei_00.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/livei_00_cleaned.dta", replace	

// clean fishing data
use "Data/fishi_00.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/fishi_00_cleaned.dta", replace	

// clean home business data 
use "Data/busi_00.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups>0  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/busi_00_cleaned.dta", replace

// clean time use data
use "Data/timea_00.dta", clear
	
duplicates tag IDind wave, gen(dups) // identify those that have same hhid and LINE number

drop if dups==1  // because I don't know why they are duplicates and which observation contains a mistake drop both

drop dups

save "Data/time_00_cleaned.dta", replace	

use "Data/educ_cleaned.dta", clear

merge 1:1 IDind wave using "Data/pact_00", nogen
merge 1:1 IDind wave using "Data/pexam_00", nogen 
merge 1:1 IDind wave using "Data/jobs_cleaned", nogen
merge 1:1 IDind wave  using "Data/roster_cleaned", nogen 
merge 1:1 IDind wave using "Data/healthcare_cleaned", nogen
merge 1:1 IDind wave using "Data/insurance_cleaned", nogen 
merge 1:1 IDind wave using  "Data/wages_cleaned", nogen
merge 1:1 IDind wave using  "Data/indinc_cleaned", nogen
merge m:1 hhid wave using "Data/hhinc_cleaned", nogen
merge 1:1 IDind wave using "Data/bio_cleaned", nogen
merge 1:1 IDind wave using "Data/surveys_pub_cleaned", nogen
merge m:1 IDind using "Data/mast_pub_cleaned", nogen
merge m:1 commid wave using "Data/m12urban_v02", nogen
merge 1:1 IDind wave using "Data/c12diet", nogen
merge 1:1 IDind wave using "Data/farmg_00_cleaned", nogen
merge 1:1 IDind wave using "Data/fishi_00_cleaned", nogen
merge 1:1 IDind wave using "Data/time_00_cleaned", nogen
merge 1:1 IDind wave using "Data/livei_00_cleaned", nogen
merge 1:1 IDind wave using "Data/busi_00_cleaned", nogen

rm *_cleaned.dta  // remove all created individual datasets used for merging


save "Data/merged_data.dta", replace

log close
exit

