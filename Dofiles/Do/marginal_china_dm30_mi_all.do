set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

log using Logs/china_dm30_all.log, replace

//use "ice30_cluster_miss.dta", clear
use "Data/ice30_all_long", clear

// drop lagged variables used for better imputation in ice but not needed here. Only keep first which is equal to baseline values
forvalues i=2/6{
foreach var of varlist *`i' {
drop `var'
	}
}
drop miss*  // drop missing indicators created by ice

save "Data/ice30_all_long_clean", replace
use "Data/ice30_all_long_clean", clear
mi import ice, automatic  // importing ice dataset to mi
mi xtset id wave
mi convert wide, clear  // converting to wide whihc makes stata work faster for data manipulation

mi passive: gen lnindwage = log(indwage)

mi register reg dmyet age han female province  index rural wave

/* Variable indicating if has become unemployed during survey*/
mi xeq: egen first_1 = min(wave / (works == 0)), by(id) // find year of first unemployment
mi xeq: by id (wave), sort: gen everunempl = 1 if wave==first_1 & works[_n-1]==1  // generate indicator for those who became unemployed after previosuly being employed
mi xeq: by id (wave), sort: replace everunempl= 1 if everunempl[_n-1]==1 // copy down 1 for years following first unemployment
mi xeq: replace everunempl = 0 if everunempl ==. & works <.  // replace rest of missing values with zeros for all those who were always unemployed or never or only in first occurance

sort id _mi_m wave
mi xeq: egen temp=min(wave) if everunempl == 1, by(id)
mi xeq: egen firstunemplidt=mean(temp), by(id)
drop temp


// renaming first observations to baseline values so I don't have to change code
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc indwage works age diabetes_sr {
mi rename `var'1 `var'_bl
}

by id _mi_m (wave),sort: gen age_sq = age^2

by id _mi_m (wave),sort: gen age_sq_bl = age_sq[1]

sort id _mi_m wave
mi xeq: egen temp=min(wave) if dmyet == 1, by(id)
mi xeq: egen firstdmidt=mean(temp), by(id)
drop temp

sort id wave _mi_m
mi xeq: by id (wave), sort: gen dmyet_bl=dmyet[1]
mi xeq: by id (wave), sort: gen everunempl_bl=everunempl[1]

mi xeq: by id (wave), sort: gen lnindwage_bl=lnindwage[1]

/*CREATING STABILIZED WEIGHTS*/
mi convert flong, clear
save "Data/ice30_weighted_all", replace
use "Data/ice30_weighted_all", clear

drop if dmyet_bl ==1
mi update
/*For dmyet as explanatory variable*/
mi estimate, saving(dmyetden_sav, replace) esample(denom): logistic dmyet age_bl age_sq age_sq_bl female rural han ib(freq).province index_bl bmi bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl age  insurance_bl works_bl index waist d3kcal stillsmoking anyalc secondary university married works i.wave if wave<=firstdmidt | firstdmidt ==. 
mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet age_bl age_sq_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt 
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
by id _mi_m (wave),sort: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
rename pdmyet dmyetnum
gen stabweightdmyetall =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
save "Data/ice30_weighted_all_dm", replace

/*With dmyet as explanatory variable for wage equation*/
/* Not working yet. Has something to do with the mi commands and creating log wage after imputation
mi estimate, saving(dmyetden_sav, replace) esample(denom): logistic dmyet age_bl age female rural han i.province index_bl  waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index waist d3kcal stillsmoking anyalc secondary university married  insurance i.wave lnindwage* if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet age_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl lnindwage_bl if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt 
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: bysort id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
rename pdmyet dmyetnum
gen stabweightdmyet_wage =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom

save "Data/ice30_weighted_all", replace
*/
/*Stabweights for ever being unemployed. Not working as expected yet. Likely also problem with creating the variable after imputation.*/
use "Data/ice30_weighted_all", clear

mi estimate, saving(everunemplden_sav, replace) esample(denom): logit everunempl age_bl age female dmyet dmyet_bl rural han i.province index_bl bmi_bl bmi  waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl index waist d3kcal stillsmoking anyalc secondary university married  insurance wave if wave<=firstunemplidt | firstunemplidt ==.

mi predict peverunempl using everunemplden_sav, esample(denom) storecompleted
replace peverunempl=invlogit(peverunempl)
replace peverunempl=1 if wave>firstunemplidt
replace peverunempl=peverunempl*everunempl + (1-peverunempl)*(1-everunempl)
rename peverunempl everunempldenom
// logistic everunempl age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl everunempl_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
mi estimate, saving(everunemplnom_sav, replace) esample(nom): logit everunempl dmyet_bl age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl wave if wave<=firstunemplidt | firstunemplidt ==.

mi predict peverunempl using everunemplnom_sav, esample(nom) storecompleted
replace peverunempl=invlogit(peverunempl)
replace peverunempl=1 if wave>firstunemplidt 
replace peverunempl=peverunempl*everunempl+(1-peverunempl)*(1-everunempl) 
by id _mi_m (wave),sort: replace peverunempl=peverunempl*peverunempl[_n-1] if _n!=1   
rename peverunempl everunemplnum
gen stabweighteverunempl =everunemplnum/everunempldenom
drop everunempldenom everunemplnum denom nom
save "Data/ice30_weighted_all_unempl", replace



use "Data/ice30_weighted_all_dm", clear

/*truncate weight to 1 and 99th percentile*/
foreach var of varlist stabweight*{
_pctile `var', nq(100) 
gen `var'_tr = `var'
replace `var'_tr = r(r99) if `var' > r(r99) & `var' <.
replace `var'_tr = r(r1) if `var' < r(r1)
}

		/*ANALYSIS*/
gen stabweightdmyetall_tr_1 = stabweightdmyetall_tr if dmyet_bl != 1 		
// have created two globals with controls for regression models. First for works and wage models and the second for all other models

global x_prod "age_bl age_sq_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave"
global x_other "age_bl age_sq_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave"

/*Stabilized weights*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)
mi estimate: logit works dmyet $x_other [pw=stabweightdmyetall_tr_1], cl(id)
mimrgns, dydx(dmyet) predict(pr)
mi estimate: regress bmi dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: regress waist dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: regress d3kcal dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: logistic  anyalc dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: logistic  stillsmoking dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mimrgns, dydx(dmyet) predict(pr)

log close

log using Logs/china_dm30_all_nostabweights.log, append

/*No stabilized weights*/
//mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl  if works == 1 & stabweightdmyet_tr<., cl(id)
mi estimate: logit works $x_other dmyet age age_sq index bmi waist d3kcal stillsmoking anyalc secondary university married insurance i.wave  if stabweightdmyet_tr<., cl(id)
mimrgns, dydx(dmyet) predict(pr)
mi estimate: regress bmi $x_other dmyet age index  waist d3kcal stillsmoking anyalc secondary university married insurance i.wave  if stabweightdmyet_tr<., cl(id)
mi estimate: regress waist $x_other dmyet age index bmi  d3kcal stillsmoking anyalc secondary university married insurance i.wave  if stabweightdmyet_tr<., cl(id)
mi estimate: regress d3kcal $x_other dmyet age index bmi waist  stillsmoking anyalc secondary university married insurance i.wave  if stabweightdmyet_tr<., cl(id)
mi estimate: logistic  anyalc $x_other dmyet age age_sq index bmi waist d3kcal stillsmoking secondary university married insurance i.wave if stabweightdmyet_tr<., cl(id)
mi estimate: logistic  stillsmoking $x_other dmyet age index bmi waist d3kcal anyalcsecondary university married insurance i.wave if stabweightdmyet_tr<., cl(id)

log close



