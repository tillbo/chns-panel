set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"

cd $homefolder
**log-file starten**
//log using Logs/xtdpdml.log, replace

use "Data/data_13.dta", clear

set matsize 1200, perm
egen newt = group(wave)

xtset id newt
******************************************************************************************************
/*Running GEE probit population average regressions to investigate the effect of a diabetes diagnosis
on behavioural and economic outcomes*/
******************************************************************************************************

global x "i.yearsdiagnomed i.yearsdiagmed i.diab_med age_ini age_sq female i.wave i.han i.province rural married secondary university insurance index"
global x_fe "i.yearsdiagnomed i.yearsdiagmed i.diab_med female  age_sq i.wave i.han i.province rural married secondary university insurance index"

global x_nf "yearsdiagall2 yearsdiagall3 yearsdiagall4 yearsdiagall5 yearsdiagall6 yearsdiagall7 index rural insurance"

/* probit estimations for binary variables*/

drop if age < 18 | age > 65


/*Alternative methods for estimation as proposed by Allison*/


xtdpdml works age index,  pre(diabetes_sr) show 
xtdpdml works age index diabetes_sr, show 
xtdpdml works age age_sq index,  pre(diabetes_sr) show 


xtdpdqml works age index diabetes_sr, fe 

exit

capture log close

/*
xtdpdml works index age insurance yearsdiagall6 yearsdiagall7, inv(han) nolog show 



