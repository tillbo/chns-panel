set more off
capture log close
clear matrix
*ssc install ice
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data_13.dta", clear

log using Logs/ice_cluster_duration.log, replace

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65
drop if province == .

gen education = 0 if secondary <.
replace education = 1 if secondary == 1
replace education = 2 if university == 1


/* baseline covariates  // don't need them for multiple imputation in wide format because it automatically takes baseline values into account
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc indwage works{
by id: gen `var'_bl= `var'[1] 
}
*/
* fixed: rural age_ini female han i.province


keep id diabetes_duration age female rural newt han province  index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works 

//rename provinc* provinc*r
reshape wide diabetes_duration age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works, i(id) j(newt)
forvalues i=1/6 {
foreach var in diabetes_duration index bmi waist d3kcal stillsmoking anyalc education married insurance indwage works age {
  replace `var'`i'= .a if age`i' == .
 }
}

ice female age* han diabetes_duration* rural* i.province index* bmi* waist* d3kcal* stillsmoking* anyalc* education* married* insurance* indwage* works*,  m(30) saving(ice30_cluster_miss_duration, replace) seed(49230) genmiss(m_)

xtset, clear

mi set wide

mi register imputed works1 works6  diabetes_duration*  stillsmoking* anyalc* education* married* insurance1 insurance2 insurance3 insurance4 insurance5 indwage* index* bmi* waist* d3kcal* 
mi register regular female age* han province rural works2 works3 works4 works5 insurance6 dmyet2 dmyet3 dmyet6
mi describe

set seed 49230
mi impute mvn works1 works6  diabetes_duration*  stillsmoking* anyalc* m.education* married* insurance1 insurance2 insurance3 insurance4 insurance5 indwage* bmi* waist* d3kcal*  = female age* index* han i.province rural works2 works3 works4 works5 university5 insurance6, add(30) dots force
save "Data/data_13_miss_duration.dta", replace
set seed 49230
mi impute chained (logit, augment) works1 works6  diabetes_duration*  stillsmoking* anyalc* married* insurance1 insurance2 insurance3 insurance4 insurance5 (mlogit, augment) education*  (regress) indwage*  bmi* waist* d3kcal*  = female age1 age2 age3 age4 age5 age6 han i.province rural works2 works3 works4 works5 insurance6 index*, add(30) noisily

save "Data/data_13_miss_duration.dta", replace

use ice30_cluster_miss_duration, clear

mi extract 0, clear

drop if _mj==0

reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works missing*, i(id _mj)

exit

capture log close


/*
ice dmyet age_ini female rural han m.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl indwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works, m(10) saving(ice10, replace)

