set more off
capture log close
clear matrix
*ssc install ice
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data_13.dta", clear

log using Logs/ice_cluster.log, append
/*
egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65
drop if province == .

gen education = 0 if secondary <.
replace education = 1 if secondary == 1
replace education = 2 if university == 1

gen dmyet=1 if diabetes_sr==1 
bysort id idt: replace dmyet=1 if dmyet!=1 & diabetes_sr[_n-1]==1 & _n!=1
bysort id idt: replace dmyet=1 if dmyet!=1 & dmyet[_n-1]==1 & _n!=1
replace dmyet = 0 if dmyet != 1

by id (idt), sort: gen everunempl = 1 if works == 0 & works[_n-1] == 1
by id (idt), sort: replace everunempl = 1 if works == 0 & works[_n-1] == 0
by id (idt), sort: replace everunempl = 1 if everunempl[_n-1] == 1 & everunempl[_n+1] > 0
by id (idt), sort: replace everunempl = 0 if everunempl == . & works !=.

/* baseline covariates  // don't need them for multiple imputation in wide format because it automatically takes baseline values into account
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc indwage works{
by id: gen `var'_bl= `var'[1] 
}
*/
* fixed: rural age_ini female han i.province



egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id dmyet age female rural newt han province  index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works everunempl

//rename provinc* provinc*r
reshape wide dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works everunempl, i(id) j(newt)


forvalues i=1/6 {
foreach var in dmyet`i' index`i' bmi`i' waist`i' d3kcal`i' stillsmoking`i' anyalc`i' education`i' married`i' insurance`i' indwage`i' works`i' age`i'{
  replace `var'= .a if age`i' == . | age`i' ==.a

 }
}

save "Data/data_13_miset.dta", replace // save data before imputation to leave clean datset to be used for chained imputation

// ice imputation

ice female age* han dmyet* rural* province index* bmi* waist* d3kcal* stillsmoking* anyalc* education* married* insurance* indwage* works*,  m(30) saving(ice30_cluster_miss, replace) seed(49230) genmiss(m_)


// reshape ice dataset
use ice30_cluster_miss, clear


drop if _mj==0

reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works m_age m_indwage, i(id _mj)
drop m_*1 m_*2 m_*3 m_*4 m_*5 m_*6
drop if m_age == 1

save "Data/ice30.dta", replace
*/
// mvn imputation

use "Data/data_13_miset.dta", clear

xtset, clear

mi set wide

mi register imputed works1 works6  dmyet1 dmyet4 dmyet5  stillsmoking* anyalc* education* married* insurance1 insurance2 insurance3 insurance4 insurance5 indwage* index* bmi* waist* d3kcal* 
mi register regular female age* han province rural works2 works3 works4 works5 insurance6 dmyet2 dmyet3 dmyet6
mi describe


set seed 49230
mi impute mvn works1 works6  dmyet1 dmyet4 dmyet5  stillsmoking* anyalc* education* married* insurance1 insurance2 insurance3 insurance4 insurance5 indwage* bmi* waist* d3kcal*  = female age* index* han i.province rural works2 works3 works4 works5 insurance6 dmyet2 dmyet3 dmyet6, add(30) dots force


mi reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works m_age m_indwage, i(id) j(newt)


save "Data/data_13_mvn.dta", replace

// mi chained imputation

use "Data/data_13_miset.dta"
set seed 49230
mi impute chained (logit, augment) works1 works6  dmyet1 dmyet4 dmyet5  stillsmoking* anyalc* married* insurance1 insurance2 insurance3 insurance4 insurance5 (mlogit, augment) education*  (regress) indwage*  bmi* waist* d3kcal*  = female age1 age2 age3 age4 age5 age6 han i.province rural works2 works3 works4 works5 insurance6 dmyet2 dmyet3 dmyet6 index*, add(30) noisily

mi reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works m_age m_indwage, i(id) j(newt)

save "Data/data_13_chained.dta", replace

exit

capture log close


/*
ice dmyet age_ini female rural han m.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl indwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works, m(10) saving(ice10, replace)

