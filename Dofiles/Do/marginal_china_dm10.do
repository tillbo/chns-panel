set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder
log using Logs/china_dm10.log, replace

//use "ice30_cluster_miss.dta", clear
use "Data/ice30_long", clear
mi import ice, automatic

drop if _mj==0

reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works m_age m_indwage, i(id _mj)
drop m_*1 m_*2 m_*3 m_*4 m_*5 m_*6
drop if m_age == 1
 
save "Data/ice30.dta", replace
use "Data/ice30.dta", clear
//reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works, i(id _mj)


drop if age < 18 | age > 65

drop if m_age == 1

rename _j  wave

replace indwage = 0 if works == 0
dummies education
rename education1 secondary
rename education2 university
gen lnindwage = log(indwage)


by id _mj (wave), sort: gen everunempl = 1 if works == 0 & works[_n-1] == 1
by id _mj (wave), sort: replace everunempl = 1 if works == 0 & works[_n-1] == 0
by id _mj (wave), sort: replace everunempl = 1 if everunempl[_n-1] == 1 & everunempl[_n+1] > 0
by id _mj (wave), sort: replace everunempl = 0 if everunempl == . & works !=.


// employment status at last obsevred wave
by id _mj (wave), sort: gen lastworks = works[_N]


foreach var of varlist age wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works dmyet lastworks{
bysort id _mj: gen `var'_bl= `var'[1]
}




drop if _mj==0
sort id _mj wave
egen temp=min(wave) if dmyet == 1, by(id _mj)
egen firstdmidt=mean(temp), by(id _mj)
drop temp


/*CREATING STABILIZED WEIGHTS*/
//  logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works idt wave if idt<=firstdmidt | firstdmidt ==.

gen stabweightdmyet = .  // generate stabweightdmyet to later replace with calculated weights in loop

forvalues i = 1/30 {
 logistic dmyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance works i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet) if _mj == `i'
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
 logistic dmyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) if _mj == `i'
bysort id _mj: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1 & _mj == `i'  // need to add _mj as another variable to sort by, otherwise it will not work
rename pdmyet dmyetnum
gen stabweightdmyet`i' =dmyetnum/dmyetdenom if _mj == `i'
drop dmyetdenom dmyetnum 
replace stabweightdmyet = stabweightdmyet`i' if _mj == `i'  // generates one overall stability weight
drop stabweightdmyet`i'
}

gen stabweightdmyet_alt = .  // generate alternative only using bl diabetes in numerator to increase number of observations 

forvalues i = 1/30 {
 logistic dmyet age_bl age female rural index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married works if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet) if _mj == `i'
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
 logistic dmyet age_bl female rural index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl works_bl if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) if _mj == `i'
bysort id _mj: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1 & _mj == `i'  // need to add _mj as another variable to sort by, otherwise it will not work
rename pdmyet dmyetnum
gen stabweightdmyet`i' =dmyetnum/dmyetdenom if _mj == `i'
drop dmyetdenom dmyetnum 
replace stabweightdmyet_alt = stabweightdmyet`i' if _mj == `i'  // generates one overall stability weight
drop stabweightdmyet`i'
}



save "Data/ice30_weighted", replace
gen stabweightdmyet_wage = .  // generate stabweightdmyet_wage to later replace with calculated weights in loop

forvalues i = 1/30 {
 logistic dmyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance works i.wave lnindwage* if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet) if _mj == `i'
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
 logistic dmyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl university_bl married_bl  insurance_bl works_bl i.wave lnindwage_bl if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) if _mj == `i'
bysort id _mj: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1 & _mj == `i'  // need to add _mj as another variable to sort by, otherwise it will not work
rename pdmyet dmyetnum
gen stabweightdmyet_wage`i' =dmyetnum/dmyetdenom if _mj == `i'
drop dmyetdenom dmyetnum 
replace stabweightdmyet_wage = stabweightdmyet_wage`i' if _mj == `i'  // generates one overall stability weight
drop stabweightdmyet_wage`i'
}


save "Data/ice30_weighted", replace
gen stabweighteverunempl = .  // generate stabweighteverunempl to later replace with calculated weights in loop

forvalues i = 1/30 {
 logistic everunempl age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl dmyet_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance dmyet i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict peverunempl if e(sample) & _mj == `i'
replace peverunempl=1 if wave>firstdmidt & _mj == `i'
replace peverunempl=peverunempl*everunempl + (1-peverunempl)*(1-everunempl) if _mj == `i'
rename peverunempl everunempldenom
// logistic everunempl age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl dmyet_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
 logistic everunempl age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl university_bl married_bl  insurance_bl dmyet_bl i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict peverunempl if e(sample) & _mj == `i'
replace peverunempl=1 if wave>firstdmidt & _mj == `i'
replace peverunempl=peverunempl*everunempl+(1-peverunempl)*(1-everunempl) if _mj == `i'
bysort id _mj: replace peverunempl=peverunempl*peverunempl[_n-1] if _n!=1 & _mj == `i'  // need to add _mj as another variable to sort by, otherwise it will not work
rename peverunempl everunemplnum
gen stabweighteverunempl`i' =everunemplnum/everunempldenom if _mj == `i'
drop everunempldenom everunemplnum 
replace stabweighteverunempl = stabweighteverunempl`i' if _mj == `i'  // generates one overall stability weight
drop stabweighteverunempl`i'
}

save "Data/ice30_weighted", replace

gen stabweightlnindwage = .  // generate stabweightlnindwage to later replace with calculated weights in loop

forvalues i = 1/30 {
 logistic lnindwage age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl dmyet_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance dmyet i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict plnindwage if e(sample) & _mj == `i'
replace plnindwage=1 if wave>firstdmidt & _mj == `i'
replace plnindwage=plnindwage*lnindwage + (1-plnindwage)*(1-lnindwage) if _mj == `i'
rename plnindwage lnindwagedenom
// logistic lnindwage age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl dmyet_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
 logistic lnindwage age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl university_bl married_bl  insurance_bl l(0/3).dmyet i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict plnindwage if e(sample) & _mj == `i'
replace plnindwage=1 if wave>firstdmidt & _mj == `i'
replace plnindwage=plnindwage*lnindwage+(1-plnindwage)*(1-lnindwage) if _mj == `i'
bysort id _mj: replace plnindwage=plnindwage*plnindwage[_n-1] if _n!=1 & _mj == `i'  // need to add _mj as another variable to sort by, otherwise it will not work
rename plnindwage lnindwagenum
gen stabweightlnindwage`i' =lnindwagenum/lnindwagedenom if _mj == `i'
drop lnindwagedenom lnindwagenum 
replace stabweightlnindwage = stabweightlnindwage`i' if _mj == `i'  // generates one overall stability weight
drop stabweightlnindwage`i'
}

save "Data/ice30_weighted", replace

use "Data/ice30_weighted", clear

/*truncate weight to 1 and 99th percentile*/
foreach var of varlist stabweightdmyet*{
_pctile `var', nq(100)
return list  // this is optional 
gen `var'_tr = `var' if `var' <= r(r99) & `var' >= r(r1)
}


// have created two globals with controls for regression models. First for works and wage models and the second for all other models

global x_prod "age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl age"
global x_other "age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl age works_bl"


 regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)
 logit lastworks dmyet $x_prod works_bl [pw=stabweightdmyet_alt_tr], cl(id)
 
 margins, dydx(dmyet)
  logit lastworks dmyet $x_prod works_bl, cl(id)

  logit works dmyet $x_prod [pw=stabweightdmyet_tr], cl(id)
 margins, dydx(dmyet)

 logit works dmyet $x_prod works_bl if stabweightdmyet_alt_tr <., cl(id)


 regress bmi dmyet $x_other [pw=stabweightdmyet], cl(id)
 regress waist dmyet $x_other [pw=stabweightdmyet], cl(id)
 regress d3kcal dmyet $x_other [pw=stabweightdmyet], cl(id)
 logistic  anyalc dmyet $x_other [pw=stabweightdmyet], cl(id)
 logistic works dmyet $x_prod [pw=stabweightdmyet], cl(id)


logit dmyet everunempl $x_other [pw=stabweighteverunempl], cl(id)
/*

  regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works == 1, cl(id)
  logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
  regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
  logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)


  regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works == 1, cl(id)
  logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
  regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
  logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)

* duration=1 for first wave diabetes is reported
replace duration=1 if wave==firstdmyear

  regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works == 1, cl(id)
  logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
  regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
  logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)

* unweighted  ADD TOME VARIANT VARIABLES HERE
  regress lnindwage dmyet $x_prod if works == 1, cl(id)
  logistic works dmyet $x_prod, cl(id)
  reg works dmyet $x_prod, cl(id)
  regress bmi dmyet $x_other, cl(id)
  regress waist dmyet $x_other, cl(id)
  regress d3kcal dmyet $x_other, cl(id)
  logistic  anyalc dmyet $x_other, cl(id)

