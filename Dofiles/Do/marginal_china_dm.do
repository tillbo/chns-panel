set more off
capture log close
clear matrix
/*
// preparational do files
global homefolder ""/home/till/Dropbox/PhD/Data/China Data""

        do "/home/till/Dropbox/PhD/Data/China Data/merging.do"	//master china files
	do "/home/till/Dropbox/PhD/Data/China Data/variablecreation.do" //creating new variables
	
	*/
	



global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"

cd $homefolder

use "Data/data_13.dta", clear

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65



by id (wave), sort: gen unemplyet = 1 if works == 0 & works[_n-1] == 1
by id (wave), sort: replace unemplyet = 1 if works == 0 & works[_n-1] == 0
by id (wave), sort: replace unemplyet = 1 if unemplyet[_n-1] == 1 & unemplyet[_n+1] > 0
by id (wave), sort: replace unemplyet = 0 if unemplyet == . & works !=.






gen dmyet = diabetes_sr
by id (idt), sort: replace dmyet = dmyet[_n-1] if dmyet[_n-1] == 1


/*gen dmnow= diabetes_sr==1 
by id (idt), sort: gen dmyet2=1 if dmnow==1 | (dmnow[_n-1]==1 )
replace dmyet2 = 0 if dmyet2 != 1*/
* baseline covariates
foreach var of varlist age wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works unemplyet dmyet{
by id: gen `var'_bl= `var'[1] 
}

* fixed: rural age_ini female han i.province








// xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works idt wave if idt<=firstdmidt | firstdmidt ==.
egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance works idt wave if idt<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
//xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl idt wave if idt<=firstdmidt==. | firstdmidt ==.

xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl idt wave if idt<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet)
sort id idt
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
rename pdmyet dmyetnum
gen stabweightdmyet =dmyetnum/dmyetdenom



egen temp=min(idt) if unemplyet == 1, by(id)
egen firstunemplidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if unemplyet==1, by(id)
egen firstunemplyear = mean(temp), by(id)
drop temp
gen stabweightunemplyet = .  // generate stabweightunemplyet to later replace with calculated weights in loop
logistic unemplyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl dmyet_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance dmyet i.wave if (wave<=firstunemplidt | firstunemplidt ==.)

predict punemplyet if e(sample) 
replace punemplyet=1 if wave>firstunemplidt 
replace punemplyet=punemplyet*unemplyet + (1-punemplyet)*(1-unemplyet) 
rename punemplyet unemplyetdenom
//xi: logistic unemplyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl dmyet_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
logistic unemplyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl university_bl married_bl  insurance_bl dmyet_bl i.wave if (wave<=firstunemplidt | firstunemplidt ==.)

predict punemplyet if e(sample) 
replace punemplyet=1 if wave>firstunemplidt 
replace punemplyet=punemplyet*unemplyet+(1-punemplyet)*(1-unemplyet) 
bysort id: replace punemplyet=punemplyet*punemplyet[_n-1] if _n!=1   // need to add _mj as another variable to sort by, otherwise it will not work
rename punemplyet unemplyetnum
gen stabweightunemplyet =unemplyetnum/unemplyetdenom 







xi: regress lnindwage dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
xi: logit works dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
xi: regress bmi dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave works_bl [pw=stabweightdmyet], cl(id)
xi: regress waist dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave works_bl[pw=stabweightdmyet], cl(id)
xi: regress d3kcal dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave works_bl[pw=stabweightdmyet], cl(id)
xi: logit  anyalc dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave  works_bl [pw=stabweightdmyet], cl(id)

/* 
drop dmyetdenom dmyetnum stabweightdmyet

