set more off
capture log close
clear matrix
	
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder
/*
use "Data/data_13.dta", clear

log using Logs/ice_long_30.log, replace

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65

drop if province == . | index ==. | rural ==.


replace indwage = 0 if works == 0  //
replace indwage = . if works == .  //

* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3* stillsmoking anyalc indwage works age diabetes_sr dmyet yearsdiagall{
bysort id (wave): gen `var'_bl= `var'[1] 
}

* fixed: rural age_bl female han i.province

drop if dmyet == . & dmyet_bl == .

egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id wave idt dmyet_bl diabetes_sr diabetes_sr_bl age_bl female rural newt han province index_bl bmi_bl waist_bl d3* stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  age insurance_bl indwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works yearsdiagall yearsdiagall_bl

/*reshape wide wave idt dmyet age_bl female rural han province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, i(id) j(newt)*/

ice age* female dmyet* yearsdiagall* rural han i.province bmi* index* waist* d3* stillsmoking* anyalc* secondary* university* married*  insurance* indwage* works* , m(30) saving(ice30_long_duration_nodmyetmiss, replace) seed(49230) conditional(indwage_bl: works_bl==1 \ indwage: works==1 \ yearsdiagall_bl: dmyet_bl ==1 \ yearsdiagall: dmyet == 1) genmiss(miss)
capture log close

*/
// creating dataset for ice including all previous and future information but mainatining data in long format
use "Data/data_13.dta", clear
log using Logs/ice_long_30_all.log, append
drop *_prev*
egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65


replace indwage = 0 if works == 0  //
replace indwage = . if works == .  //

keep id wave idt dmyet diabetes_sr age female rural newt han province  index bmi waist d3kcal d3carbo d3fat d3protn stillsmoking anyalc secondary university married  insurance indwage works yearsdiagall


drop if province == . | index ==. | rural ==.

forvalues i=1/6{
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal d3carbo d3fat d3protn stillsmoking anyalc indwage works age diabetes_sr dmyet yearsdiagall{
bysort id (wave) : gen `var'`i'= `var'[`i'] 
}
*for interval limits of prediction of yearsdiagall and indwage
gen ll`i' = yearsdiagall`i' if yearsdiagall`i' !=.
replace ll`i' = 0 if yearsdiagall`i' == .
gen ul`i' = yearsdiagall`i' if yearsdiagall`i' !=.
replace ul`i' = 54 if yearsdiagall`i' == . 
gen ll_wage`i' = indwage`i' if indwage`i' !=.
replace ll_wage`i' = 0 if indwage`i' ==.
}

gen ll = yearsdiagall if yearsdiagall !=.
replace ll = 0 if yearsdiagall == .
gen ul = yearsdiagall if yearsdiagall !=.
replace ul = 54 if yearsdiagall == . 

gen ll_wage = indwage if indwage !=.
replace ll_wage = 0 if indwage ==.
gen ul_wage =.

drop if dmyet == . & dmyet1 == . & dmyet2 == . & dmyet3 == . & dmyet4 == . & dmyet5 == . & dmyet6 == .  // drops observations with missing diabetes for all waves
 
ice age* female dmyet* ul* ll* rural han i.province index* bmi* waist* d3* stillsmoking* anyalc* secondary* university* married*  insurance* indwage* works* yearsdiagall* wave i.idt, m(30) saving(ice30_all_long_duration_nodmyetmiss, replace) conditional(indwage: works == 1 \ indwage1: works1==1 \ indwage2: works2==1 \ indwage3: works3==1 \ indwage4: works4==1 \ indwage5: works5==1 \ indwage6: works6==1 \ yearsdiagall: dmyet == 1 \ yearsdiagall2: dmyet2==1 \ yearsdiagall3: dmyet3==1 \ yearsdiagall4: dmyet4==1 \ yearsdiagall5: dmyet5==1 \ yearsdiagall6: dmyet6==1) genmiss(miss) interval(yearsdiagall: ll ul, yearsdiagall2: ll2 ul2, yearsdiagall3: ll3 ul3, yearsdiagall4: ll4 ul4, yearsdiagall5: ll5 ul5, yearsdiagall6: ll6 ul6, indwage: ll_wage ul_wage, indwage1: ll_wage1 ul_wage, indwage2: ll_wage2 ul_wage, indwage3: ll_wage3 ul_wage, indwage4: ll_wage4 ul_wage, indwage5: ll_wage5 ul_wage, indwage6: ll_wage6 ul_wage)

exit

capture log close


