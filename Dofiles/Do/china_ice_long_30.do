set more off
capture log close
clear matrix
	
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder
/*
use "Data/data_13.dta", clear

log using Logs/ice_long_30.log, replace

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65

drop if province == . | index ==. | rural ==.


replace indwage = 0 if works == 0  //
replace indwage = . if works == .  //

* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3* stillsmoking anyalc indwage works age diabetes_sr dmyet yearsdiagall{
by id: gen `var'_bl= `var'[1] 
}

* fixed: rural age_bl female han i.province



egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id wave idt dmyet_bl diabetes_sr diabetes_sr_bl age_bl female rural newt han province index_bl bmi_bl waist_bl d3* stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  age insurance_bl indwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works yearsdiagall yearsdiagall_bl

/*reshape wide wave idt dmyet age_bl female rural han province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, i(id) j(newt)*/

ice age* female dmyet* yearsdiagall* rural han i.province bmi* index* waist* d3* stillsmoking* anyalc* secondary* university* married*  insurance* indwage* works* , m(30) saving(ice30_long_duration, replace) seed(49230) conditional(indwage_bl: works_bl==1 \ indwage: works==1 \ yearsdiagall_bl: dmyet_bl ==1 \ yearsdiagall: dmyet == 1) genmiss(miss)
capture log close

*/
// creating dataset for ice including all previous and future information but mainatining data in long format
use "Data/data_13.dta", clear
log using Logs/ice_long_30_all.log, append
drop *_prev*
egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65


replace indwage = 0 if works == 0  //
replace indwage = . if works == .  //

keep id wave idt dmyet diabetes_sr age female rural newt han province  index bmi waist d3kcal d3carbo d3fat d3protn stillsmoking anyalc secondary university married  insurance indwage works yearsdiagall

drop if province == . | index ==. | rural ==.

forvalues i=1/6{
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal d3carbo d3fat d3protn stillsmoking anyalc indwage works age diabetes_sr dmyet yearsdiagall{
by id: gen `var'`i'= `var'[`i'] 
}
}



ice age* female diabetes_sr* dmyet* rural han i.province index* bmi* waist* d3* stillsmoking* anyalc* secondary* university* married*  insurance* indwage* works* yearsdiagall*, m(30) saving(ice30_all_long_duration, replace) conditional(indwage: works == 1 \ indwage1: works1==1 \ indwage2: works2==1 \ indwage3: works3==1 \ indwage4: works4==1 \ indwage5: works5==1 \ indwage6: works6==1 \ yearsdiagall: dmyet == 1 \ yearsdiagall1: dmyet1==1 \ yearsdiagall2: dmyet2==1 \ yearsdiagall3: dmyet3==1 \ yearsdiagall4: dmyet4==1 \ yearsdiagall5: dmyet5==1 \ yearsdiagall6: dmyet6==1) genmiss(miss)

exit

capture log close


