set more off
capture log close
clear matrix
	
global homefolder ""/home/till/Phd/China Data/Data_2014""
*global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data.dta", clear

log using Logs/ice_long_30.log, replace
drop *_prev

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65

drop if province == . | index ==. | rural ==.


replace indwage = 0 if works == 0  //
replace indwage = . if works == .  //

drop if dmyet == .

* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3* MET* retired hypertension systolic diastolic fatperc stillsmoking alc works age diabetes_sr dmyet yearsdiagall hhincpc_cpi{
bysort id (wave): gen `var'_bl= `var'[1] 
}

* fixed: rural age_bl female han i.province



egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id hhid wave idt dmyet_bl dmyet diabetes_sr diabetes_sr_bl age_bl female rural newt han province index_bl bmi_bl waist_bl d3* stillsmoking_bl alc_bl secondary_bl university_bl married_bl  age insurance_bl works_bl index bmi waist d3kcal stillsmoking alc secondary university married  insurance  works yearsdiagall yearsdiagall_bl hhincpc_cpi hhincpc_cpi_bl diastolic hypertension systolic diastolic_bl hypertension_bl systolic_bl MET_PA* retired*  
gen ll = yearsdiagall if yearsdiagall !=.
replace ll = 0 if yearsdiagall == .
gen ul = yearsdiagall if yearsdiagall !=.
replace ul = 54 if yearsdiagall == . 
gen ll_bl = yearsdiagall_bl if yearsdiagall_bl !=.
replace ll_bl = 0 if yearsdiagall_bl == .
gen ul_bl = yearsdiagall_bl if yearsdiagall_bl !=.
replace ul_bl = 54 if yearsdiagall_bl == . 

gen ll_income = hhincpc_cpi if hhincpc_cpi !=.
gen ul_income = hhincpc_cpi if hhincpc_cpi !=.

gen ll_income_bl = hhincpc_cpi_bl if hhincpc_cpi_bl !=.
gen ul_income_bl = hhincpc_cpi_bl if hhincpc_cpi_bl !=.

/*reshape wide wave idt dmyet age_bl female rural han province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl alc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking alc secondary university married  insurance lnindwage works, i(id) j(newt)*/

ice age* female hhid dmyet* ul ll ll_bl ul_bl yearsdiagall* alc* rural han i.province bmi* index* waist* d3* stillsmoking* secondary* university* married*  insurance*  works* i.wave hhinc* diastolic* hypertension* systolic* retired MET_PA*, m(30) saving(ice30_long_duration_nodmyetmiss, replace) seed(49230) conditional(yearsdiagall_bl: dmyet ==1 \ yearsdiagall: dmyet == 1) genmiss(miss) interval(yearsdiagall: ll ul, yearsdiagall_bl: ll_bl ul_bl)
capture log close 

/*
set more off
capture log close
clear matrix
	
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data_13.dta", clear

drop if wave < 1997

log using Logs/ice_long_3.log, replace
drop *_prev

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65

drop if province == . | index ==. | rural ==.


replace indwage = . if works == 0  //
replace indwage = . if works == .  //


drop if dmyet == .

* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3* stillsmoking alc indwage works age diabetes_sr dmyet yearsdiagall hhincpc_cpi lnhhinc_cpi hhexpense_cpi systolic diastolic hypertension{
bysort id (wave): gen `var'_bl= `var'[1] 
}

* fixed: rural age_bl female han i.province


egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id hhid wave dmyet_bl dmyet diabetes_sr diabetes_sr_bl age_bl female rural newt han province index_bl bmi_bl waist_bl d3* stillsmoking_bl alc_bl secondary_bl university_bl married_bl  age insurance_bl indwage_bl works_bl index bmi waist d3kcal stillsmoking alc secondary university married  insurance indwage works yearsdiagall yearsdiagall_bl hhincpc_cpi hhincpc_cpi_bl diastolic_bl hypertension_bl systolic_bl diastolic hypertension systolic
gen ll = yearsdiagall if yearsdiagall !=.
replace ll = 0 if yearsdiagall == .
gen ul = yearsdiagall if yearsdiagall !=.
replace ul = 54 if yearsdiagall == . 
gen ll_bl = yearsdiagall_bl if yearsdiagall_bl !=.
replace ll_bl = 0 if yearsdiagall_bl == .
gen ul_bl = yearsdiagall_bl if yearsdiagall_bl !=.
replace ul_bl = 54 if yearsdiagall_bl == . 

gen ll_wage = indwage if works == 1 & indwage !=.
gen ul_wage = indwage if works == 1 & indwage !=.

gen ll_wage_bl = indwage_bl if works == 1 & indwage_bl !=.
gen ul_wage_bl = indwage_bl if works== 1 & indwage_bl !=.

gen ll_income = hhincpc_cpi if hhincpc_cpi !=.
gen ul_income = hhincpc_cpi if hhincpc_cpi !=.

gen ll_income_bl = hhincpc_cpi_bl if hhincpc_cpi_bl !=.
gen ul_income_bl = hhincpc_cpi_bl if hhincpc_cpi_bl !=.

/*reshape wide wave idt dmyet age_bl female rural han province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, i(id) j(newt)*/

ice age* female hhid dmyet* ul ll ll_bl ul_bl yearsdiagall* alc rural han i.province bmi* index* waist* d3* stillsmoking* secondary* university* married*  insurance* indwage* works* i.wave hhinc* diastolic* hypertension* systolic*, m(3) saving(ice3_long_duration_nodmyetmiss, replace) seed(49230) conditional(indwage_bl: works_bl==1 \ indwage: works==1 \ yearsdiagall_bl: dmyet ==1 \ yearsdiagall: dmyet == 1) genmiss(miss) interval(yearsdiagall: ll ul, yearsdiagall_bl: ll_bl ul_bl)
capture log close 


