set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder
log using Logs/china_dm30.log, replace

//use "ice30_cluster_miss.dta", clear
use "Data/ice30_long", clear
mi import ice, automatic

/*drop if _mj==0

reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works m_age m_indwage, i(id _mj)
drop m_*1 m_*2 m_*3 m_*4 m_*5 m_*6
drop if m_age == 1
 
save "Data/ice30.dta", replace
use "Data/ice30.dta", clear
//reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance indwage works, i(id _mj)
drop if age < 18 | age > 65
drop if m_age == 1
rename _j  wave
dummies education
rename education1 secondary
rename education2 university
*/

mi passive: gen lnindwage = log(indwage)
mi update

mi register reg dmyet age han female

by id _mi_m (wave), sort: gen everunempl = 1 if works == 0 & works[_n-1] == 1
by id _mi_m (wave), sort: replace everunempl = 1 if works == 0 & works[_n-1] == 0
by id _mi_m (wave), sort: replace everunempl = 1 if everunempl[_n-1] == 1 & everunempl[_n+1] > 0
by id _mi_m (wave), sort: replace everunempl = 0 if everunempl == . & works !=.



// employment status at last obsevred wave
mi passive: by id (wave): gen lastworks = works[_N]
mi update
/*
foreach var of varlist age wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works dmyet lastworks{
bysort id _mj: gen `var'_bl= `var'[1]
}
*/

gen age_sq = age^2

//drop if _mj==0
sort id _mi_m wave
egen temp=min(wave) if dmyet == 1, by(id _mi_m)
egen firstdmidt=mean(temp), by(id _mi_m)
drop temp

by id (wave), sort: gen dmyet_bl=dmyet[1]
mi update
drop if dmyet_bl ==1
mi update
mi passive: by id (wave): gen lnindwage_bl=lnindwage[1]
mi update
/*CREATING STABILIZED WEIGHTS*/

/*With dmyet as explanatory variable*/
mi estimate, saving(dmyetden_sav, replace) esample(denom): logistic dmyet age_bl female rural han i.province index_bl  waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl age  insurance_bl works_bl index waist d3kcal stillsmoking anyalc secondary university married  insurance works i.wave if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet age_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt 
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: bysort id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
rename pdmyet dmyetnum
gen stabweightdmyet =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom
save "Data/ice30_weighted", replace

/*With dmyet as explanatory variable for wage equation*/
/* Not working
mi estimate, saving(dmyetden_sav, replace) esample(denom): logistic dmyet age_bl age female rural han i.province index_bl  waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index waist d3kcal stillsmoking anyalc secondary university married  insurance i.wave lnindwage* if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetden_sav, esample(denom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
mi estimate, saving(dmyetnom_sav, replace) esample(nom): logistic dmyet age_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl lnindwage_bl if wave<=firstdmidt | firstdmidt ==.

mi predict pdmyet using dmyetnom_sav, esample(nom) storecompleted
replace pdmyet=invlogit(pdmyet)
replace pdmyet=1 if wave>firstdmidt 
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) 
mi xeq: bysort id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1   
rename pdmyet dmyetnum
gen stabweightdmyet_wage =dmyetnum/dmyetdenom
drop dmyetdenom dmyetnum denom nom

save "Data/ice30_weighted", replace
*/
/*With lastworks as explanatory variable and dmyet as dependant variable*/

mi estimate, saving(worksden_sav, replace) esample(denom): logistic works age_bl age female dmyet* rural han i.province index_bl  waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index waist d3kcal stillsmoking anyalc secondary university married  insurance works_bl i.wave if wave<=firstdmidt | firstdmidt ==.

mi predict pworks using worksden_sav, esample(denom) storecompleted
replace pworks=invlogit(pworks)
replace pworks=1 if wave>firstdmidt
replace pworks=pworks*works + (1-pworks)*(1-works)
rename pworks worksdenom
// logistic works age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
mi estimate, saving(worksnom_sav, replace) esample(nom): logistic works dmyet_bl age_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl if wave<=firstdmidt | firstdmidt ==.

mi predict pworks using worksnom_sav, esample(nom) storecompleted
replace pworks=invlogit(pworks)
replace pworks=1 if wave>firstdmidt 
replace pworks=pworks*works+(1-pworks)*(1-works) 
mi xeq: bysort id: replace pworks=pworks*pworks[_n-1] if _n!=1   
rename pworks worksnum
gen stabweightworks =worksnum/worksdenom
drop worksdenom worksnum denom nom
save "Data/ice30_weighted", replace

/*With log individual wage as explanatory variable*/

mi estimate, saving(lnindwageden_sav, replace) esample(denom): logistic lnindwage dmyet* age_bl age female rural han i.province index_bl  waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index waist d3kcal stillsmoking anyalc secondary university married  insurance works i.wave if wave<=firstdmidt | firstdmidt ==.

mi predict plnindwage using lnindwageden_sav, esample(denom) storecompleted
replace plnindwage=invlogit(plnindwage)
replace plnindwage=1 if wave>firstdmidt
replace plnindwage=plnindwage*lnindwage + (1-plnindwage)*(1-lnindwage)
rename plnindwage lnindwagedenom
// logistic lnindwage age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
mi estimate, saving(lnindwagenom_sav, replace) esample(nom): logistic lnindwage dmyet_bl age_bl female rural han i.province index_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl if wave<=firstdmidt | firstdmidt ==.

mi predict plnindwage using lnindwagenom_sav, esample(nom) storecompleted
replace plnindwage=invlogit(plnindwage)
replace plnindwage=1 if wave>firstdmidt 
replace plnindwage=plnindwage*lnindwage+(1-plnindwage)*(1-lnindwage) 
mi xeq: bysort id: replace plnindwage=plnindwage*plnindwage[_n-1] if _n!=1   
rename plnindwage lnindwagenum
gen stabweightlnindwage =lnindwagenum/lnindwagedenom
drop lnindwagedenom lnindwagenum denom nom

save "Data/ice30_weighted", replace

use "Data/ice30_weighted", clear

/*truncate weight to 1 and 99th percentile*/
foreach var of varlist stabweight*{
_pctile `var', nq(100) 
gen `var'_tr = `var'
replace `var'_tr = r(r99) if `var' > r(r99) & `var' <.
replace `var'_tr = r(r1) if `var' < r(r1)
}


// have created two globals with controls for regression models. First for works and wage models and the second for all other models

global x_prod "age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave"
global x_other "age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl"


mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl [pw=stabweightdmyet_tr] if works == 1, cl(id)
mi estimate: logit lastworks dmyet $x_prod works_bl [pw=stabweightdmyet_tr], cl(id)
mimrgns, dydx(dmyet) predict(pr)
mi estimate: regress bmi dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: regress waist dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: regress d3kcal dmyet $x_other [pw=stabweightdmyet_tr], cl(id)
mi estimate: logistic  anyalc dmyet $x_other [pw=stabweightdmyet_tr], cl(id)

mi estimate: regress lnindwage dmyet $x_prod lnindwage_bl  if works == 1 & stabweightdmyet_tr<., cl(id)
mi estimate: logit lastworks dmyet $x_prod works_bl if stabweightdmyet_tr<., cl(id)
mimrgns, dydx(dmyet) predict(pr)
mi estimate: regress bmi dmyet $x_other if stabweightdmyet_tr<., cl(id)
mi estimate: regress waist dmyet $x_other if stabweightdmyet_tr<., cl(id)
mi estimate: regress d3kcal dmyet $x_other if stabweightdmyet_tr<., cl(id)
mi estimate: logistic  anyalc dmyet $x_other if stabweightdmyet_tr<., cl(id)


mi estimate: logit dmyet $x_prod [pw=stabweightworks_tr], cl(id)
mimrgns, dydx(dmyet) predict(pr)

// this does not make sense as lnindwage changes over time
mi estimate: logit dmyet lnindwage $x_other [pw=stabweightworks_tr], cl(id)
mimrgns, dydx(dmyet) predict(pr)


/*

  regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works == 1, cl(id)
  logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
  regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
  logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)


  regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works == 1, cl(id)
  logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
  regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
  logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)

* duration=1 for first wave diabetes is reported
replace duration=1 if wave==firstdmyear

  regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works == 1, cl(id)
  logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
  regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
  regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
  logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)

* unweighted  ADD TOME VARIANT VARIABLES HERE
  regress lnindwage dmyet $x_prod if works == 1, cl(id)
  logistic works dmyet $x_prod, cl(id)
  reg works dmyet $x_prod, cl(id)
  regress bmi dmyet $x_other, cl(id)
  regress waist dmyet $x_other, cl(id)
  regress d3kcal dmyet $x_other, cl(id)
  logistic  anyalc dmyet $x_other, cl(id)

