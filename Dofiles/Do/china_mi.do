set more off
capture log close
clear matrix
*ssc install ice
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Data/data_13.dta", clear

log using Logs/mi_cluster.log, replace

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65
drop if province == .


gen dmnow= diabetes_sr==1 


bysort id idt: gen dmyet=1 if dmnow==1 | (dmnow[_n-1]==1 )
replace dmyet = 0 if dmyet != 1
/* baseline covariates  // don't need them for multiple imputation in wide format because it automatically takes baseline values into account
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works{
by id: gen `var'_bl= `var'[1] 
}
*/
* fixed: rural age_ini female han i.province



egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id dmyet age female rural newt han province  index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works 

//rename provinc* provinc*r
reshape wide dmyet age index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, i(id) j(newt)

xtset, clear

mi set wide

mi register imputed   index* bmi* waist* d3kcal* stillsmoking* anyalc* secondary* university* married* insurance* lnindwage* works* 
mi register regular female age* han province rural dmyet*
mi describe

//set seed 49230
mi impute mvn  index* bmi* waist* d3kcal* stillsmoking* anyalc* secondary* university* married* insurance*  works* = female age* han province rural dmyet*, add(10) force

ice female age* han dmyet* rural* province index* bmi* waist* d3kcal* stillsmoking* anyalc* secondary* university* married* insurance* lnindwage* works*,  m(30) saving(ice30_cluster, replace) seed(49230)

exit

capture log close


/*
ice dmyet age_ini female rural han m.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, m(10) saving(ice10, replace)

