set more off
capture log close
clear matrix
/*
// preparational do files
global homefolder ""/home/till/Dropbox/PhD/Data/China Data""

        do "/home/till/Dropbox/PhD/Data/China Data/merging.do"	//master china files
	do "/home/till/Dropbox/PhD/Data/China Data/variablecreation.do" //creating new variables
	
	*/
	



global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder

use "Stata/data.dta", clear

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65


gen dmnow= diabetes_sr==1 


bysort id idt: gen dmyet=1 if dmnow==1 | (dmnow[_n-1]==1 )
replace dmyet = 0 if dmyet != 1
* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works{
by id: gen `var'_bl= `var'[1] 
}

* 

egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

gen duration=wave-firstdmyear if dmyet==1
replace duration = 0 if duration ==.


// xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works idt wave if idt<=firstdmidt | firstdmidt ==.
xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance works idt wave if idt<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
//xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl idt wave if idt<=firstdmidt==. | firstdmidt ==.
xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl idt wave if idt<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet)
sort id idt
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
rename pdmyet dmyetnum
gen stabweightdmyet =dmyetnum/dmyetdenom

global x_prod "age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave"
global x_other "age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave works_bl works"


xi: regress lnindwage dmyet $x_prod [pw=stabweightdmyet], cl(id)
xi: logistic works dmyet $x_prod wave [pw=stabweightdmyet], cl(id)
xi: regress bmi dmyet $x_other [pw=stabweightdmyet], cl(id)
xi: regress waist dmyet $x_other [pw=stabweightdmyet], cl(id)
xi: regress d3kcal dmyet $x_other [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc dmyet $x_other [pw=stabweightdmyet], cl(id)


xi: regress lnindwage duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: logistic works duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: regress bmi duration $x_other [pw=stabweightdmyet], cl(id)
xi: regress waist duration $x_other [pw=stabweightdmyet], cl(id)
xi: regress d3kcal duration $x_other [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc duration $x_other [pw=stabweightdmyet], cl(id)

xi: regress lnindwage i.duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: logistic works i.duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)




/* 
drop dmyetdenom dmyetnum stabweightdmyet

