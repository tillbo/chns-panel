set more off
capture log close
clear matrix
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder ""/home/till/Phd/China Data/Data_2014_2""

cd $homefolder

/*RE ESTIMATES*/




log using Logs/re_binary_mi_results_xtreg_hausman.log, replace
***************************************
/*+++++Binary diabetes indicator+++++*/
***************************************

use "Data/data_imputed3", clear

drop if dmyet_bl == 1
drop if maxseq == 1
drop if retired == 1

global x "dmyet age_sq wave2-wave6 han rural married secondary university insurance index provinc2 provinc3 provinc5 provinc6 provinc7 provinc9 hhincpc_cpi"


//eststo mi_works_re_ia: mi est, post cmdok esampvaryok : xtreg works $x c.c__dmyet#i.female c.m__dmyet#i.female female, re ro  

/*BY GENDER*/

forvalues i =0/1{
display `i'
foreach var of varlist works smoke alc bmi waist d3kcal obese overweight MET_PA{
eststo mi_`var'_fe`i': mi est, post cmdok esampvaryok : xtreg `var' $x if female == `i', fe ro 
est sto fe 
eststo mi_`var'_re`i': mi est, post cmdok esampvaryok : xtreg `var' age $x if female == `i', re ro  
est sto re
*mi est, post cmdok esampvaryok : rhausman re fe, cluster 
*estadd r(chi2)
*estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster subset(dmyet)
estadd r(chi2)
estadd r(p)
}
}



forvalues i =0/1{
display `i'

eststo mi_smoking_fe`i': mi est, post cmdok esampvaryok : xtreg smoke $x works if female == `i', fe ro 
est sto fe 
eststo mi_smoking_re`i': mi est, post cmdok esampvaryok : xtreg smoke $x works if female == `i', re ro 
est sto re 
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2)
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2) subset(dmyet)

eststo mi_alc_fe`i': mi est, post cmdok esampvaryok : xtreg alc $x works if female == `i', fe ro
est sto fe
eststo mi_alc_re`i': mi est, post cmdok esampvaryok : xtreg alc $x works if female == `i', re ro 
est sto re 
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2)
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2) subset(dmyet) 

eststo mi_bmi_fe`i': mi est, post cmdok esampvaryok: xtreg bmi $x works if female == `i', fe ro 
est sto fe
eststo mi_bmi_re`i': mi est, post cmdok esampvaryok: xtreg bmi $x works if female == `i', re ro 
est sto re
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2)
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2) subset(dmyet) 


eststo mi_waist_fe`i': mi est, post cmdok esampvaryok: xtreg waist $x works if female == `i', fe ro 
est sto fe
eststo mi_waist_re`i': mi est, post cmdok esampvaryok: xtreg waist $x works if female == `i', re ro 
est sto re
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2)
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2) subset(dmyet) 


eststo mi_kcal_fe`i': mi est, post cmdok esampvaryok: xtreg d3kcal $x works if female == `i', fe 
est sto fe
eststo mi_kcal_re`i': mi est, post cmdok esampvaryok: xtreg d3kcal $x works if female == `i', re 
est sto re

estadd r(chi2)
estadd r(p)
mi est, cmdok esampvaryok : rhausman re fe, cluster reps(2) subset(dmyet) 


eststo mi_obese_fe`i': mi est, post cmdok esampvaryok: xtreg obese $x works if female == `i', fe ro 
est sto fe
eststo mi_obese_re`i': mi est, post cmdok esampvaryok: xtreg obese $x works if female == `i', re ro 
est sto re
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2)
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2) subset(dmyet) 


eststo mi_overweight_fe`i': mi est, post cmdok esampvaryok: xtreg overweight $x works if female == `i', fe ro 
est sto fe
eststo mi_overweight_re`i': mi est, post cmdok esampvaryok: xtreg overweight $x works if female == `i', re ro 
est sto re
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2)
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster rep(2) subset(dmyet) 
}

estwrite mi_* using "savedestimates/diabetes_dummy_mi_re_xtreg_hausman", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_re_xtreg_hausman"


/*TABLES BINARY FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs nolz  ///
	 alignment(S S)   collabels(none) keep(dmyet) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")

	forvalues i=0/1{
	esttab mi_works_re`i' mi_smoking_re`i'  mi_alc_re`i' mi_bmi_re`i' mi_waist_re`i' mi_kcal_re`i' using "Tables/RE/RE_estimates_mi_xtreg`i'.tex", replace comp ///
	 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
	       }
	
		*Obesity and overweight	
	
	esttab  mi_overweight_re0 mi_obese_re0 mi_overweight_re1 mi_obese_re1 using "Tables/RE/RE_estimates_mi_xtreg_obese.tex", replace comp ///
	 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))
	            



	      
*Word
  global table_reduced b(%9.3f) se(%9.3f)  nolz noobs  ///
	 collabels(none) keep(dmyet) label nobaselevels ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_re`i' mi_smoking_re`i'  mi_alc_re`i' mi_bmi_re`i' mi_waist_re`i' mi_kcal_re`i' using "Tables/RE/RE_estimates_mi_xtreg`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}


estwrite mi_* using "savedestimates/diabetes_dummy_mi_re_xtreg_hausman", id() replace
estread mi_* using "savedestimates/diabetes_dummy_mi_re_xtreg_hausman"


capture log close


****************************************
/*++++++++++++++++TRENDS++++++++++++++*/
****************************************

use "Data/data_imputed3", clear

drop if dmyet_bl == 1
drop if maxseq == 1
drop if retired == 1
global x "yearsdiagall age_sq  han  rural married secondary university insurance index ib43.province hhincpc_cpi"

label var yearsdiagall "Years since diagnosis"


log using Logs/re_duration_mi_results_xtreg_hausman.log, replace

/*BY GENDER*/

forvalues i =0/1{
display `i'
foreach var of varlist works smoke alc bmi waist d3kcal obese overweight MET_PA{
eststo mi_`var'_re`i': mi est, post cmdok esampvaryok : xtreg `var' $x if female == `i', re ro  
}
}

forvalues i =0/1{
display `i'
eststo mi_works_re_dur`i': mi est, post cmdok esampvaryok : xtreg works $x  if female == `i', re ro  
est sto re
eststo mi_works_fe_dur`i': mi est, post cmdok esampvaryok : xtreg works $x  if female == `i', fe ro  
est sto fe 
mi est, post cmdok esampvaryok : rhausman re fe, cluster
estadd r(chi2)
estadd r(p)
mi est, post cmdok esampvaryok : rhausman re fe, cluster subset(yearsdiagall)
estadd r(chi2)
estadd r(p)

eststo mi_smoking_re_dur`i': mi est, post cmdok esampvaryok : xtreg smoke $x works if female == `i', re ro  
eststo mi_alc_re_dur`i': mi est, post cmdok esampvaryok : xtreg alc $x works if female == `i', re ro  
eststo mi_obese_re_dur`i': mi est, post cmdok esampvaryok: xtreg obese $x works if female == `i', re ro 
eststo mi_overweight_re_dur`i': mi est, post cmdok esampvaryok: xtreg overweight $x works if female == `i', re ro 
eststo mi_obesec_`i'_re_dur`i': mi est, post cmdok esampvaryok: xtreg obesec_`i' $x works if female == `i', re ro 

}

forvalues i =0/1{
eststo mi_bmi_re_dur`i': mi est, post cmdok esampvaryok: xtreg bmi $x works if female == `i', re ro 
eststo mi_waist_re_dur`i': mi est, post cmdok esampvaryok: xtreg waist $x works if female == `i', re ro 
eststo mi_kcal_re_dur`i': mi est, post cmdok esampvaryok: xtreg d3kcal $x works if female == `i', re ro 

}

estwrite mi_*_dur* using "savedestimates/diabetes_duration_mi_re_xtreg", id() replace
estread mi_*_dur*  using "savedestimates/diabetes_duration_mi_re_xtreg"

/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION*/

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs nolz   ///
	 alignment(S S)   collabels(none) keep(yearsdiagall) label nobaselevels ///
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")



forvalues i=0/1{
esttab mi_works_re_dur`i' mi_smoking_re_dur`i'  mi_alc_re_dur`i' mi_bmi_re_dur`i' mi_waist_re_dur`i' mi_kcal_re_dur`i' using "Tables/RE/RE_estimates_mi_xtreg_duration`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} fragment
       }
   
  *Obesity and Overweight     
   esttab  mi_overweight_re_dur0 mi_obese_re_dur0 mi_overweight_re_dur1 mi_obese_re_dur1 using "Tables/RE/RE_estimates_mi_xtreg_obese_dur.tex", replace comp ///
	 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))

           
*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell eform(1 1 1 0 0 0) ///
	 collabels(none) keep(yearsdiagall) label  nobaselevels /// 
	 addnote("Other control variables: age, age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")



forvalues i=0/1{
esttab mi_works_re_dur`i' mi_smoking_re_dur`i'  mi_alc_re_dur`i' mi_bmi_re_dur`i' mi_waist_re_dur`i' mi_kcal_re_dur`i' using "Tables/RE/RE_estimates_mi_xtreg_duration`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close




***************************************
/*++++++++++DURATION GROUPS++++++++++*/
***************************************
global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
capture log close

use "Data/data_imputed30", clear
drop if dmyet_bl == 1
drop if maxseq == 1

log using Logs/re_durationgroups_mi_xtreg_hausman.log, append

global x "i.yearsdiagall_g age_sq  i.wave i.han ib(freq).province rural married secondary university insurance index hhincpc_cpi"



/*BY GENDER*/

	* normally use loops, but here need to estimate them seperatley as for female smoking regression gives error because ommits different groups between datasets, i.e. ommits > 20 years duration in some due to lack of data. Thefore exclude this group in female estimation, but not in male
eststo mi_works_re_g0: mi est, post esampvaryok errorok: xtreg works $x if female == 0, re ro 
eststo mi_smoking_re_g0: mi est, post esampvaryok errorok: xtreg smoke $x  works if female == 0, re ro 
eststo mi_alc_re_g0: mi est, post esampvaryok errorok: xtreg alc $x i.province works if female == 0, re ro  

eststo mi_works_re_g1: mi est, post cmdok esampvaryok errorok: xtreg works $x if female == 1, re ro 
eststo mi_smoking_re_g1: mi est, post cmdok esampvaryok : xtreg smoke $x  works if female == 1, re ro 
eststo mi_alc_re_g1: mi est, post cmdok esampvaryok errorok: xtreg alc $x i.province works if female == 1, re ro  

label var bmi "BMI" 
label var waist "Waist circumference (cm)" 
label var works "Employment status"
label var alc "Any alcohol consumption"
label var smoke "Smoking status"
label var obese "Obese"
label var overweight "Overweight"
label var d3kcal "Kcal"




	*Using xtreg and later mimargns command to compare marginal effects to xtreg results

forvalues i =0/1{
eststo mi_obese_re_g`i': mi est, post cmdok esampvaryok: xtreg obese $x works if female == `i', re ro 
eststo mi_overweight_re_g`i': mi est, post cmdok esampvaryok: xtreg overweight $x works if female == `i', re ro 
eststo mi_bmi_re_g`i': mi est, post cmdok esampvaryok : xtreg bmi $x works if female == `i', re ro 
eststo mi_waist_re_g`i': mi est, post cmdok esampvaryok errorok: xtreg waist $x works if female == `i', re ro 
eststo mi_kcal_re_g`i': mi est, post cmdok esampvaryok errorok: xtreg d3kcal $x works if female == `i', re ro 
}

estwrite mi_*_g* using "savedestimates/diabetes_durationgroups_mi_re_xtreg_hausman", id() replace
estread mi_*_g* using "savedestimates/diabetes_durationgroups_mi_re_xtreg_hausman"

*PLOTTING ALL DURATION GROUP RESULTS*
coefplot mi_works_re_g0 mi_works_re_g1 , bylabel("Employed")   || mi_smoking_re_g0 mi_smoking_re_g1, bylabel("Smoking")|| ///
 mi_alc_re_g0 mi_alc_re_g1, bylabel("Alcohol") || ///
(mi_bmi_re_g0, label(men) lpattern(solid)) (mi_bmi_re_g1, label(women) lpattern(shortdash)) , bylabel("BMI") || ///
mi_waist_re_g0 mi_waist_re_g1,  bylabel("Waist (cm)") || ///
mi_kcal_re_g0 mi_kcal_re_g1, bylabel("Kcal") ||, ///
keep(*.yearsdiagall_g) yline(0) recast(line)  byopts(yrescale)  ylabel(#6)  vertical fcolor(none) lwidth(thin) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_re.eps, replace 

coefplot  (mi_overweight_re_g0, label(overweight) lpattern(solid)) (mi_obese_re_g0, label(obese) lpattern(shortdash)), bylabel("Males") || ///
mi_overweight_re_g1 mi_obese_re_g1 , bylabel("Females")   ||, ///
keep(*.yearsdiagall_g) yline(0) recast(line)  byopts(yrescale)  ylabel(#15)  vertical fcolor(none) lwidth(small) ciopts( lwidth(thin)) ///
name(xtreg_all, replace) ylabel(,  angle(horizontal) nogrid) xlabel(,  angle(vertical)) scheme(s1mono) ytitle("Marginal effect") legend(off) xtitle("Years after diagnosis") xsize(8)
graph export mi_obese_re.eps, replace 






/*TABLES DURATION FIXED EFFECTS ESTIMATES FOR MULTIPLE IMPUTATION OF DIABETES DURATION GROUPS*/

  global table_reduced b(%9.3f) se(%9.3f)  booktabs nolz star(* 0.10 ** 0.05 *** 0.01) ///
	 alignment(S S)   collabels(none) keep(*yearsdiagall_g) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_re_g`i' mi_smoking_re_g`i'  mi_alc_re_g`i' mi_bmi_re_g`i' mi_waist_re_g`i' mi_kcal_re_g`i' using "Tables/RE/RE_estimates_mi_xtreg_duration_g`i'.tex", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced}
       }
       
         *Obesity and Overweight     
   esttab  mi_overweight_re_g0 mi_obese_re_g0 mi_overweight_re_g1 mi_obese_re_g1 using "Tables/RE/RE_estimates_mi_xtreg_obese_dur_g.tex", replace comp ///
	 mti("Overweight" "Obese" "Overweight" "Obese") ${table_reduced} mgroups("Males" "Females", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span}))

*Word
  global table_reduced b(%9.3f) ci(%9.3f)  nolz nostar noobs wide onecell  eform(1 1 1 0 0 0) mgroups("Odds ratios" "Beta coefficients", pattern(1 0 0 1 0 0)) ///
	 collabels(none) label  nobaselevels ///
	 addnote("Other control variables: age squared, region, urban, education, han, marital status, urbanicity index, time dummies, health insurance status, per capita household income")


forvalues i=0/1{
esttab mi_works_re_g`i' mi_smoking_re_g`i'  mi_alc_re_g`i' mi_bmi_re_g`i' mi_waist_re_g`i' mi_kcal_re_g`i' using "Tables/RE/RE_estimates_mi_xtreg_duration_g`i'.rtf", replace comp ///
 mti("Employment" "Smoking" "Any alcohol" "BMI" "Waist (cm)" "Calories (kcal)") ${table_reduced} 
}

capture log close



