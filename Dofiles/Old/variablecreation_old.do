
set more off
capture log close
clear matrix
/*
// preparational do files
global homefolder ""/home/till/Dropbox/PhD/Data/China Data""

        do "/home/till/Dropbox/PhD/Data/China Data/merging.do"	//master china files
	do "/home/till/Dropbox/PhD/Data/China Data/variablecreation.do" //creating new variables
	
	*/
	
// renaming and creating variables


global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder

use "Data/merged_data.dta", clear

rename IDind id


// generate time variable with regular time gaps to later create lagged values
recode wave (1989=1) (1991=2) (1993=3) (1997=4) (2000=5) (2004=6) (2006=7) (2009=8) (2011=9), gen(idt)

// set panel variables
xtset id idt

 // generate new variables
 
 /*demographics*/
 
 /* Province in variable T1
 21 Liaoning 23 Heilongjiang 32 Jiangsu 37 Shandong
 42 Hubei 43 Hunan 45 Guangxi 52 Guizhou 41 Henan */
 
label define province 11 "Beijing" 21 "Liaoning" 23 "Heilongjiang" 31 "Shanghai" 32 "Jiangsu" ///
   37 "Shandong" 42 "Hubei" 43 "Hunan" 45 "Guangxi" 52 "Guizhou" 41 "Henan" 55 "Chongqing"

label values T1 province

clonevar province = T1  // create variable called province based on T1
 
// create WAVE dummies out of WAVE variable
dummies wave

// create province dummies
dummies province 

// nationality
gen han = 0 if nationality != 1
replace han = 1 if nationality == 1
label define han 1 "Han" 0 "Other"
label value han han
label var han "is respondent of han nationality"
 
// urban or rural
gen rural = 0 if T2 == 1 // urban  

replace  rural = 1 if T2 == 2 // rural

label define rural 0 "urban" 1 "rural"

label values rural rural

// age 
replace age = round(age,1)
replace age = . if age <0 // rounding age 
gen age_sq = age^2



by id: gen age_ini = age[1]
label var age_ini "Age when first entered survey"


// gender
gen male = 0

replace male = 1 if gender == 1

gen female = 0

replace female = 1 if gender == 2

// marital status
gen married = 0 if A8 <.

replace married = 1 if A8 == 2


/*education -- highest education level attained*/
gen primary = 0 if A12 != 1 & A12 <.

replace primary = 1 if A12 == 1

gen secondary = 0 if A12 != 2 & A12 != 3 & A12 != 4  & A12 <.

replace secondary = 1 if A12 == 2 | A12 == 3 | A12 == 4  

gen university = 0 if A12 <=4 | A12 > 6 & A12 <.

replace university = 1 if A12 > 4 & A12 <=6

gen school_curr = 0  // if currently in school

replace school_curr = 1 if A13 == 1

/*employment -- if presently working*/
gen works = B2 if B2 > -9


/* health treatment expenditures variable over last 4 weeks*/
gen healthexp = M30 if M30 > -1 & M30 < 99999 

/* medical insurance yes / no */
 
clonevar insurance = M1 if M1 <9

/* log income adjusted fro inflation to 2006-- from pre-constructed income variable from below mentioned income sources:
INDBUSNyyyy - business income
INDFARMyyyy - farming income
INDFISHyyyy - fishing income
INDGARDyyyy - gardening income
INDLVSTyyyy - livestock income
INDRETIREyyyy - retirement income
INDWAGEyyyy - non-retirement wages */
 
gen lnindinc_cpi = log(indinc_cpi)  // individual income

gen lnhhinc_cpi = log(hhinc_cpi)  // total household income
 
gen lnindwage = log(indwage)  // non-retirement wages


/*Diabetes variables*/

clonevar diabetes_sr = U24A if U24A <3  // self-reported diabetes diagnosis
label var diabetes_sr "Self-reported diabetes diagnosis"

clonevar diabetes_age = U24B if U24B >=0  // self-reported age at diagnosis
label var diabetes_age "Self-reported age at diabetes diagnosis"

clonevar treat_insulin = U24F if U24F <3
label var treat_insulin "Currently diabetes: taking insulin"
replace treat_insulin = 0 if diabetes_sr == 0

clonevar treat_oral = U24E if U24E <3 & treat_insulin == 0
label var treat_oral "Currently diabetes: taking oral medication"
replace treat_oral = 0 if diabetes_sr == 0

gen treat_med = 1 if treat_oral == 1 | treat_insulin == 1  // take medicine
replace treat_med = 0 if treat_oral == 0 & treat_insulin == 0 // take no medicine for diabetes treatment
label var treat_med "Currently diabetes: taking oral medication or insulin"


gen treat_nomed = 0 if treat_med == 1 | diabetes_sr == 0  // take medicine
replace treat_nomed = 1 if treat_oral == 0 & treat_insulin == 0 & diabetes_sr == 1 // take no medicine for diabetes treatment
label var treat_nomed "Currently diabetes: not taking medication"

// creat categorical variable of medical treatment instead of dummies
gen diab_med = 0 if diabetes_sr == 0
replace diab_med = 1 if treat_nomed == 1
replace diab_med = 2 if treat_med == 1

/*First occurence of diabetes diagnosis*/
by id, sort: egen firsttime = min(cond(diabetes_sr == 1, wave, .))
gen byte first = wave == firsttime
label define first 1 "newly diagnosed"
label value first first
label var first "first time diabetes reported"


/*Find those that have newly entered the survey and report diabetes or report diabetes in 1997
to then use the information of age at diagnosis to construct dummies for when was first diagnosed*/

bysort id: egen temp1 = mean(diabetes_age)
gen diabetes_age_mean = round(temp1)

bysort id: egen firstwave = min(wave)  // which wave is first wave
bysort id: egen temp2 = min(wave) if diabetes_sr == 1  // wave where diagnosis was first reported
bysort id: egen yeardx = mean(temp2) // wave where diagnosis was first reported

gen year_diagnosis_reported = wave - (age - diabetes_age_mean)  // year diagnosis was reported
replace yeardx = year_diagnosis_reported if firstwave == yeardx  // replace those that report diabetes diagnosis in their first wave with the year they were diagnosed

gen yearssincedx = wave - yeardx  // years since diagnosis

//gen yearssincedx_uncorr = wave - yeardx // without correction for those newly in wave
/*Treatment at first diagnosis*/

gen diag_oral = 0 if diabetes_sr == 1
replace diag_oral = 1 if first == 1 & treat_oral == 1
label var diag_oral "Newly diagnosed with oral medication"
gen diag_insulin = 0 if diabetes_sr == 1
replace diag_insulin = 1 if first == 1 & treat_insulin == 1
label var diag_insulin "Newly diagnosed with insulin"
gen diag_nomed = 0 if diabetes_sr == 1
replace diag_nomed = 1 if first == 1 & treat_oral == 0 & treat_insulin == 0
label var diag_nomed "Newly diagnosed without medication"
gen diag_med = 0 if diabetes_sr == 1
replace diag_med = 1 if first == 1 & (treat_oral == 1 | treat_insulin == 1)
label var diag_med "Newly diagnosed with either oral medication or insulin"

/*Extend first treatment to other waves by id.
 Doing this we assume that even if persons reported no diabetes after they reported a diagnosis before, that they still have diabetes.
 I.e., we replace */
bysort id: egen temp_med = max(diag_med)
bysort id: egen temp_nomed = max(diag_nomed)
bysort id: egen temp_diab = max(diabetes_sr)

/*Multiply above treatment by years since diagnosis to get years since diagnosis with specific treatment for every wave*/
gen yearsdiagmed = temp_med * yearssincedx  // years since first diagnosis with medication treatment
gen yearsdiagnomed = temp_nomed * yearssincedx  // years since first diagnosis without medication treatment
gen yearsdiagall = temp_diab * yearssincedx  // years since first diagnosis without medication treatment


// need to check again the creation of the years since diagnosis variable to be sure that I use correct values
foreach var of varlist yearsdiagmed yearsdiagnomed yearsdiagall{  // recode the variables to get groups
recode `var' (1 2 = 2) (3 4 = 3) (5 6 = 4) (7 8 = 5) (9/14 = 6) (15/. = 0) (-1000/-1 = 0)
label define `var' 0 "No" 1 "0" 2 "1-2" 3 "3-4" 4 "5-6" 5 "7-8" 6 "9-14"
label value `var' `var'
label var `var' "Years since diagnosis"
}
replace yearsdiagmed = 1 if temp_med == 1 & yearsdiagmed == 0 & diabetes_sr == 1   
replace yearsdiagnomed = 1 if temp_nomed == 1 & yearsdiagnomed == 0 & diabetes_sr == 1 
replace yearsdiagall = 1 if temp_diab == 1 & yearsdiagall == 0 & diabetes_sr == 1 


/*Years since diagnosis based on self-reported year of diagnosis*/
bysort id: egen diabetes_age_min=min(diabetes_age) // find smallest reported year of diagnosis for those that report different ages at different waves
gen diabetes_duration = age - diabetes_age_min  // create years with diabetes
replace diabetes_duration = 0 if diabetes_age_min ==. | diabetes_duration <0  //replace with 0 if age of diagnosis is missing or diabetes duration smaller than 0
gen duration_sq = diabetes_duration^2

/*Other diseases*/

clonevar hypertension =  U22 if U22 <9

clonevar infarct = U24J if U24J <9

clonevar stroke = U24L if U24L <9

clonevar cancer = U24W if U24W <9

/*self-rated health*/

clonevar health_status = U48A if U48A <9

label define health_status 1 "Excellent" 2 "Good" 3 "Fair" 4 "Poor"

label values health_status health_status

dummies health_status

/*Biomarkers*/

egen systolic = rowmean(SYSTOL1 SYSTOL2 SYSTOL3)
egen diastolic = rowmean(DIASTOL1 DIASTOL2 DIASTOL3)

clonevar hba1c = Y50  // Hba1c

clonevar glucose_mg = GLUCOSE_MG   // glucose in mg

gen diab_hba1c = 0 if hba1c <.
replace diab_hba1c = 1 if hba1c <. & hba1c > 6.4

gen diab_ud = 0 if hba1c <.
replace diab_ud = 1 if hba1c <. & hba1c > 6.4 & diabetes_sr == 0
/* bmi and other bodytype measures*/

gen bmi = weight/((height/100)^2)

replace bmi =. if bmi > 100 | bmi <10 // cut-off extreme bmi values

clonevar waist = U10

/*Diabetes risk*/
gen diab_risk = 0 if hba1c <.
replace diab_risk = 1 if hba1c >= 5.7 & hba1c <.


/*bmi official cut-offs*/
gen obese = 0 if bmi <.
replace obese = 1 if bmi >=30 & bmi <.
gen overweight = 0 if bmi <.
replace overweight = 1 if bmi >=25 & bmi <30

/*bmi cut-offs as suggested by China Obesity Task Force*/

gen obese_china = 0 if bmi <.
replace obese_china = 1 if bmi >=28 & bmi <.
gen overweight_china = 0 if bmi <.
replace overweight_china = 1 if bmi >=24 & bmi <28


/*Smoking */

clonevar eversmoked = U25 if U25 < 9 // if eversmoked

clonevar stillsmoking = U27 if U27 < 9 // if still smoking
replace stillsmoking  = 0 if eversmoked == 0  // replace with 0 if never smoked


/* Drinking */

clonevar anyalc = U40 if U40<8 // if drank any alcohol this year


/* Physical Activity */

clonevar sleephrs = U324 if U324 > 0   // hours of sleep per day

* use Compendium of Physical Activities to assign activity level to activity at work, leisure, etc. similar to here http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2731106/

/*Create lagged LHS variables*/
sort id wave
bysort id: egen seq=seq()  // sequence variable to see how many waves people are in panel


foreach var of varlist lnindwage works stillsmoking anyalc bmi waist d3*{
gen `var'_prev=`var'[_n-1] if seq!=1
label var `var'_prev "Lagged value from previous wave"
}

/* Find those with only one wave reported -- No need to drop them now because we are also interested in those recently diagnosed in 2011
egen countwaves = count(wave), by (id)
drop if countwaves==1  // drop everybody with only 1 wave
*/
/* create baseline values for some of the variables for model as first proposed by Max*/
/*foreach var of varlist lnindwage works age {
gen temp=`var' if seq==1
bysort id: egen `var'_bl = mean(temp)
drop temp
label var `var'_bl "Baseline value"
}
*/

/*dropping not needed observations from before 1997 where no diabetes was reported*/
drop temp*
drop if wave < 1997

sort id wave
bysort id: egen seq_new=seq()  // new sequence variable after dropping all waves before 1997


save "Stata/data.dta", replace
saveold "Stata/data_13.dta", replace

 
