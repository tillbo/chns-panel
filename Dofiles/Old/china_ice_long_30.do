set more off
capture log close
clear matrix
	
global homefolder ""/home/till/Phd/China Data/Data_2014""
global homefolder "/gpfs/med/gsd12ytu/thesis/China"
cd $homefolder

use "Stata/data.dta", clear

log using Logs/ice.log, replace

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65


gen dmnow= diabetes_sr==1 


bysort id idt: gen dmyet=1 if dmnow==1 | (dmnow[_n-1]==1 )
replace dmyet = 0 if dmyet != 1
* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc indwage works age{
by id: gen `var'_bl= `var'[1] 
}

* fixed: rural age_bl female han i.province



egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

keep id wave idt dmyet age_bl female rural newt han province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works 

/*reshape wide wave idt dmyet age_bl female rural han province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, i(id) j(newt)*/

ice dmyet age_bl female rural han m.province index* waist* d3kcal* stillsmoking* anyalc* secondary* university* married*  insurance* indwage* works* , m(30) saving(ice30_long, replace)

exit

capture log close


/*
ice dmyet age_bl female rural han m.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works, m(10) saving(ice10, replace)

