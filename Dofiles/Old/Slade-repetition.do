******************************************************************************************************
/*Running GEE probit population average regressions to investigate the effect of a diabetes diagnosis
on behavioural and economic outcomes as in Slade JHE paper*/
******************************************************************************************************

global x "i.yearsdiagnomed i.yearsdiagmed i.diab_med age_ini age_sq female i.wave i.han i.province rural married secondary university insurance index"
global x_fe "i.yearsdiagnomed i.yearsdiagmed i.diab_med female  age_sq i.wave i.han i.province rural married secondary university insurance index"

global x_nf "yearsdiagall2 yearsdiagall3 yearsdiagall4 yearsdiagall5 yearsdiagall6 yearsdiagall7 index rural insurance"

/* probit estimations for binary variables*/

drop if age < 18 | age > 65


	*no lag
foreach var of varlist stillsmoking anyalc works{
xtgee `var' $x, family(binomial) link(probit) corr(exchangeable) vce(robust)
margins yearsdiagmed, post
estimates store med
xtgee `var' $x, family(binomial) link(probit) corr(exchangeable) vce(robust)
margins yearsdiagnomed,  post
estimates store nomed
xtreg `var' $x_fe, fe
margins yearsdiagmed, post
estimates store med_fe
xtreg `var' $x_fe, fe
margins yearsdiagnomed, post
estimates store nomed_fe

coefplot (med, recast(line)) (med_fe, recast(line)) (nomed, recast(line)) (nomed_fe, recast(line)), vertical  title("`var'") name(`var', replace)
graph export marg_`var'.eps, replace
}



* using lags
foreach var of varlist stillsmoking anyalc works{
xtgee `var' `var'_prev $x, family(binomial) link(probit) corr(exchangeable) vce(robust)
margins yearsdiagmed, post
estimates store med
}
xtgee `var' `var'_prev $x, family(binomial) link(probit) corr(exchangeable) vce(robust)
margins yearsdiagnomed,  post
estimates store nomed
coefplot (med, recast(line)) (nomed, recast(line)), vertical  title("`var'")
graph export marg_`var'_lag.eps, replace
}

/*+++++ xtreg population average estimations for continuous variables+++++++*/


foreach var of varlist bmi waist lnindwage d3*{ // regression with xtreg for continuous variables 
xtreg `var' $x, pa vce(robust)
margins yearsdiagmed, post
estimates store med
xtreg `var' $x, pa vce(robust)
margins yearsdiagnomed,  post
estimates store nomed
coefplot (med, recast(line)) (nomed, recast(line)), vertical  title("`var'") name(`var', replace)
graph export marg_`var'.eps, replace
}

// with lagged outcome variable

foreach var of varlist bmi waist lnindwage{ // regression with xtreg for contineous variables 
xtreg `var' `var'_prev $x, pa vce(robust)
margins yearsdiagmed, post
estimates store med
xtreg `var' `var'_prev $x, pa vce(robust)
margins yearsdiagnomed,  post
estimates store nomed
coefplot (med, recast(line)) (nomed, recast(line)), vertical  title("`var'") name(`var', replace)
graph export marg_`var'_lag.eps, replace
}


**************************************************************
/*+++++Now without taking treatment type into account+++++++*/
**************************************************************

global x "i.yearsdiagall_g age_sq i.wave married secondary university insurance index"

/*binary outcomes*/

foreach var of varlist stillsmoking anyalc works{ // no lag
/*xtgee `var' $x, family(binomial) link(probit) corr(exchangeable) vce(robust)
margins yearsdiagall, post
estimates store binary*/
xtreg `var' $x, fe 
margins yearsdiagall_g, post
estimates store binary_fe
coefplot /*(binary, recast(line))*/ (binary_fe, recast(line)), vertical  title("`var'") name(`var', replace)
graph export margins`var'_all.eps, replace

}

foreach var of varlist stillsmoking anyalc works activity_any{ // lagged
xtgee `var' `var'_prev $x, family(binomial) link(probit) corr(exchangeable) vce(robust)
margins yearsdiagall, post
estimates store binary
xtreg `var' `var'_prev $x, fe 
margins yearsdiagall, post
estimates store binary_fe
coefplot (binary, recast(line)) (binary_fe, recast(line)), vertical  title("`var'") name(`var', replace)
graph export margins`var'_all_lag.eps, replace
}

/*continuous outcomes*/

foreach var of varlist bmi waist lnindwage d3kcal{ // no lag 
/*xtreg `var' $x, pa vce(robust)
margins yearsdiagall,post
estimates store `var'*/
xtreg `var' $x, fe
margins yearsdiagall_g, post
estimates store `var'_fe
coefplot /*(`var', recast(line))*/ (`var'_fe, recast(line)), vertical  title("`var'") name(`var', replace)
graph export margins`var'_all.eps, replace
}



foreach var of varlist bmi waist lnindwage d3kcal d3carbo d3fat d3protn{ // lagged
sort id idt
xtreg `var' `var'_prev $x, pa vce(robust)
margins yearsdiagall, post
estimates store `var'
sort id idt
xtreg `var' `var'_prev $x, fe
margins yearsdiagall, post
estimates store `var'_fe
coefplot (`var', recast(line)) (`var'_fe, recast(line)), vertical  title("`var'_lag") name(`var', replace)
graph export margins`var'_all_lag.eps, replace
}

