set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder
log using Logs/china_dm10.log, replace

use "Stata/ice30.dta", clear

drop if _mj==0

reshape long dmyet age index bmi waist d3kcal stillsmoking anyalc education married  insurance indwage works m_age m_indwage, i(id _mj)
drop m_*1 m_*2 m_*3 m_*4 m_*5 m_*6
drop if m_age == 1


drop if age < 18 | age > 65



rename _j  wave

bysort id _mj: egen works_alw = min(works)

replace indwage = 0 if works_alw == 0


dummies education
rename education1 secondary
rename education2 university

drop if _mj==0
sort id _mj wave
egen temp=min(wave) if dmyet == 1, by(id _mj)
egen firstdmidt=mean(temp), by(id _mj)
drop temp

gen lnindwage = log(indwage)


// creating variable that is always 1 after someone became unemployed after start of survey
by id _mj (wave), sort: gen everunempl = 1 if works == 0 & works[_n-1] == 1
by id _mj (wave), sort: replace everunempl = 1 if works == 0 & works[_n-1] == 0
by id _mj (wave), sort: replace everunempl = 1 if everunempl[_n-1] == 1 & everunempl[_n+1] > 0
by id _mj (wave), sort: replace everunempl = 0 if everunempl == . & works !=.



foreach var of varlist age wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works everunempl{
bysort id _mj: gen `var'_bl= `var'[1] 
}





/*CREATING STABILIZED WEIGHTS*/
// xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_alw_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works_alw idt wave if idt<=firstdmidt | firstdmidt ==.

gen stabweightdmyet = .  // generate stabweightdmyet to later replace with calculated weights in loop
forvalues i = 1/30 {
xi: logistic dmyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_alw_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance works_alw i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet) if _mj == `i'
rename pdmyet dmyetdenom
//xi: logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_alw_bl wave wave if wave<=firstdmidt==. | firstdmidt ==.
xi: logistic dmyet age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl university_bl married_bl  insurance_bl works_alw_bl i.wave if _mj == `i' & (wave<=firstdmidt | firstdmidt ==.)

predict pdmyet if e(sample) & _mj == `i'
replace pdmyet=1 if wave>firstdmidt & _mj == `i'
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet) if _mj == `i'
bysort id _mj: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1 & _mj == `i'  // need to add _mj as another variable to sort by, otherwise it will not work
rename pdmyet dmyetnum
gen stabweightdmyet`i' =dmyetnum/dmyetdenom if _mj == `i'
drop dmyetdenom dmyetnum 
replace stabweightdmyet = stabweightdmyet`i' if _mj == `i'  // generates one overall stability weight
}


save "Stata/ice30_weighted", replace

use "Stata/ice30_weighted", clear


// have created two globals with controls for regression models. First for works_alw and wage models and the second for all other models

global x_prod "age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave"
global x_other "age_bl female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave works_alw"


xi: mim: regress lnindwage dmyet $x_prod ln indwage_bl [pw=stabweightdmyet] if works_alw == 1, cl(id)
xi: mim: logistic works_alw dmyet $x_prod [pw=stabweightdmyet], cl(id)
xi: mim: reg works_alw dmyet $x_prod [pw=stabweightdmyet], cl(id)
xi: mim: regress bmi dmyet $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress waist dmyet $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress d3kcal dmyet $x_other [pw=stabweightdmyet], cl(id)
xi: mim: logistic  anyalc dmyet $x_other [pw=stabweightdmyet], cl(id)

/*

xi: mim: regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works_alw == 1, cl(id)
xi: mim: logistic works_alw i.duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: mim: regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)


xi: mim: regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works_alw == 1, cl(id)
xi: mim: logistic works_alw i.duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: mim: regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)

* duration=1 for first wave diabetes is reported
replace duration=1 if wave==firstdmyear

xi: mim: regress lnindwage i.duration  $x_prod [pw=stabweightdmyet]  if works_alw == 1, cl(id)
xi: mim: logistic works_alw i.duration  $x_prod [pw=stabweightdmyet], cl(id)
xi: mim: regress bmi i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress waist i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: regress d3kcal i.duration $x_other [pw=stabweightdmyet], cl(id)
xi: mim: logistic  anyalc i.duration $x_other [pw=stabweightdmyet], cl(id)

* unweighted  ADD TOME VARIANT VARIABLES HERE
xi: mim: regress lnindwage dmyet $x_prod if works_alw == 1, cl(id)
xi: mim: logistic works_alw dmyet $x_prod, cl(id)
xi: mim: reg works_alw dmyet $x_prod, cl(id)
xi: mim: regress bmi dmyet $x_other, cl(id)
xi: mim: regress waist dmyet $x_other, cl(id)
xi: mim: regress d3kcal dmyet $x_other, cl(id)
xi: mim: logistic  anyalc dmyet $x_other, cl(id)

