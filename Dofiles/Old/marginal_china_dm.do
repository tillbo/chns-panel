set more off
capture log close
clear matrix
/*
// preparational do files
global homefolder ""/home/till/Dropbox/PhD/Data/China Data""

        do "/home/till/Dropbox/PhD/Data/China Data/merging.do"	//master china files
	do "/home/till/Dropbox/PhD/Data/China Data/variablecreation.do" //creating new variables
	
	*/
	



global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder

use "Stata/data.dta", clear

egen newt = group(wave)

xtset id newt

drop if age < 18 | age > 65

* baseline covariates
foreach var of varlist wave secondary university married  insurance index bmi waist d3kcal stillsmoking anyalc lnindwage works diabetes_sr{
by id: gen `var'_bl= `var'[1] 
}

* fixed: rural age_ini female han i.province
drop if diabetes_sr_bl == 1

* generate indicator if has diabetes that corrects for inconsistent reporting later on, i.e. always 1 after first reported diagnosis
gen dmyet = diabetes_sr
by id (idt), sort: replace dmyet = dmyet[_n-1] if dmyet[_n-1] == 1



* generate indicator if has gotten unemployed during the survey, i.e. always 1 after first change from employed to unemployed

by id (wave), sort: gen unemplyet = 1 if works == 0 & works[_n-1] == 1  // indicator if is unemployed after previously being employed
by id (wave), sort: replace unemplyet = 1 if works == 0 & works[_n-1] == 0  // indicate if is currently unemployed nad has been before as well
by id (wave), sort: replace unemplyet = 1 if unemplyet[_n-1] == 1 & unemplyet[_n+1] > 0  // replace current missing or zeros if was unemployed in previous wave and missing or unemployed in later wave
by id (wave), sort: replace unemplyet = 0 if unemplyet == . & works !=. // replace with zero (employed) if missing and works is not missing






egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp

gen duration=wave-firstdmyear
replace duration = 0 if duration ==.

//  logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works idt wave if idt<=firstdmidt | firstdmidt ==.
 logistic dmyet age_ini female rural han works_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance i.wave if idt<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
rename pdmyet dmyetdenom
// logistic dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl idt wave if idt<=firstdmidt==. | firstdmidt ==.
 logistic dmyet age_ini female rural han works_bl index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave if idt<=firstdmidt | firstdmidt ==.

predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet)
sort id idt
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
// problem above is that many have dmyet from previous wave missing so that many observations get lost doing the calculation. Alternative would be commented out code but stabweigths are far from 1. Not sure if this works though? This is especially important when those with diabetes from the start on are excluded as it is needed for the MSM.
rename pdmyet dmyetnum
gen stabweightdmyet =dmyetnum/dmyetdenom

// employment

egen temp=min(idt) if unemplyet == 1, by(id)
egen firstunemplidt =mean(temp), by(id)
drop temp

egen temp = min(wave) if unemplyet==1, by(id)
egen firstunemplyear = mean(temp), by(id)
drop temp

logistic unemplyet age_ini female rural i.province han index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl index bmi waist d3kcal stillsmoking anyalc secondary university married  insurance i.wave if idt<=firstunemplidt | firstunemplidt ==.

predict punemplyet if e(sample)
replace punemplyet=1 if idt>firstunemplidt
replace punemplyet=punemplyet*unemplyet + (1-punemplyet)*(1-unemplyet)
rename punemplyet unemplyetdenom
// logistic unemplyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl idt wave if idt<=firstdmidt==. | firstdmidt ==.
logistic unemplyet age_ini female i.province rural han index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl idt  if idt<=firstunemplidt | firstunemplidt ==.

predict punemplyet if e(sample)
replace punemplyet=1 if idt>firstunemplidt
replace punemplyet=punemplyet*unemplyet+(1-punemplyet)*(1-unemplyet)
sort id idt
by id: replace punemplyet=punemplyet*punemplyet[_n-1] if _n!=1
rename punemplyet unemplyetnum
gen stabweightunemplyet =unemplyetnum/unemplyetdenom


 regress lnindwage dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  i.wave [pw=stabweightdmyet], cl(id)
 logit works dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.wave, cl(id)
 regress bmi dmyet age_ini female rural han i.province index_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  i.wave [pw=stabweightdmyet], cl(id)
 regress waist dmyet age_ini female rural han i.province index_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  i.wave [pw=stabweightdmyet], cl(id)
 regress d3kcal dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  i.wave [pw=stabweightdmyet], cl(id)
 logit  anyalc dmyet age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl secondary_bl university_bl married_bl  insurance_bl  i.wave [pw=stabweightdmyet], cl(id)

// effect of unemployment on diabetes
 logit dmyet unemplyet age_ini female rural han index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl i.idt [pw=stabweightunemplyet], cl(id)
 logit dmyet unemplyet age_ini female rural han index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  i.idt if stabweightunemplyet <., cl(id)


 regress lnindwage duration age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
 logit works duration age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
 regress bmi duration age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
 regress waist duration age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
 regress d3kcal duration age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)
 logit  anyalc duration age_ini female rural han i.province index_bl bmi_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl  idt wave [pw=stabweightdmyet], cl(id)

/* 
drop dmyetdenom dmyetnum stabweightdmyet

