//cd "\\ueahome\eresfmh3\wm063\data\Documents\max\PhD\till"

set more off
capture log close
clear matrix


global homefolder ""/home/till/Phd/China Data/Data_2014""
cd $homefolder

use "Stata/ice1.dta", clear

drop if _mj==0
sort id idt
egen temp=min(idt) if dmyet == 1, by(id)
egen firstdmidt=mean(temp), by(id)
drop temp

xi: logistic dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl works_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works i.idt if idt<=firstdmidt | firstdmidt ==.
predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet + (1-pdmyet)*(1-dmyet)
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
rename pdmyet dmyetdenom

xi: logistic dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl i.idt if idt<=firstdmidt | firstdmidt ==.
predict pdmyet if e(sample)
replace pdmyet=1 if idt>firstdmidt
replace pdmyet=pdmyet*dmyet+(1-pdmyet)*(1-dmyet)
by id: replace pdmyet=pdmyet*pdmyet[_n-1] if _n!=1
rename pdmyet dmyetnum

gen stabweightdmyet =dmyetnum/dmyetdenom

xi: logistic works dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet] if works==1, cl(id)
xi: regress BMI dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress waist dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress d3kcal dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)

* duration=0 for first wave diabetes is reported
egen temp = min(wave) if dmyet==1, by(id)
egen firstdmyear = mean(temp), by(id)
drop temp
gen duration=wave-firstdmyear if dmyet==1
replace duration = 0 if duration ==.

xi: logistic works duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet] if works==1, cl(id)
xi: regress BMI duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress waist duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress d3kcal duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)

xi: logistic works i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet] if works==1, cl(id)
xi: regress BMI i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress waist i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress d3kcal i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)

* duration=1 for first wave diabetes is reported
replace duration=1 if wave==firstdmyear
xi: logistic works i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress lnindwage i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet] if works==1, cl(id)
xi: regress BMI i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress waist i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: regress d3kcal i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)
xi: logistic  anyalc i.duration age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl  i.idt [pw=stabweightdmyet], cl(id)


/* 
* equivalent pooled unweighted models
xi: logistic works dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage  i.idt, cl(id)
xi: regress lnindwage dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking anyalc secondary university married  insurance   i.idt, cl(id)
xi: regress BMI dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index  waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage  i.idt, cl(id)
xi: regress waist dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI  d3kcal stillsmoking anyalc secondary university married  insurance lnindwage  i.idt, cl(id)
xi: regress d3kcal dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist  stillsmoking anyalc secondary university married  insurance lnindwage  i.idt, cl(id)
xi: logistic anyalc dmyet age_ini female rural han i.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking  secondary university married  insurance lnindwage  i.idt, cl(id)



