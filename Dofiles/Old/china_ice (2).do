cd "\\ueahome\eresfmh3\wm063\data\Documents\max\PhD\till"
use data.dta, clear
drop if age < 18 | age > 65
replace anyalc=. if anyalc==8
drop seq
sort id idt
egen seq=seq(), by(id)
* drop individuals if in only one wave
egen maxseq=max(seq), by(id)
drop if maxseq==1
/* drop individuals if diabetes reported in first wave
gen temp=1 if  seq==1 & dmyet==1
egen prevalentdm=mean(temp), by(id)
drop temp
drop if prevalentdm==1
*/
sort id idt
gen dmyet=1 if diabetes_sr==1 
by id: replace dmyet=1 if dmyet!=1 & diabetes_sr[_n-1]==1 & _n!=1
by id: replace dmyet=1 if dmyet!=1 & dmyet[_n-1]==1 & _n!=1
replace dmyet = 0 if dmyet != 1

* baseline covariates
foreach var of varlist secondary university married  insurance index BMI waist d3kcal stillsmoking anyalc lnindwage works{
by id: gen `var'_bl= `var'[1] 
}
* 

keep id idt wave seq dmyet age_ini female rural han province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works 

ice dmyet age_ini female rural han m.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works i.idt, m(1) saving(ice1, replace)
ice dmyet age_ini female rural han m.province index_bl BMI_bl waist_bl d3kcal_bl stillsmoking_bl anyalc_bl secondary_bl university_bl married_bl  insurance_bl lnindwage_bl works_bl index BMI waist d3kcal stillsmoking anyalc secondary university married  insurance lnindwage works i.idt, m(10) saving(ice10, replace)

